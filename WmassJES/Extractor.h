#pragma once

#include "WmassJES/SystHistoManager.h"
#include "WmassJES/FFFitter.h"

#include "TFile.h"
#include "TH1D.h"

#include <fstream>
#include <map>
#include <memory>
#include <string>
#include <vector>

class TH2D;

class Extractor {

public:
    explicit Extractor(const std::string& folder);

    ~Extractor() = default;

    inline void SetOutputFolder(const std::string& folder) {m_outputFolder = folder;}

    inline void SetRegion(const std::string& region) {m_region = region;}

    inline void SetVariableSuffixes(const std::vector<std::string>& suffixes) {m_variable_suffixes = suffixes;}

    void SetTtbarName(const std::string& name) {m_ttbar_name = name;}

    void SetBackgroundNames(const std::vector<std::string>& names) {m_background_names = names;}

    void SetSpecialNames(const std::vector<std::string>& names) {m_special_names = names;}

    void SetParameterS(const int steps,
                       const float min,
                       const float max);

    void SetParameterR(const int steps,
                       const float min,
                       const float max);

    void SetAllSysts(const std::vector<Systematic>& systs) {m_systematics = systs;}

    void SetFitData(const bool flag) {m_fit_data = flag;}

    void SetUseSyst(const bool flag) {m_use_syst = flag;}

    void SetNumberOfPE(const int n) {m_PE_number = n;}

    void SetObsName(const std::string& name) {m_obs_name = name;}

    void SetTemplateName(const std::string& name) {m_template_name = name;}

    void SetDebug(const bool flag) {m_debug = flag;}

    void SetSingleRegions(const std::vector<std::pair<std::string, std::string> >& regions);

    void SetCrossRegions(const std::vector<std::pair<std::string, std::pair<std::string, std::string> > >& regions);

    void SetIsJesFit(const bool flag) {m_is_jes_fit = flag;}

    void SetClosureIndicesCombined(const std::vector<int>& indices) {m_closure_indices_combined = indices;}

    void SetStartIndex(const int index) {m_start_index = index;}

    void SetIs1DFit(const bool flag) {m_is_1D_fit = flag;}

    void ExtractJES();

    void SetProfile(const bool flag) {m_profile = flag;}

    void SetRunClosure(const bool flag) {m_run_closure = flag;}

private:

    struct Param {
        int steps;
        float min;
        float max;
    };

    void OpenRootFiles();

    void CloseRootFiles();

    std::unique_ptr<TH1D> GetSystShiftedHist(const std::vector<Systematic>& systs,
                                             const std::string& observable,
                                             const bool is_up,
                                             const bool use_ref,
                                             const bool use_subtract = false) const;

    std::vector<TH1D> GetShiftedHistSingleRegions(const std::vector<Systematic>& systs,
                                                  const bool is_up,
                                                  const bool use_ref) const;

    std::vector<TH1D> GetShiftedHistCrossRegions(const std::vector<Systematic>& systs,
                                                 const bool is_up,
                                                 const bool use_ref) const;

    std::unique_ptr<TH1D> GetShiftedAuxHist(const std::vector<Systematic>& systs,
                                            const bool is_up,
                                            const bool use_ref) const;

    void PlotFinalUncertaintyCombined(TFile* out, const bool is_asimov) const;

    std::vector<std::pair<std::vector<TH1D>, std::string> > GetNominalTemplateSingleRegion(const FFFitter& fitter) const;

    std::vector<std::pair<std::vector<std::vector<TH1D> >, std::pair<std::string, std::string> > > GetNominalTemplateCrossRegion(const FFFitter& fitter) const;

    std::vector<TH1D> GetDataHistogramSingleRegion() const;
    std::vector<TH1D> GetDataHistogramCrossRegion() const;

    std::vector<std::string> GetSingleRegionNames() const;
    std::vector<std::string> GetCrossRegionNames() const;

    void AsimovFitCombined(FFFitter* fitter);

    void DataFitCombined(FFFitter* fitter);

    std::vector<TH1D> GetAsimovSingleRegions() const;

    std::vector<TH1D> GetAsimovCrossRegions() const;

    void ProcessSingleSystematicCombined(FFFitter* fitter, const std::string& syst);

    void UpdateUncertainties(const std::map<std::string, double>& up,
                             const std::map<std::string, double>& down,
                             const std::map<std::string, double>& central,
                             const std::string& syst,
                             const std::string& category,
                             const bool is_one_sided);

    void PrintUncertaintiesCombined();

    std::vector<TH1D> GetClosureHistogramsSingleRegions(const std::string& region1,
                                                        const std::string& region2,
                                                        const int index1,
                                                        const int index2,
                                                        const bool is_jes) const;
    std::vector<TH1D> GetClosureHistogramsCrossRegions(const std::string& region1,
                                                       const std::string& region2,
                                                       const int index1,
                                                       const int index2,
                                                       const bool is_jes) const;

    std::map<std::string, double> RunClosureCombined(FFFitter* fitter, const bool is_jes, std::map<std::string, double> closure_asimov);

    std::unique_ptr<TH1D> GetAsimovSingleHisto(const std::string& folder, const std::string& obs) const;

    void RunPseudoExperimentsCombined(FFFitter* fitter);

    void DrawCorrelationMatrix(const FFFitter& fitter, TFile* out,  const bool is_asimov) const;

    void RunProfiledFit(FFFitter* fitter);

    std::map<std::string, std::pair<std::vector<double>, std::vector<double> > > GetImpacts() const;

    void PlotRedBlue(const std::string& name,
                     const std::vector<TH1D>& nominal_single,
                     const std::vector<TH1D>& nominal_cross,
                     const std::vector<TH1D>& syst_up_single,
                     const std::vector<TH1D>& syst_up_cross,
                     const std::vector<TH1D>& syst_down_single,
                     const std::vector<TH1D>& syst_down_cross,
                     const std::vector<TH1D>& data_single,
                     const std::vector<TH1D>& data_cross,
                     const std::pair<std::vector<double>, std::vector<double> >& impacts) const;

    void StoreResults(TFile* out) const;

    void DrawPrePost(const FFFitter& fitter,
                     const std::vector<TH1D>& nominal_single,
                     const std::vector<TH1D>& nominal_cross,
                     const std::vector<TH1D>& data_single,
                     const std::vector<TH1D>& data_cross) const;

    bool m_debug;

    std::string m_folder;
    std::string m_outputFolder;
    std::string m_region;
    std::vector<std::string> m_variable_suffixes;
    bool m_fit_data;
    bool m_use_syst;
    int m_PE_number;
    std::string m_ttbar_name;
    std::string m_obs_name;
    std::string m_template_name;
    std::vector<std::string> m_background_names;
    std::vector<std::string> m_special_names;

    Param m_param_s;
    Param m_param_r;

    std::unique_ptr<TFile> m_ttbar_file;
    std::unique_ptr<TFile> m_data_file;
    std::vector<std::unique_ptr<TFile> > m_background_files;
    std::vector<std::unique_ptr<TFile> > m_special_files;

    std::vector<Systematic> m_systematics;

    bool m_run_closure;

    std::vector<std::pair<std::string, std::string> > m_single_regions;
    std::vector<std::pair<std::string, std::pair<std::string, std::string> > > m_cross_regions;
    bool m_is_jes_fit;

    std::map<std::string, std::map<std::string, std::pair<double,double> > > m_combined_fit_uncertainty_all;
    std::map<std::string, std::map<std::string, std::pair<double,double> > > m_combined_fit_uncertainty_category;
    std::map<std::string, std::pair<double, double> > m_combined_fit_stat;
    std::map<std::string, double> m_combined_fit_central;
    std::map<std::string, double> m_combined_fit_central_data;
    std::map<std::string, std::pair<double, double> > m_combined_fit_data;

    std::vector<int> m_closure_indices_combined;
    int m_start_index;
    bool m_is_1D_fit;
    bool m_profile;
};
