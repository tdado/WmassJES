#pragma once

#include "WmassJES/SystHistoManager.h"

#include <string>
#include <vector>

namespace SetupSystematic {
  std::vector<std::string> GetListOfTtbarSamples();

  std::vector<std::string> GetListOfBackgroundSamples();

  std::vector<std::string> GetListOfSpecialSamples();

  std::string NominalTtbarSample();

  std::vector<Systematic> GetAllSystematics(const bool forPlotting, const bool is_jes);
};
