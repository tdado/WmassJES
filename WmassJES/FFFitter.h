#pragma once

#include "WmassJES/FoldingParameter.h"

#include "TF2.h"
#include "TH1.h"
#include "TH2D.h"
#include "TRandom3.h"

#include "RooAbsData.h"
#include "RooAbsPdf.h"
#include "RooArgList.h"
#include "RooDataSet.h"
#include "RooFitResult.h"
#include "RooWorkspace.h"

#include <memory>
#include <vector>


class FFFitter {

public:

    struct FitParams {
        double central;
        double positive_error;
        double negative_error;
        double error;
    };

    struct JetParams {
        double s;
        double r;
        double norm;
        double s_uncertainty;
        double r_uncertainty;
        double norm_uncertainty;
    };

    FFFitter();

    ~FFFitter() = default;

    void SetParameterR(const int steps,
                       const double min,
                       const double max);

    void SetParameterS(const int steps,
                       const double min,
                       const double max);

    void SetRandomSeed(const int seed){m_seed = seed;}

    void PrepareWSCombined();

    int GetSsteps() const {return m_param_s->GetSteps();}
    int GetRsteps() const {return m_param_r->GetSteps();}

    double GetDeltaS() const {return m_param_s->GetDelta();}
    double GetDeltaR() const {return m_param_r->GetDelta();}

    bool FitIsOkay() const {return m_fit_is_okay;}

    double GetMinS() const {return m_param_s->GetMin();}
    double GetMinR() const {return m_param_r->GetMin();}

    double GetMaxS() const {return m_param_s->GetMax();}
    double GetMaxR() const {return m_param_r->GetMax();}

    double IndexToValue(const int index, const FoldingParameter::PARAMETER& par) const;

    void SetOutputFolder(const std::string& folder) {m_output_folder = folder;}

    inline void SetDebug(const bool flag) {m_debug = flag;}

    void SetSingleRegionTemplates(const std::vector<std::pair<std::vector<TH1D>, std::string> >& templatesi, const std::vector<std::string>& names);
    void SetCrossRegionTemplates(const std::vector<std::pair<std::vector<std::vector<TH1D> >, std::pair<std::string, std::string> > >& templates, const std::vector<std::string>& names);

    void PrepareCombinedFitParametrisation();

    void SetIsJesFit(const bool flag) {m_is_jes_fit = flag;}

    std::map<std::string, double> FitCombinedModel(const std::vector<TH1D>& single_region_histograms, const std::vector<TH1D>& cross_region_histograms, const bool write, const int print_level = 1);

    const std::map<std::string, FitParams>& GetCombinedFitResults() const {return m_combined_fit_results;};
    void SetCombinedFitResults();

    FitParams GetCombinedFitResultSingleParam(const std::string& param) const;
    double GetCombinedFitCentralSingleParam(const std::string& param) const;

    void SetSingleRegions(const std::vector<std::pair<std::string, std::string> >& regions) {m_single_regions = regions;}

    void SetCrossRegions(const std::vector<std::pair<std::string, std::pair<std::string, std::string> > >& regions) {m_cross_regions = regions;}

    void SetStartIndex(const int index){m_start_index = index;}

    void SetIs1DFit(const bool flag) {m_is_1D_fit = flag;}

    std::map<std::string, std::vector<double> > RunCombinedPseudoExperiments(const std::vector<TH1D>& single_region_hists,
                                                                             const std::vector<TH1D>& cross_region_hists,
                                                                             const int n);

    std::unique_ptr<TH2D> GetCorrelationMatrix() const;

    void SetStartingParameter(const RooAbsPdf* model,
                              const RooAbsData* data,
                              const std::vector<std::pair<std::string, double> >& param_values) const;

    void SetProfile(const bool flag) {m_profile = flag;}

    void SetImpactMap(const std::map<std::string, std::pair<std::vector<double>, std::vector<double> > >& map) {m_impact_map = map;}

    void SetNominalSingleRegion(const std::vector<TH1D>& hists);
    void SetNominalCrossRegion(const std::vector<TH1D>& hists);

    const std::vector<TF1>& GetSingleRegionFunctions() const {return m_single_region_functions;}
    const std::vector<TF1>& GetCrossRegionFunctions() const {return m_cross_region_functions;}

private:

    void Poissonise(TH1D* h, TRandom3* rand) const;

    void Gaussinise(TH1D* h, TRandom3* rand) const;

    void PrepareCombinedFitParametrisationSingleRegion();
    void PrepareCombinedFitParametrisationCrossRegion();

    void ProcessCombinedFitSingleRegion(const std::size_t index);
    void ProcessCombinedFitCrossRegion(const std::size_t index);

    std::string TranslateFormulaCombined1D(const TF1& func, const std::string& param) const;
    std::string TranslateFormulaCombined2D(const TF2& func, const std::pair<std::string, std::string>& param) const;

    std::string GetSingleParamFromRegion(const std::string& region) const;
    std::pair<std::string, std::string> GetCrossParamFromRegion(const std::string& region) const;

    std::vector<std::pair<std::string, double> > GetStartingValuesCombined(const std::vector<double>& data_values,
                                                                           const std::vector<double>& data_values_cross) const;

    double SolveStartingPoint(const TF1& func, const double value) const;

    double SolveStartingPointFirst(const std::string& param,
                                   const std::vector<std::pair<std::string, double> >& starting_points,
                                   const std::vector<double>& data_values_cross) const;

    std::unique_ptr<FoldingParameter> m_param_r;
    std::unique_ptr<FoldingParameter> m_param_s;

    int m_seed;
    bool m_fit_is_okay;

    std::string m_output_folder;
    RooArgList m_observables;
    RooArgList m_data_args;
    std::unique_ptr<RooWorkspace> m_ws_combined;
    bool m_debug;
    std::unique_ptr<RooFitResult> m_fit_result_combined;
    std::unique_ptr<RooDataSet> m_dataset;
    std::unique_ptr<RooDataHist> m_datahist;

    std::vector<std::pair<std::vector<TH1D>, std::string> > m_single_templates;
    std::vector<std::pair<std::vector<std::vector<TH1D>>, std::pair<std::string,std::string> > > m_cross_templates;
    std::vector<std::string> m_single_region_names;
    std::vector<std::string> m_cross_region_names;

    std::vector<std::pair<std::string, std::string> > m_combined_formula_single_region;
    std::vector<std::pair<std::string, std::string> > m_combined_formula_cross_region;
    bool m_is_jes_fit;
    std::map<std::string, FitParams> m_combined_fit_results;
    std::map<std::string, double> m_combined_fit_central;

    std::vector<std::pair<std::string, std::string> > m_single_regions;
    std::vector<std::pair<std::string, std::pair<std::string, std::string> > > m_cross_regions;
    int m_start_index;
    std::vector<TF1> m_single_region_functions;
    std::vector<TF1> m_cross_region_functions;
    bool m_is_1D_fit;
    bool m_profile;

    std::map<std::string, std::pair<std::vector<double>, std::vector<double> > > m_impact_map;

    std::vector<TH1D> m_nominal_single_region;
    std::vector<TH1D> m_nominal_cross_region;

    std::vector<std::string> m_pruned_params;
};
