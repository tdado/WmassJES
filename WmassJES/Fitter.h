#pragma once

#include "TF1.h"

#include <memory>

class TH1D;

class Fitter {
  public:
    explicit Fitter(TH1D* histo);

    ~Fitter() = default;

    void Fit(const int color);

    inline float GetMean() const {return m_mean;}
    
    inline float GetSigma() const {return m_sigma;}
    
    inline float GetChi2() const {return m_chi2;}

    inline TF1* GetFunc() const {return m_formula.get();}

    inline void SetRangeRMS(const float factor) {m_factor = factor;}

  public:
    std::unique_ptr<TF1> m_formula;
    TH1D* m_hist;
    float m_mean;
    float m_sigma;
    float m_chi2;
    float m_factor;

};
