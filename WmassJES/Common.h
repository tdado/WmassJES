#pragma once

#include "WmassJES/SystHistoManager.h"

#include <map>
#include <string>
#include <vector>

class TH1D;
class TH2D;

namespace Common {
    enum LEPTONTYPE {
        EL = 0,
        MU = 1,
    };

    void AddOverflow(TH1D* hist);

    void AddOverflow(TH2D* hist);

    void NormaliseMatrix(TH2D* mat, bool byRow = true);

    std::size_t IndexFromVec(const float& value,
                             const std::vector<float>& binning,
                             const bool& is_abs);

    std::string RemoveSubstrings(const std::string& str, const std::vector<std::string>& remove);

    float GetMax(const TH1D* h1, const TH1D* h2);

    double GetMean(const std::vector<double>& v);

    double GetSigma(const std::vector<double>& v);

    double GetMeanGaus(const std::vector<double>& y, const double min, const double max, double& error);

    double GetSigmaGaus(const std::vector<double>& y,
                        const double min,
                        const double max,
                        double& error,
                        const std::string& path,
                        const bool is_jes,
                        const std::string& label);

    std::vector<std::string> GetSystUniqueNames(const std::vector<Systematic>& all);

    std::vector<Systematic> GetSystSameName(const std::vector<Systematic>& all,
                                            const std::string& name);

    bool SystematicsAreConsistent(const std::vector<Systematic>& systs);

    int GetSystematicIndex(const std::vector<Systematic>& syst, const std::string& name);

    void AddMapUncertainty(std::map<std::string, std::pair<double,double> >& map,
                           const double& up,
                           const double& down,
                           const std::string& name);

    double SumCategories(const std::map<std::string, std::pair<double,double> >& map,
                         const bool& isUp);

    std::string NiceRangeLabels(const std::string& in, const bool isEta);

    std::string ReplaceString(const std::string& original, const std::string& from, const std::string& to);

    std::string RegionToRangeLatex(const std::string& region);

    std::string RegionToRangeTxT(const std::string& region);

    std::pair<double, double> RegionToRangeValue(const std::string& region);

    void SetErrorsToExpected(TH1D* hist);

    double GetUncertaintyOnMean(const TH1D& hist);

    double GetUncertaintyOnRMS(const TH1D& hist);

    std::vector<double> GetMeanRMS(const std::vector<TH1D>& histos, const bool is_mean);
}
