#ifndef COMPARATOR_H_
#define COMPARATOR_H_

#include "TFile.h"

#include <map>
#include <memory>
#include <string>
#include <vector>

struct Labels{
    std::string x_axis;
    std::string units;
};

class Comparator {

public:
    Comparator();

    ~Comparator() = default;

    void SetFirstHisto(const std::string& folder,
                       const std::string& file,
                       const std::string& name);
    
    void SetSecondHisto(const std::string& folder,
                        const std::string& file,
                        const std::string& name);
                        
    void SetRegionNames(const std::vector<std::string>& names);

    void SetNormalise(const bool& flag) {m_normalise = flag;}

    void OpenRootFiles();

    void GetHistoList();

    void PlotAllHistos();

    void CloseRootFiles();
    
    void AddAllLabels();

private:

    std::string m_folder1_name;
    std::string m_file1_name;
    std::string m_histo1_name;
    std::string m_folder2_name;
    std::string m_file2_name;
    std::string m_histo2_name;
    std::vector<std::string> m_regions;
    bool m_normalise;
    std::vector<std::string> m_histo_list;

    std::map<std::string, Labels> m_labels_map;

    std::unique_ptr<TFile> m_file1;
    std::unique_ptr<TFile> m_file2;

    void PlotSingleHisto(const std::string& name) const;

    void AddSingleLabel(const std::string& name,
                        const std::string& x,
                        const std::string& units);

};

#endif
