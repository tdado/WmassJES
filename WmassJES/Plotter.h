#pragma once

#include "WmassJES/SystHistoManager.h"

#include "TFile.h"
#include "TArrow.h"

#include <map>
#include <memory>
#include <string>
#include <utility>
#include <vector>

class TGraphAsymmErrors;
class TH1D;
class TPad;

class Plotter {

public:
    explicit Plotter(const std::string& directory);

    ~Plotter() = default;

    void SetOutputDirectory(const std::string& output);

    void SetRegionNames(const std::vector<std::string>& names);

    void SetTTbarNames(const std::vector<std::string>& names);

    void SetBackgroundNames(const std::vector<std::string>& names);

    void SetSpecialNames(const std::vector<std::string>& names);

    void SetDataName(const std::string& name);

    void SetAtlasLabel(const std::string& label);

    void SetLumiLabel(const std::string& label);

    void SetCollection(const std::string& collection);

    void SetNominalTTbarName(const std::string& name);

    const std::string& GetNominalTTbarName() const {return m_nominal_ttbar_name;}

    void SetSystShapeOnly(const bool& flag) {m_syst_shape_only = flag;}

    void SetAllSysts(const std::vector<Systematic>& systs) {m_systematics = systs;}

    void SetUseSyst(const bool flag) {m_run_syst = flag;}

    void PlotTruthPlots(const std::string& name, const std::string& axis, const bool& normalise) const;

    void PlotResponsePlots(const std::string& name,
                           const std::string& suffix,
                           const std::string& axis,
                           const bool normalise) const;

    void PlotTemplatePlots(const std::string& name,
                           const std::string& axis,
                           const std::string& elements,
                           const std::string& units,
                           const bool& is_1D,
                           const bool& is_s,
                           const bool& normalise,
                           const bool& doing_response) const;

    void PlotDataMCPlots(const std::string& name,
                         const std::string& axis,
                         const std::string& elements,
                         const std::string& units,
                         const bool& logY) const;

    void PlotDataMCPlots(const std::string& name,
                         const std::string& axis,
                         const std::string& elements,
                         const std::string& units,
                         const bool& logY,
                         const std::string& region) const;

    void PlotRecoEfficiencies() const;
    void PlotRecoEfficiencies(const std::string& region) const;

    void PlotJetPairing() const;

    void CalculateYields() const;

    void OpenRootFiles();

    void CloseRootFiles();

private:
    std::vector<std::string> m_regions;
    std::string m_output_directory;
    std::string m_directory;

    std::vector<std::string> m_ttbar_names;
    std::vector<std::string> m_background_names;
    std::vector<std::string> m_special_names;
    std::string m_data_name;

    std::vector<std::unique_ptr<TFile> > m_ttbar_files;
    std::vector<std::unique_ptr<TFile> > m_background_files;
    std::vector<std::unique_ptr<TFile> > m_special_files;
    std::unique_ptr<TFile> m_data_file;

    std::vector<std::pair<int,int> > m_styles;

    std::string m_atlas_label;
    std::string m_lumi_label;
    std::string m_collection;
    std::string m_nominal_ttbar_name;
    std::size_t m_nominal_ttbar_index;
    bool m_syst_shape_only;
    bool m_run_syst;
    std::map<const std::string, int> m_colour_map;
    std::map<const std::string, std::string> m_label_map;
    std::vector<Systematic> m_systematics;

    void PlotUpperComparison(const std::vector<std::unique_ptr<TH1D> >& histos,
                             TPad* pad,
                             const std::string& axis) const;

    void PlotRatioComparison(const std::vector<std::unique_ptr<TH1D> >& histos,
                             TPad* pad,
                             const std::string& axis,
                             const float& range,
                             const bool& force) const;

    void FillStyleMap();

    float MaxResize(const std::vector<std::unique_ptr<TH1D> >& histos, const bool is_log) const;

    void DrawLabels(TPad *pad, const float& x, const float& y, const bool& add_lumi) const;

    void DrawUpperTemplatePlot(TPad* pad,
                               const std::vector<std::unique_ptr<TH1D> >& histos) const;

    void DrawRatioTemplatePlot(TPad* pad,
                               const std::vector<std::unique_ptr<TH1D> >& histos,
                               const std::string& axis,
                               const bool& is_s,
                               const bool& doing_response) const;

    void DrawUpperDataMCPlot(TPad* pad,
                             TH1D* ttbar_histo,
                             const std::vector<std::unique_ptr<TH1D> >& background_histos,
                             TH1D* data_histo,
                             TGraphAsymmErrors* error) const;

    void DrawRatioDataMCPlot(TPad* pad,
                             const TH1D* combined,
                             TH1D* data,
                             const std::string& axis,
                             TGraphAsymmErrors* total_down,
                             std::vector<std::unique_ptr<TArrow> >& arrows) const;

    void SetColourMap();

    void SetLabelMap();

    void GetTotalUpDownUncertainty(TH1D* total_up, TH1D* total_down, const std::string& name, const std::string& region) const;

    void AddSingleSyst(TH1D* total_up, TH1D* total_down, const std::string& name, const Systematic& systematic, const std::string& region) const;

    std::unique_ptr<TH1D> GetHistoFromAll(const std::string& file_name, const std::string& histo_name) const;

    void AddHistosInSquares(TH1D* total_up, TH1D* total_down, const TH1D* histo_up, const TH1D* histo_down, const TH1D* histo_nominal) const;

    void TransformErrorHistoToTGraph(TGraphAsymmErrors* error, const TH1D* up, const TH1D* down) const;

    void CalculateYields(const std::string& region) const;
};
