#include "WmassJES/Comparator.h"
#include "WmassJES/Common.h"
#include "AtlasUtils/AtlasStyle.h"

#include "TCanvas.h"
#include "TDirectory.h"
#include "TH1D.h"
#include "TKey.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TLine.h"
#include "TPad.h"
#include "TSystem.h"

#include <iostream>

Comparator::Comparator() :
    m_folder1_name(""),
    m_file1_name(""),
    m_histo1_name(""),
    m_folder2_name(""),
    m_file2_name(""),
    m_histo2_name(""),
    m_normalise(false) {

        SetAtlasStyle();
}

void Comparator::SetFirstHisto(const std::string& folder,
                               const std::string& file,
                               const std::string& name) {
    m_folder1_name = folder;
    m_file1_name = file;
    m_histo1_name = name;
}

void Comparator::SetSecondHisto(const std::string& folder,
                                const std::string& file,
                                const std::string& name) {
    m_folder2_name = folder;
    m_file2_name = file;
    m_histo2_name = name;
}

void Comparator::SetRegionNames(const std::vector<std::string>& names) {
    if (names.size() == 0) {
        std::cerr << "Comparator::SetRegionNames: Passed name vector is empty!" << std::endl;
        exit(EXIT_FAILURE);
    }
    m_regions = names;
}

void Comparator::OpenRootFiles() {
    const std::string path1 = m_folder1_name+"/"+m_file1_name+".root";
    const std::string path2 = m_folder2_name+"/"+m_file2_name+".root";
    m_file1 = std::unique_ptr<TFile>(TFile::Open(path1.c_str()));
    m_file2 = std::unique_ptr<TFile>(TFile::Open(path2.c_str()));

    if (!m_file1 || !m_file2) {
        std::cout << "Comparator::OpenRootFiles: Cannot open the ROOT files" << std::endl;
        exit(EXIT_FAILURE);
    }
}

void Comparator::GetHistoList() {
    m_file1->cd(m_histo1_name.c_str());

    TIter next(gDirectory->GetListOfKeys());
    TKey *key;
    while ((key = static_cast<TKey*>(next()))) {
        if (key->IsFolder()) continue;
        std::string str = key->GetName();
        std::string substr ("_s_");
        if (str.find(substr) != std::string::npos) continue;
        m_histo_list.emplace_back(str);
    }

    delete key;
}

void Comparator::PlotAllHistos() {
    const std::string out_name = m_file1_name+"-"+m_histo1_name+"_VS_"+m_file2_name+"-"+m_histo2_name;
    gSystem->mkdir(("Comparison/"+out_name).c_str());
    
    for (const auto& iname : m_histo_list) {
        std::cout << "Comparator::PlotAllHistos: Plotting: " << iname << "\n";
        PlotSingleHisto(iname);
    }
}

void Comparator::CloseRootFiles() {
    m_file1->Close();
    m_file2->Close();
}

void Comparator::PlotSingleHisto(const std::string& name) const {
    TH1D* h1 = static_cast<TH1D*>(m_file1->Get((m_histo1_name+"/"+name).c_str()));
    TH1D* h2 = static_cast<TH1D*>(m_file2->Get((m_histo2_name+"/"+name).c_str()));

    if (!h1 || !h2) {
        std::cout << "Comparator::PlotSingleHisto: Cannot read one of the histograms, skipping\n";
        return;
    }

    if (m_normalise) {
        h1->Scale(1./h1->Integral());
        h2->Scale(1./h2->Integral());
    }

    auto itr = m_labels_map.find(name);
    if (itr == m_labels_map.end()) {
        std::cout << "Comparator::PlotSingleHisto: Cannot find labels map for: " << name << ", skipping\n";
        return;
    }

    const Labels label = itr->second;
    
    /// Canvas
    TCanvas c("","",800,600);
    TPad pad1("pad1","pad1",0.0, 0.3, 1.0, 1.00);
    TPad pad2("pad2","pad2", 0.0, 0.010, 1.0, 0.3);
    pad1.SetBottomMargin(0.001);
    pad1.SetBorderMode(0);
    pad2.SetBottomMargin(0.5);
    pad1.SetTicks(1,1);
    pad2.SetTicks(1,1);
    pad1.Draw();
    pad2.Draw();

    pad1.cd();

    h1->SetLineColor(kBlack);
    h2->SetLineColor(kRed);

    const float max = Common::GetMax(h1,h2);
    h1->GetYaxis()->SetRangeUser(0.0001, 1.7*max);
    const float bin_width = h1->GetBinWidth(1);
    if (m_normalise) {
        if (bin_width < 1) {
            h1->GetYaxis()->SetTitle(Form("Normalised / %.2f %s", bin_width, label.units.c_str()));
        } else {
            h1->GetYaxis()->SetTitle(Form("Normalised / %.1f %s", bin_width, label.units.c_str()));
        }
    } else {
        if (bin_width < 1) {
            h1->GetYaxis()->SetTitle(Form("Events / %.2f %s", bin_width, label.units.c_str()));
        } else {
            h1->GetYaxis()->SetTitle(Form("Events / %.1f %s", bin_width, label.units.c_str()));
        }
    }

    h1->GetYaxis()->SetLabelSize(0.05);
    h1->GetYaxis()->SetLabelFont(42);
    h1->GetYaxis()->SetTitleFont(42);
    h1->GetYaxis()->SetTitleSize(0.07);
    h1->GetYaxis()->SetTitleOffset(1.1);
    h1->Draw("HIST");
    h2->Draw("HIST same");

    TLatex l1;
    l1.SetTextAlign(9);
    l1.SetTextFont(72);
    l1.SetTextSize(0.06);
    l1.SetNDC();
    l1.DrawLatex(0.21, 0.88, "ATLAS");

    TLatex l2;
    l2.SetTextAlign(9);
    l2.SetTextSize(0.06);
    l2.SetTextFont(42);
    l2.SetNDC();
    l2.DrawLatex(0.33, 0.88, "Internal");
    
    TLatex l3;
    l3.SetTextAlign(9);
    l3.SetTextSize(0.06);
    l3.SetTextFont(42);
    l3.SetNDC();
    l3.DrawLatex(0.21, 0.81, "#sqrt{s} = 13 TeV");

    TLatex l4;
    l4.SetTextAlign(9);
    l4.SetTextSize(0.04);
    l4.SetTextFont(42);
    l4.SetNDC();
    if (name.find("response") != std::string::npos) {
        if (name.find("eta_0_") != std::string::npos) l4.DrawLatex(0.21, 0.74, "|#eta| < 0.8");
        else if (name.find("eta_1_") != std::string::npos) l4.DrawLatex(0.21, 0.74, "0.8 < |#eta| < 1.3"); 
        else if (name.find("eta_2_") != std::string::npos) l4.DrawLatex(0.21, 0.74, "1.3 < |#eta| < 2.5"); 

        if (name.find("pt_0_") != std::string::npos) l4.DrawLatex(0.21, 0.67, "0 < p_{T} < 20 [GeV]");
        else if (name.find("pt_1_") != std::string::npos) l4.DrawLatex(0.21, 0.67, "20 < p_{T} < 30 [GeV]");
        else if (name.find("pt_2_") != std::string::npos) l4.DrawLatex(0.21, 0.67, "30 < p_{T} < 50 [GeV]");
        else if (name.find("pt_3_") != std::string::npos) l4.DrawLatex(0.21, 0.67, "50 < p_{T} < 70 [GeV]");
        else if (name.find("pt_4_") != std::string::npos) l4.DrawLatex(0.21, 0.67, "70 < p_{T} < 100 [GeV]");
        else if (name.find("pt_5_") != std::string::npos) l4.DrawLatex(0.21, 0.67, "p_{T} > 100 [GeV]");
    }
    else if (name.find("pt_20_30") != std::string::npos) l4.DrawLatex(0.21, 0.74, "20 < p_{T} < 30 [GeV], |#eta| < 0.8");
    else if (name.find("pt_30_50") != std::string::npos) l4.DrawLatex(0.21, 0.74, "30 < p_{T} < 50 [GeV], |#eta| < 0.8");
    else if (name.find("pt_50_70") != std::string::npos) l4.DrawLatex(0.21, 0.74, "50 < p_{T} < 70 [GeV], |#eta| < 0.8");
    else l4.DrawLatex(0.21, 0.74, "Inclusive");


    TLegend leg(0.55, 0.75, 0.9, 0.92);
    if (m_file1_name != m_file2_name) {
        leg.AddEntry(h1, m_file1_name.c_str(), "l");
        leg.AddEntry(h2, m_file2_name.c_str(), "l");
        leg.SetFillColor(0);
        leg.SetLineColor(0);
        leg.SetBorderSize(0);
        leg.SetTextFont(72);
        leg.SetTextSize(0.045);
        leg.Draw("same");
    }
    
    TLegend leg2(0.55, 0.60, 0.90, 0.70);
    if (m_folder1_name != m_folder2_name) {
        leg2.AddEntry(h1, m_folder1_name.c_str(), "l");
        leg2.AddEntry(h2, m_folder2_name.c_str(), "l");
        leg2.SetFillColor(0);
        leg2.SetLineColor(0);
        leg2.SetBorderSize(0);
        leg2.SetTextFont(72);
        leg2.SetTextSize(0.045);
        leg2.Draw("same");
    }

    TLegend leg3(0.55, 0.75, 0.9, 0.92);
    if (m_histo1_name != m_histo2_name) {
        leg3.AddEntry(h1, m_histo1_name.c_str(), "l");
        leg3.AddEntry(h2, m_histo2_name.c_str(), "l");
        leg3.SetFillColor(0);
        leg3.SetLineColor(0);
        leg3.SetBorderSize(0);
        leg3.SetTextFont(72);
        leg3.SetTextSize(0.045);
        leg3.Draw("same");
    }
    
    pad1.RedrawAxis();

    pad2.cd();
    std::unique_ptr<TH1D> ratio(static_cast<TH1D*>(h1->Clone()));
    ratio->Divide(h2);
    
    ratio->GetXaxis()->SetLabelFont(42);
    ratio->GetXaxis()->SetLabelSize(0.15);
    ratio->GetXaxis()->SetLabelOffset(0.01);
    ratio->GetXaxis()->SetTitleFont(42);
    ratio->GetXaxis()->SetTitleSize(0.20);
    ratio->GetXaxis()->SetTitleOffset(0.9);
    ratio->GetXaxis()->SetNdivisions(505);
    ratio->GetXaxis()->SetTitle(label.x_axis.c_str());
    ratio->GetYaxis()->SetRangeUser(0.9,1.1);
    ratio->GetYaxis()->SetLabelFont(42);
    ratio->GetYaxis()->SetLabelSize(0.15);
    ratio->GetYaxis()->SetLabelOffset(0.03);
    ratio->GetYaxis()->SetTitleFont(42);
    ratio->GetYaxis()->SetTitleSize(0.15);
    ratio->GetYaxis()->SetTitleOffset(0.5);
    ratio->GetYaxis()->SetNdivisions(505);
    ratio->GetYaxis()->SetTitle("ratio");
    
    ratio->Draw("HIST");
    
    /// Draw line
    const float min_line = ratio->GetXaxis()->GetBinLowEdge(1);
    const float max_line = ratio->GetXaxis()->GetBinUpEdge(ratio->GetNbinsX());
    TLine line(min_line, 1, max_line, 1);
    line.SetLineColor(kRed);
    line.SetLineStyle(2);
    line.SetLineWidth(3);
    line.Draw("same");

    pad2.RedrawAxis();

    const std::string out_name = m_file1_name+"-"+m_histo1_name+"_VS_"+m_file2_name+"-"+m_histo2_name;

    if (m_normalise) {
        c.Print(("Comparison/"+out_name+"/"+name+"_normalised.png").c_str());
        //c.Print(("Comparison/"+out_name+"/"+name+"_normalised.pdf").c_str());
    } else {
        c.Print(("Comparison/"+out_name+"/"+name+".png").c_str());
        //c.Print(("Comparison/"+out_name+"/"+name+".pdf").c_str());
    }

}

void Comparator::AddAllLabels() {
    for (const auto& ireg : m_regions) {
        AddSingleLabel("W_mass_"+ireg, "W mass [GeV]", "GeV");
        AddSingleLabel("W_mass_no_spanet_"+ireg, "W mass [GeV] - no spanet", "GeV");
        AddSingleLabel("W_mass_peak_"+ireg, "W mass [GeV]", "GeV");
        AddSingleLabel("number_of_jets_"+ireg, "jet multiplicity [-]", "");
        AddSingleLabel("number_of_b_jets_"+ireg, "b-jet multiplicity [-]", "");
        AddSingleLabel("electron_pt_"+ireg, "electron p_{T} [GeV]", "GeV");
        AddSingleLabel("electron_eta_"+ireg, "electron #eta [-]", "");
        AddSingleLabel("muon_pt_"+ireg, "muon p_{T} [GeV]", "GeV");
        AddSingleLabel("muon_eta_"+ireg, "muon #eta [-]", "");
        AddSingleLabel("leading_jet_pt_"+ireg, "leading jet p_{T} [GeV]", "GeV");
        AddSingleLabel("leading_jet_eta_"+ireg, "leading jet #eta [-]", "");
        AddSingleLabel("second_leading_jet_pt_"+ireg, "2nd leading jet p_{T} [GeV]", "GeV");
        AddSingleLabel("second_leading_jet_eta_"+ireg, "2nd leading jet #eta [-]", "");
        AddSingleLabel("third_leading_jet_pt_"+ireg, "3rd leading jet p_{T} [GeV]", "GeV");
        AddSingleLabel("third_leading_jet_eta_"+ireg, "3rd leading jet #eta [-]", "");
        AddSingleLabel("fourth_leading_jet_pt_"+ireg, "4th leading jet p_{T} [GeV]", "GeV");
        AddSingleLabel("fourth_leading_jet_eta_"+ireg, "4th leading jet #eta [-]", "");
        AddSingleLabel("leading_bjet_pt_"+ireg, "leading b-jet p_{T} [GeV]", "GeV");
        AddSingleLabel("leading_bjet_eta_"+ireg, "leading b-jet #eta [-]", "");
        AddSingleLabel("second_leading_bjet_pt_"+ireg, "2nd leading b-jet p_{T} [GeV]", "GeV");
        AddSingleLabel("second_leading_bjet_eta_"+ireg, "2nd leading b-jet #eta [-]", "");
        AddSingleLabel("met_"+ireg, "E_{T}^{miss} [GeV]", "GeV");
        AddSingleLabel("met_phi_"+ireg, "E_{T}^{miss} #phi [-]", "");
        AddSingleLabel("spanet_had_top_detection_"+ireg, "Spanet had top detection [-]", "");
        AddSingleLabel("spanet_had_top_assignment_"+ireg, "Spanet had top assignment [-]", "");
        AddSingleLabel("truth_w_mass_no_spanet_"+ireg, "truth W mass [GeV]", "GeV");
        AddSingleLabel("best_Chi2_"+ireg, "Best #chi^[2] [-]", "");
        AddSingleLabel("electron_met_"+ireg, "elecron MET [-]", "");
        AddSingleLabel("muon_met_"+ireg, "muon MET [GeV]", "GeV");
        AddSingleLabel("average_interactions_per_crossing_"+ireg, "average #mu [-]", "");
        AddSingleLabel("actual_interactions_per_crossing_"+ireg, "actual #mu [-]", "");
        AddSingleLabel("transverse_W_mass_"+ireg, "Transverse mass of the W [GeV]", "GeV");
        AddSingleLabel("IsoFixedCone5PtPUsub_"+ireg, "IsoFixedCone5PtPUsub", "");
        AddSingleLabel("jet1_from_W_IsoFixedCone5PtPUsub_"+ireg, "IsoFixedCone5PtPUsub", "");
        AddSingleLabel("jet2_from_W_IsoFixedCone5PtPUsub_"+ireg, "IsoFixedCone5PtPUsub", "");
    }

    for (auto eta = 0; eta < 3; ++ eta) {
        for (auto pt = 0; pt < 6; ++ pt) {
            AddSingleLabel("jet_response_eta_"+std::to_string(eta)+"_pt_"+std::to_string(pt)+"_light_jet_Inclusive", "light jet reco p_{T}/truth p_{T} [-]", "");
            AddSingleLabel("jet_response_eta_"+std::to_string(eta)+"_pt_"+std::to_string(pt)+"_cjet_Inclusive", "c-jet reco p_{T}/truth p_{T} [-]", "");
            AddSingleLabel("jet_response_eta_"+std::to_string(eta)+"_pt_"+std::to_string(pt)+"_bjet_Inclusive", "b-jet reco p_{T}/truth p_{T} [-]", "");
            AddSingleLabel("jet_response_eta_"+std::to_string(eta)+"_pt_"+std::to_string(pt)+"_gluon_jet_Inclusive", "gluon jet reco p_{T}/truth p_{T} [-]", "");
        }
    }
}

void Comparator::AddSingleLabel(const std::string& name,
                                const std::string& x,
                                const std::string& units) {
    Labels label;
    label.x_axis = x;
    label.units = units;

    m_labels_map[name] = label;
}
