#include "WmassJES/FoldingParameter.h"

FoldingParameter::FoldingParameter(const int& steps,
                                   const float& min,
                                   const float& max) : 
    m_float_steps(steps),
    m_float_min(min),
    m_float_max(max) {}

int FoldingParameter::GetSteps() const {
    return m_float_steps;
}

float FoldingParameter::GetMin() const {
    return m_float_min;
}

float FoldingParameter::GetMax() const {
    return m_float_max;
}

float FoldingParameter::GetDelta() const {
    return static_cast<float>((m_float_max - m_float_min)/(m_float_steps - 1));
}
