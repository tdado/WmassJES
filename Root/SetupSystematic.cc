#include "WmassJES/SetupSystematic.h"

#include <algorithm>
#include <stdexcept>
#include <iostream>

std::vector<std::string> SetupSystematic::GetListOfTtbarSamples() {
  return {"ttbar_PP8_FS","ttbar_PP8_AF","ttbar_PowhegHerwig_AF","ttbar_PowhegHerwig_FS_old","ttbar_pthard_AF"};
}

std::vector<std::string> SetupSystematic::GetListOfBackgroundSamples() {
  return {"SingleTop_s_PP8_FS","SingleTop_t_PP8_FS","SingleTop_tW_DR_PP8_FS","Wjets_Sherpa_FS","Zjets_Sherpa_FS","Diboson_Sherpa_FS"}; //SingleTop_tW_DR_PP8_FS is fast sim for now, Multijet is missing
}

std::vector<std::string> SetupSystematic::GetListOfSpecialSamples() {
  return {"ttbar_PP8_AF", "ttbar_PowhegHerwig_AF","ttbar_PowhegHerwig_FS_old", 
          "ttbar_pthard_AF", "ttbar_hdamp_AF", 
          "ttbar_recoil_to_top_AF", "ttbar_CR0_AF", "ttbar_CR1_AF", "ttbar_CR2_AF",
          "ttbar_mtop_172p0_AF", "ttbar_mtop_173p0_AF", 
          "ttbar_Var1Down_AF", "ttbar_Var1Up_AF",
          "ttbar_ERD_AF", "ttbar_ERDon_AF",
          "SingleTop_tW_DR_PP8_AF", "SingleTop_tW_PowhegHerwig_AF", 
          "SingleTop_tW_pthard_AF", "SingleTop_tW_DS_PP8_FS",
          "SingleTop_tW_mtop_172p0_FS", "SingleTop_tW_mtop_173p0_FS"};
}

std::string SetupSystematic::NominalTtbarSample() {
  return "ttbar_PP8_FS";
}

std::vector<Systematic> SetupSystematic::GetAllSystematics(const bool forPlotting, const bool is_jes) {

  const std::vector<std::string> ttbarSamples = SetupSystematic::GetListOfTtbarSamples();
  auto itr = std::find(ttbarSamples.begin(), ttbarSamples.end(), SetupSystematic::NominalTtbarSample());

  if (itr == ttbarSamples.end()) {
    throw std::invalid_argument("Nominal ttbar sample not in the list of the ttbar samples");
  }

  std::vector<std::string> all(SetupSystematic::GetListOfBackgroundSamples());
  all.emplace_back(*itr);
  SystHistoManager systManager{};
  for (const auto& ifile : all) {
    if (ifile == "Multijet") continue;

    // FTag
    for (int ib = 0; ib < 85; ++ib) {
        systManager.AddTwoSidedSyst(ifile, "", "FT_EFF_Eigen_B_"+std::to_string(ib)+"__1up", "FT_EFF_Eigen_B_"+std::to_string(ib)+"__1down", "", "NOSYS", ifile, "Btag_B_"+std::to_string(ib), "BTag");
    }
    for (int ic = 0; ic < 56; ++ic) {
        systManager.AddTwoSidedSyst(ifile, "", "FT_EFF_Eigen_C_"+std::to_string(ic)+"__1up", "FT_EFF_Eigen_C_"+std::to_string(ic)+"__1down", "", "NOSYS", ifile, "Btag_C_"+std::to_string(ic), "BTag");
    }
    for (int ilight = 0; ilight < 42; ++ilight) {
        systManager.AddTwoSidedSyst(ifile, "", "FT_EFF_Eigen_Light_"+std::to_string(ilight)+"__1up", "FT_EFF_Eigen_Light_"+std::to_string(ilight)+"__1down", "", "NOSYS", ifile, "Btag_Light_"+std::to_string(ilight), "BTag");
    }
    ///systManager.AddTwoSidedSyst(ifile, "", "FT_EFF_extrapolation__1up", "FT_EFF_extrapolation__1down", "", "NOSYS", ifile, "Btag_pt_extrapolation", "BTag");
    ///systManager.AddTwoSidedSyst(ifile, "", "FT_EFF_extrapolation_from_charm__1up", "FT_EFF_extrapolation_from_charm__1up", "", "NOSYS", ifile, "Btag_charm_pt_extrapolation", "BTag");

    // electron
    systManager.AddTwoSidedSyst(ifile, "", "EG_RESOLUTION_ALL__1up", "EG_RESOLUTION_ALL__1down", "", "NOSYS", ifile, "Egamma_resolution", "Electron");
    systManager.AddTwoSidedSyst(ifile, "", "EG_SCALE_ALL__1up", "EG_SCALE_ALL__1down", "", "NOSYS", ifile, "Egamma_scale", "Electron");
    //systManager.AddTwoSidedSyst(ifile, "", "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up", "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down", "", "NOSYS", ifile, "Electron_ID", "Electron");
    //systManager.AddTwoSidedSyst(ifile, "", "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up", "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down", "", "NOSYS", ifile, "Electron_ISO", "Electron");
    //systManager.AddTwoSidedSyst(ifile, "", "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up", "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down", "", "NOSYS", ifile, "Electron_RECO", "Electron");
    systManager.AddTwoSidedSyst(ifile, "", "EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up", "EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down", "", "NOSYS", ifile, "Electron_Trigger", "Electron");

    // muon
    systManager.AddTwoSidedSyst(ifile, "", "MUON_CB__1up", "MUON_CB__1down", "", "NOSYS", ifile, "Muon_CB_resolution", "Muon");
    systManager.AddTwoSidedSyst(ifile, "", "MUON_SCALE__1up", "MUON_SCALE__1down", "", "NOSYS", ifile, "Muon_scale", "Muon");
    systManager.AddTwoSidedSyst(ifile, "", "MUON_SAGITTA_DATASTAT__1up", "MUON_SAGITTA_DATASTAT__1down", "", "NOSYS", ifile, "Muon_sagitta_stat", "Muon");
    systManager.AddTwoSidedSyst(ifile, "", "MUON_SAGITTA_GLOBAL__1up", "MUON_SAGITTA_GLOBAL__1down", "", "NOSYS", ifile, "Muon_sagitta_global", "Muon");
    systManager.AddTwoSidedSyst(ifile, "", "MUON_SAGITTA_PTEXTRA__1up", "MUON_SAGITTA_PTEXTRA__1down", "", "NOSYS", ifile, "Muon_sagitta_pt", "Muon");
    systManager.AddTwoSidedSyst(ifile, "", "MUON_SAGITTA_RESBIAS__1up", "MUON_SAGITTA_RESBIAS__1down", "", "NOSYS", ifile, "Muon_sagitta_residual_bias", "Muon");
    systManager.AddTwoSidedSyst(ifile, "", "MUON_EFF_RECO_STAT__1up", "MUON_EFF_RECO_STAT__1down", "", "NOSYS", ifile, "Muon_ID_stat", "Muon");
    systManager.AddTwoSidedSyst(ifile, "", "MUON_EFF_RECO_SYS__1up", "MUON_EFF_RECO_SYS__1down", "", "NOSYS", ifile, "Muon_ID_syst", "Muon");
    systManager.AddTwoSidedSyst(ifile, "", "MUON_EFF_ISO_STAT__1up", "MUON_EFF_ISO_STAT__1down", "", "NOSYS", ifile, "Muon_ISO_stat", "Muon");
    systManager.AddTwoSidedSyst(ifile, "", "MUON_EFF_ISO_SYS__1up", "MUON_EFF_ISO_SYS__1down", "", "NOSYS", ifile, "Muon_ISO_syst", "Muon");
    systManager.AddTwoSidedSyst(ifile, "", "MUON_EFF_TTVA_STAT__1up", "MUON_EFF_TTVA_STAT__1down", "", "NOSYS", ifile, "Muon_TTVA_stat", "Muon");
    systManager.AddTwoSidedSyst(ifile, "", "MUON_EFF_TTVA_SYS__1up", "MUON_EFF_TTVA_SYS__1down", "", "NOSYS", ifile, "Muon_TTVA_syst", "Muon");
    systManager.AddTwoSidedSyst(ifile, "", "MUON_EFF_TrigStatUncertainty__1up", "MUON_EFF_TrigStatUncertainty__1down", "", "NOSYS", ifile, "Muon_Trigger_stat", "Muon");
    systManager.AddTwoSidedSyst(ifile, "", "MUON_EFF_TrigSystUncertainty__1up", "MUON_EFF_TrigSystUncertainty__1down", "", "NOSYS", ifile, "Muon_Trigger_syst", "Muon");

    // MET
    systManager.AddTwoSidedSyst(ifile, "", "MET_SoftTrk_Scale__1up", "MET_SoftTrk_Scale__1down", "", "NOSYS", ifile, "MET_scale", "MET");
    systManager.AddOneSidedSyst(ifile, "MET_SoftTrk_ResoPara", ifile, "NOSYS", ifile, "MET_soft_term_resolution_parallel", "MET");
    systManager.AddOneSidedSyst(ifile, "MET_SoftTrk_ResoPerp", ifile, "NOSYS", ifile, "MET_soft_term_resolution_perpendicular", "MET");

    // PRW
    //systManager.AddTwoSidedSyst(ifile, "", "PRW_DATASF__1up", "PRW_DATASF__1down", "", "NOSYS", ifile, "Pileup_reweighting", "Other");

    // add dedicated uncertainties for plotting
    if (forPlotting || !is_jes) {
      // JES
      systManager.AddTwoSidedSyst(ifile, "", "JET_BJES_Response__1up", "JET_BJES_Response__1down", "", "NOSYS", ifile, "Jet_Bjes_response", "JES");
      //systManager.AddTwoSidedSyst(ifile, "", "JET_JESUnc_Noise_PreRec__1up", "JET_JESUnc_Noise_PreRec__1down", "", "NOSYS", ifile, "Jet_JESUnc_noise_PreRec", "JES");
      //systManager.AddTwoSidedSyst(ifile, "", "JET_JESUnc_VertexingAlg_PreRec__1up", "JET_JESUnc_VertexingAlg_PreRec__1down", "", "NOSYS", ifile, "Jet_JESUnc_VertexingAlg_PreRec", "JES");
      //systManager.AddTwoSidedSyst(ifile, "", "JET_JESUnc_mc20vsmc21_MC20_PreRec__1up", "JET_JESUnc_mc20vsmc21_MC20_PreRec__1down", "", "NOSYS", ifile, "Jet_JESUnc_mc20vsmc21_MC20_PreRec", "JES");
      systManager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Detector1__1up", "JET_EffectiveNP_Detector1__1down", "", "NOSYS", ifile, "Jet_EffectiveNP_Detector1", "JES");
      systManager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Detector2__1up", "JET_EffectiveNP_Detector2__1down", "", "NOSYS", ifile, "Jet_EffectiveNP_Detector2", "JES");
      systManager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Mixed1__1up", "JET_EffectiveNP_Mixed1__1down", "", "NOSYS", ifile, "Jet_EffectiveNP_Mixed1", "JES");
      systManager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Mixed2__1up", "JET_EffectiveNP_Mixed2__1down", "", "NOSYS", ifile, "Jet_EffectiveNP_Mixed2", "JES");
      systManager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Mixed3__1up", "JET_EffectiveNP_Mixed3__1down", "", "NOSYS", ifile, "Jet_EffectiveNP_Mixed3", "JES");
      systManager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Modelling1__1up", "JET_EffectiveNP_Modelling1__1down", "", "NOSYS", ifile, "Jet_EffectiveNP_Modelling1", "JES");
      systManager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Modelling2__1up", "JET_EffectiveNP_Modelling2__1down", "", "NOSYS", ifile, "Jet_EffectiveNP_Modelling2", "JES");
      systManager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Modelling3__1up", "JET_EffectiveNP_Modelling3__1down", "", "NOSYS", ifile, "Jet_EffectiveNP_Modelling3", "JES");
      systManager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Modelling4__1up", "JET_EffectiveNP_Modelling4__1down", "", "NOSYS", ifile, "Jet_EffectiveNP_Modelling4", "JES");
      systManager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Statistical1__1up", "JET_EffectiveNP_Statistical1__1down", "", "NOSYS", ifile, "Jet_EffectiveNP_Statistical1", "JES");
      systManager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Statistical2__1up", "JET_EffectiveNP_Statistical2__1down", "", "NOSYS", ifile, "Jet_EffectiveNP_Statistical2", "JES");
      systManager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Statistical3__1up", "JET_EffectiveNP_Statistical3__1down", "", "NOSYS", ifile, "Jet_EffectiveNP_Statistical3", "JES");
      systManager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Statistical4__1up", "JET_EffectiveNP_Statistical4__1down", "", "NOSYS", ifile, "Jet_EffectiveNP_Statistical4", "JES");
      systManager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Statistical5__1up", "JET_EffectiveNP_Statistical5__1down", "", "NOSYS", ifile, "Jet_EffectiveNP_Statistical5", "JES");
      systManager.AddTwoSidedSyst(ifile, "", "JET_EffectiveNP_Statistical6__1up", "JET_EffectiveNP_Statistical6__1down", "", "NOSYS", ifile, "Jet_EffectiveNP_Statistical6", "JES");
      systManager.AddTwoSidedSyst(ifile, "", "JET_EtaIntercalibration_Modelling__1up", "JET_EtaIntercalibration_Modelling__1down", "", "NOSYS", ifile, "Jet_EtaIntercalibration_Modelling", "JES");
      systManager.AddTwoSidedSyst(ifile, "", "JET_EtaIntercalibration_NonClosure_PreRec__1up", "JET_EtaIntercalibration_NonClosure_PreRec__1down", "", "NOSYS", ifile, "JET_EtaIntercalibration_NonClosure_PreRec", "JES");
      systManager.AddTwoSidedSyst(ifile, "", "JET_EtaIntercalibration_TotalStat__1up", "JET_EtaIntercalibration_TotalStat__1down", "", "NOSYS", ifile, "Jet_EtaIntercalibration_TotalStat", "JES");
      systManager.AddTwoSidedSyst(ifile, "", "JET_Flavor_Composition__1up", "JET_Flavor_Composition__1down", "", "NOSYS", ifile, "JET_Flavor_Composition", "JES");
      systManager.AddTwoSidedSyst(ifile, "", "JET_Flavor_Response__1up", "JET_Flavor_Response__1down", "", "NOSYS", ifile, "JET_Flavor_Response", "JES");
      //systManager.AddTwoSidedSyst(ifile, "", "JET_InSitu_NonClosure_PreRec__1up", "JET_InSitu_NonClosure_PreRec__1down", "", "NOSYS", ifile, "JET_InSitu_NonClosure_PreRec", "JES");
    }

    if (forPlotting || is_jes) {
      // JER (special treatment)
      //systManager.AddTwoSidedSyst(ifile, "", "JET_JERUnc_Noise_PreRec__1up", "JET_JERUnc_Noise_PreRec__1down", "", "NOSYS", ifile, "Jet_JERUnc_noise_PreRec", "JER", "JET_JERUnc_Noise_PreRec_PseudoData__1up", "JET_JERUnc_Noise_PreRec_PseudoData__1down");
      //systManager.AddTwoSidedSyst(ifile, "", "JET_JERUnc_mc20vsmc21_MC20_PreRec__1up", "JET_JERUnc_mc20vsmc21_MC20_PreRec__1down", "", "NOSYS", ifile, "Jet_JERUnc_mc20vsmc21_MC20_PreRec", "JER", "JET_JERUnc_mc20vsmc21_MC20_PreRec_PseudoData__1up", "JET_JERUnc_mc20vsmc21_MC20_PreRec_PseudoData__1down");
      systManager.AddTwoSidedSyst(ifile, "", "JET_JER_DataVsMC_MC16__1up", "JET_JER_DataVsMC_MC16__1down", "", "NOSYS", ifile, "Jet_JER_DataVsMC_MC16", "JER", "JET_JER_DataVsMC_MC16_PseudoData__1up", "JET_JER_DataVsMC_MC16_PseudoData__1down");
      systManager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_1__1up", "JET_JER_EffectiveNP_1__1down", "", "NOSYS", ifile, "Jet_JER_effectiveNP_1", "JER", "JET_JER_EffectiveNP_1_PseudoData__1up", "JET_JER_EffectiveNP_1_PseudoData__1down");
      systManager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_2__1up", "JET_JER_EffectiveNP_2__1down", "", "NOSYS", ifile, "Jet_JER_effectiveNP_2", "JER", "JET_JER_EffectiveNP_2_PseudoData__1up", "JET_JER_EffectiveNP_2_PseudoData__1down");
      systManager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_3__1up", "JET_JER_EffectiveNP_3__1down", "", "NOSYS", ifile, "Jet_JER_effectiveNP_3", "JER", "JET_JER_EffectiveNP_3_PseudoData__1up", "JET_JER_EffectiveNP_3_PseudoData__1down");
      systManager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_4__1up", "JET_JER_EffectiveNP_4__1down", "", "NOSYS", ifile, "Jet_JER_effectiveNP_4", "JER", "JET_JER_EffectiveNP_4_PseudoData__1up", "JET_JER_EffectiveNP_4_PseudoData__1down");
      systManager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_5__1up", "JET_JER_EffectiveNP_5__1down", "", "NOSYS", ifile, "Jet_JER_effectiveNP_5", "JER", "JET_JER_EffectiveNP_5_PseudoData__1up", "JET_JER_EffectiveNP_5_PseudoData__1down");
      systManager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_6__1up", "JET_JER_EffectiveNP_6__1down", "", "NOSYS", ifile, "Jet_JER_effectiveNP_6", "JER", "JET_JER_EffectiveNP_6_PseudoData__1up", "JET_JER_EffectiveNP_6_PseudoData__1down");
      systManager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_7__1up", "JET_JER_EffectiveNP_7__1down", "", "NOSYS", ifile, "Jet_JER_effectiveNP_7", "JER", "JET_JER_EffectiveNP_7_PseudoData__1up", "JET_JER_EffectiveNP_7_PseudoData__1down");
      systManager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_8__1up", "JET_JER_EffectiveNP_8__1down", "", "NOSYS", ifile, "Jet_JER_effectiveNP_8", "JER", "JET_JER_EffectiveNP_8_PseudoData__1up", "JET_JER_EffectiveNP_8_PseudoData__1down");
      systManager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_9__1up", "JET_JER_EffectiveNP_9__1down", "", "NOSYS", ifile, "Jet_JER_effectiveNP_9", "JER", "JET_JER_EffectiveNP_9_PseudoData__1up", "JET_JER_EffectiveNP_9_PseudoData__1down");
      systManager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_10__1up", "JET_JER_EffectiveNP_10__1down", "", "NOSYS", ifile, "Jet_JER_effectiveNP_10", "JER", "JET_JER_EffectiveNP_10_PseudoData__1up", "JET_JER_EffectiveNP_10_PseudoData__1down");
      systManager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_11__1up", "JET_JER_EffectiveNP_11__1down", "", "NOSYS", ifile, "Jet_JER_effectiveNP_11", "JER", "JET_JER_EffectiveNP_11_PseudoData__1up", "JET_JER_EffectiveNP_11_PseudoData__1down");
      systManager.AddTwoSidedSyst(ifile, "", "JET_JER_EffectiveNP_12restTerm__1up", "JET_JER_EffectiveNP_12restTerm__1down", "", "NOSYS", ifile, "Jet_JER_effectiveNP_12restTerm", "JER", "JET_JER_EffectiveNP_12restTerm_PseudoData__1up", "JET_JER_EffectiveNP_12restTerm_PseudoData__1down");
    }

    if (forPlotting) {
      systManager.AddNormSyst("ttbar_PP8_FS", 0.05, 0.05, "ttbar_normalisation");
    }

    /// Luminosity
    //systManager.AddNormSyst(ifile, 0.0086, 0.0086, "Luminosity");
  }

  // ttbar modelling
  systManager.AddOneSidedSyst("ttbar_PP8_FS", "NNLO_FOvsPSTopPt", "ttbar_PP8_FS", "NOSYS", "ttbar_PP8_FS", "ttbar_NNLO", "ttbar_NNLO");
  systManager.AddOneSidedSyst("ttbar_hdamp_AF", "NOSYS", "ttbar_PP8_AF", "NOSYS", "ttbar_PP8_FS", "ttbar_hdamp", "ttbar_hdamp");
  systManager.AddOneSidedSyst("ttbar_recoil_to_top_AF", "NOSYS", "ttbar_PP8_AF", "NOSYS", "ttbar_PP8_FS", "ttbar_recoil", "ttbar_recoil");
  systManager.AddOneSidedSyst("ttbar_pthard_AF", "NOSYS", "ttbar_PP8_AF", "NOSYS", "ttbar_PP8_FS", "ttbar_pthard", "ttbar_pthard");
  // TEMPORARY
  systManager.AddOneSidedSyst("ttbar_PP8_FS", "GEN_isrmuRfac10_fsrmuRfac20", "ttbar_PP8_FS", "NOSYS", "ttbar_PP8_FS", "ttbar_alphas_s_FSR", "ttbar_alphas_s_FSR");

  //systManager.AddOneSidedSyst("ttbar_PowhegHerwig_FS_old", "NOSYS", "ttbar_PP8_FS", "NOSYS", "ttbar_PP8_FS", "ttbar_PowhegHerwig", "ttbar_PowhegHerwig");
  systManager.AddOneSidedSyst("ttbar_PowhegHerwig_AF", "NOSYS", "ttbar_PP8_AF", "NOSYS", "ttbar_PP8_FS", "ttbar_PowhegHerwig", "ttbar_PowhegHerwig");

  // TOO LOW STAT
  systManager.AddOneSidedSyst("ttbar_CR1_AF", "NOSYS", "ttbar_CR0_AF", "NOSYS", "ttbar_PP8_FS", "ttbar_CR1", "ttbar_CR1");
  systManager.AddOneSidedSyst("ttbar_CR2_AF", "NOSYS", "ttbar_CR0_AF", "NOSYS", "ttbar_PP8_FS", "ttbar_CR2", "ttbar_CR2");

  systManager.AddTwoSidedSyst("ttbar_PP8_FS", "ttbar_PP8_FS", "GEN_2muF_NNPDF31_NLO_0118", "GEN_0p5muF_NNPDF31_NLO_0118", "ttbar_PP8_FS", "NOSYS", "ttbar_PP8_FS", "ttbar_mu_f", "ttbar_mu_f");
  systManager.AddTwoSidedSyst("ttbar_PP8_FS", "ttbar_PP8_FS", "GEN_2muR_NNPDF31_NLO_0118", "GEN_0p5muR_NNPDF31_NLO_0118", "ttbar_PP8_FS", "NOSYS", "ttbar_PP8_FS", "ttbar_mu_r", "ttbar_mu_r");
  systManager.AddTwoSidedSyst("ttbar_PP8_FS", "ttbar_PP8_FS", "GEN_Var3cUp", "GEN_Var3cDown", "ttbar_PP8_FS", "NOSYS", "ttbar_PP8_FS", "ttbar_Var3c", "ttbar_Var3c");
  systManager.AddOneSidedSyst("ttbar_mtop_173p0_AF", "NOSYS", "ttbar_PP8_AF", "NOSYS", "ttbar_PP8_FS", "ttbar_top_mass", "ttbar_mass");

  // SingleTop tW modelling
  systManager.AddOneSidedSyst("SingleTop_tW_DS_PP8_FS","NOSYS","SingleTop_tW_DR_PP8_FS","NOSYS","SingleTop_tW_DR_PP8_FS", "DS_vs_DR", "SingleTop_tW_modelling");

  // TEMPORARY
  systManager.AddOneSidedSyst("SingleTop_tW_pthard_AF", "NOSYS", "SingleTop_tW_DR_PP8_FS", "NOSYS", "SingleTop_tW_DR_PP8_FS", "SingleTop_tW_pthard", "SingleTop_tW_modelling");

  systManager.AddOneSidedSyst("SingleTop_tW_PowhegHerwig_AF", "NOSYS", "SingleTop_tW_DR_PP8_AF", "NOSYS", "SingleTop_tW_DR_PP8_FS", "SingleTop_tW_PowhegHerwig", "SingleTop_tW_modelling");
  systManager.AddTwoSidedSyst("SingleTop_tW_mtop_173p0_FS", "SingleTop_tW_mtop_172p0_FS", "NOSYS", "NOSYS", "SingleTop_tW_DR_PP8_FS", "NOSYS", "SingleTop_tW_DR_PP8_FS", "SingleTop_tW_top_mass", "SingleTop_tW_modelling");

  // background normalisation
  systManager.AddNormSyst("SingleTop_s_PP8_FS", 0.05, 0.05, "singletop_s_normalisation");
  systManager.AddNormSyst("SingleTop_t_PP8_FS", 0.05, 0.05, "singletop_t_normalisation");
  systManager.AddNormSyst("SingleTop_tW_DR_PP8_FS", 0.05, 0.05, "singletop_tW_normalisation");
  systManager.AddNormSyst("Wjets_Sherpa_FS", 0.5, 0.5, "Wjets_normalisation");
  systManager.AddNormSyst("Zjets_Sherpa_FS", 0.5, 0.5, "Zjets_normalisation");

  return systManager.GetAllSystematics();
}
