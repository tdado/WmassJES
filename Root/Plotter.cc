#include "WmassJES/Plotter.h"
#include "WmassJES/Common.h"
#include "AtlasUtils/AtlasStyle.h"

#include "TCanvas.h"
#include "TColor.h"
#include "TGraphAsymmErrors.h"
#include "TF1.h"
#include "TH1D.h"
#include "TLatex.h"
#include "TLine.h"
#include "TLegend.h"
#include "TPad.h"
#include "TSystem.h"

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <fstream>

Plotter::Plotter(const std::string& directory) :
    m_directory(directory),
    m_data_name("SetMe"),
    m_data_file(nullptr),
    m_atlas_label("Internal"),
    m_lumi_label("139"),
    m_collection(""),
    m_nominal_ttbar_name(""),
    m_nominal_ttbar_index(999),
    m_syst_shape_only(false),
    m_run_syst(false) {

    SetAtlasStyle();
    FillStyleMap();
    SetColourMap();
    SetLabelMap();
}

void Plotter::SetRegionNames(const std::vector<std::string>& names) {
    if (names.size() == 0) {
        std::cerr << "Plotter::SetRegionNames: Passed name vector is empty!" << std::endl;
        exit(EXIT_FAILURE);
    }
    m_regions = names;
}

void Plotter::SetOutputDirectory(const std::string& output) {
    m_output_directory = output;
}

void Plotter::SetTTbarNames(const std::vector<std::string>& names) {
    if (names.size() == 0) {
        std::cerr << "Plotter::SetTTbarNames: Passed name vector is empty!" << std::endl;
        exit(EXIT_FAILURE);
    }
    m_ttbar_names = names;
}

void Plotter::SetBackgroundNames(const std::vector<std::string>& names) {
    m_background_names = names;
}

void Plotter::SetSpecialNames(const std::vector<std::string>& names) {
    if (names.size() == 0) {
        std::cerr << "Plotter::SetSpecialNames: Passed name vector is empty!" << std::endl;
        exit(EXIT_FAILURE);
    }
    m_special_names = names;
}

void Plotter::SetDataName(const std::string& name) {
    m_data_name = name;
}

void Plotter::SetAtlasLabel(const std::string& label) {
    m_atlas_label = label;
}

void Plotter::SetLumiLabel(const std::string& label) {
    m_lumi_label = label;
}

void Plotter::SetCollection(const std::string& collection) {
    m_collection = collection;
}

void Plotter::SetNominalTTbarName(const std::string& name) {
    m_nominal_ttbar_name = name;

    auto itr = std::find(m_ttbar_names.begin(), m_ttbar_names.end(), name);
    if (itr == m_ttbar_names.end()) {
        std::cerr << "Plotter::SetNominalTTbarName: The nominal ttbar name is not in the list of ttbar names" << std::endl;
        exit(EXIT_FAILURE);
    }

    m_nominal_ttbar_index = std::distance(m_ttbar_names.begin(), itr);
}

void Plotter::OpenRootFiles() {

    /// TTbar files
    for (const auto& ittbar : m_ttbar_names) {
        std::unique_ptr<TFile> f(TFile::Open((m_directory+"/"+ittbar+".root").c_str(), "READ"));
        if (!f) {
            std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  m_directory+"/"+ittbar+".root" << std::endl;
            exit(EXIT_FAILURE);
        }

        m_ttbar_files.emplace_back(std::move(f));
    }

    /// Background files
    for (const auto& ibkg : m_background_names) {
        std::unique_ptr<TFile> f(TFile::Open((m_directory+"/"+ibkg+".root").c_str(), "READ"));
        if (!f) {
            std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  m_directory+"/"+ibkg+".root" << std::endl;
            exit(EXIT_FAILURE);
        }

        m_background_files.emplace_back(std::move(f));
    }

    /// Special files
    for (const auto& ispecial : m_special_names) {
        std::unique_ptr<TFile> f(TFile::Open((m_directory+"/"+ispecial+".root").c_str(), "READ"));
        if (!f) {
            std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  m_directory+"/"+ispecial+".root" << std::endl;
            exit(EXIT_FAILURE);
        }

        m_special_files.emplace_back(std::move(f));
    }

    /// Data File
    m_data_file.reset(TFile::Open((m_directory+"/"+m_data_name+".root").c_str(), "READ"));
    if (!m_data_file) {
        std::cerr << "Plotter::OpenRootFiles: Cannot open file: " <<  m_directory+"/"+m_data_name+".root" << std::endl;
    }

    if ((m_ttbar_files.size() == 0)) {
        std::cerr << "Plotter::OpenRootFiles: TTbar files are empty" << std::endl;
        exit(EXIT_FAILURE);
    }

    /// Create directory
    gSystem->mkdir((m_output_directory).c_str());
    for (const auto& ireg : m_regions) {
        gSystem->mkdir((m_output_directory+"/"+ireg).c_str());
        if (ireg == "Inclusive_no_met_cut") continue;
        gSystem->mkdir((m_output_directory+"/"+ireg+"/DataMCplots").c_str());
    }
    gSystem->mkdir((m_output_directory+"/Inclusive/Response").c_str());
    gSystem->mkdir((m_output_directory+"/Inclusive/ResponseTemplates").c_str());
    gSystem->mkdir((m_output_directory+"/Inclusive_no_met_cut/WmassTemplates").c_str());

}

void Plotter::PlotTruthPlots(const std::string& name, const std::string& axis, const bool& normalise) const {
    std::cout << "Plotter::PlotTruthPlots: Plotting " << name << "\n";
    std::vector<std::unique_ptr<TH1D>>  histos;
    for (const auto& ifile : m_ttbar_files) {
        if (ifile->Get(("nominal/"+name).c_str()) == nullptr) {
            std::cerr << "Cannot read histogram: " << name << "  ignoring.\n";
            continue;
        }
        histos.emplace_back(static_cast<TH1D*>(ifile->Get(("nominal/"+name).c_str())));
    }

    /// normalise to unity
    if (normalise) {
        for (auto& ihist : histos) {
            ihist->Scale(1./ihist->Integral());
        }
    }

    if (histos.size() < 2) return;

    TCanvas canvas("","",800,600);
    canvas.cd();
    TPad pad1("pad1","pad1",0.0, 0.3, 1.0, 1.00);
    TPad pad2("pad2","pad2",0.0, 0.010, 1.0, 0.3);
    pad1.SetBottomMargin(0.001);
    pad1.SetBorderMode(0);
    pad2.SetBottomMargin(0.4);
    pad1.Draw();
    pad2.Draw();

    PlotUpperComparison(histos, &pad1, axis);

    /// Draw legend
    TLegend leg(0.60,0.5,0.75,0.9);
    for (std::size_t ihist = 0; ihist < histos.size(); ++ihist) {
        leg.AddEntry(histos.at(ihist).get(), (m_ttbar_names.at(ihist)).c_str(), "l");
    }
    leg.SetFillColor(0);
    leg.SetLineColor(0);
    leg.SetBorderSize(0);
    leg.SetTextFont(72);
    leg.SetTextSize(0.035);
    leg.Draw("same");

    pad1.RedrawAxis();

    pad2.cd();

    std::vector<std::unique_ptr<TH1D> > ratios;
    for (auto& ihist : histos) {
        ratios.emplace_back(static_cast<TH1D*>(ihist->Clone()));
    }

    PlotRatioComparison(ratios, &pad2, axis, 0.3, true);

    /// Draw line
    const float min = ratios.at(0)->GetXaxis()->GetBinLowEdge(1);
    const float max = ratios.at(0)->GetXaxis()->GetBinUpEdge(ratios.at(0)->GetNbinsX());
    TLine line(min, 1, max, 1);
    line.SetLineColor(kBlack);
    line.SetLineStyle(2);
    line.SetLineWidth(3);
    line.Draw("same");

    pad2.RedrawAxis();

    canvas.Print((m_output_directory+"/"+m_directory+"/Response/"+name+".png").c_str());
    //canvas.Print((m_output_directory+"/"+m_directory+"/Response/"+name+".pdf").c_str());

}

void Plotter::PlotResponsePlots(const std::string& name,
                                const std::string& suffix,
                                const std::string& axis,
                                const bool normalise) const {

    std::cout << "Plotter::PlotResponsePlots: Plotting " << name << "\n";

    std::vector<std::size_t> eta_indices = {0, 1, 2};
    std::vector<std::string> eta_intervals = {"0.0 < |#eta| < 0.8","0.8 < |#eta| < 1.3","1.3 < |#eta| < 2.5"};
    std::vector<std::size_t> pt_indices = {0, 1, 2, 3, 4, 5, 6, 7};
    std::vector<std::string> pt_intervals = {"0 < p_{T} < 20 [GeV]","20 < p_{T} < 35 [GeV]","35 < p_{T} < 50 [GeV]","50 < p_{T} < 70 [GeV]","70 < p_{T} < 100 [GeV]","100 < p_{T} < 150 [GeV]","150 < p_{T} < 200 [GeV]","p_{T} > 200 [GeV]"};

    for (std::size_t i = 0; i < eta_indices.size(); ++i) {
        std::vector<float> fitted_mu;
        std::vector<float> fitted_sigma;
        for (std::size_t j = 0; j < pt_indices.size(); ++j) {
            const std::string long_name = name+"_eta_"+std::to_string(eta_indices.at(i))+"_pt_"+std::to_string(pt_indices.at(j))+"_"+suffix+"_Inclusive";
            std::vector<std::unique_ptr<TH1D> > histos;
            for (const auto& ifile : m_ttbar_files) {
                if (ifile->Get(("NOSYS/"+long_name).c_str()) == nullptr) {
                    std::cerr << "Cannot read histogram: " << "NOSYS/"+long_name << " ignoring.\n";
                    continue;
                }
                histos.emplace_back(static_cast<TH1D*>(ifile->Get(("NOSYS/"+long_name).c_str())));
            }

            /// normalise to unity
            if (normalise) {
                for (auto& ihist : histos) {
                    ihist->Scale(1./ihist->Integral());
                }
            }

            TCanvas canvas("","",800,600);
            canvas.cd();
            TPad pad1("pad1","pad1",0.0, 0.3, 1.0, 1.00);
            TPad pad2("pad2","pad2",0.0, 0.010, 1.0, 0.3);
            pad1.SetBottomMargin(0.001);
            pad1.SetBorderMode(0);
            pad2.SetBottomMargin(0.4);
            pad1.Draw();
            pad2.Draw();

            PlotUpperComparison(histos, &pad1, axis);

            const double mean = histos.at(0)->GetMean();
            const double sigma = histos.at(0)->GetRMS();

            // first run a fit to get the range approximatelly right
            TF1 gaussFirst("func", "gaus", mean - 1.*sigma, mean + 1.*sigma);
            gaussFirst.SetParameter(0, 1.);
            gaussFirst.SetParameter(1, mean);
            gaussFirst.SetParameter(2, sigma);

            const double meanFit = gaussFirst.GetParameter(1);
            const double sigmaFit = gaussFirst.GetParameter(2);

            // run the gaussian fit
            TF1 gauss("func", "gaus", meanFit - sigmaFit, meanFit + sigmaFit);
            gauss.SetLineColor(kRed);
            gauss.SetParameter(0, 1.);
            gauss.SetParameter(1, meanFit);
            gauss.SetParameter(2, sigmaFit);

            histos.at(0)->Fit(&gauss, "RQ");
            gauss.Draw("same");

            if (i == 0) {
                fitted_mu.emplace_back(gauss.GetParameter(1));
                fitted_sigma.emplace_back(gauss.GetParameter(2));
            }

            /// Draw legend
            TLegend leg(0.60,0.7,0.75,0.9);
            for (std::size_t ihist = 0; ihist < histos.size(); ++ihist) {
                leg.AddEntry(histos.at(ihist).get(), (m_ttbar_names.at(ihist)).c_str(), "l");
            }
            leg.SetFillColor(0);
            leg.SetLineColor(0);
            leg.SetBorderSize(0);
            leg.SetTextFont(72);
            leg.SetTextSize(0.035);
            leg.Draw("same");

            TLatex l1;
            l1.SetTextAlign(9);
            l1.SetTextSize(0.05);
            l1.SetTextFont(42);
            l1.SetNDC();
            l1.DrawLatex(0.72, 0.5, (eta_intervals.at(i)).c_str());
            l1.DrawLatex(0.72, 0.43, (pt_intervals.at(j)).c_str());
            l1.DrawLatex(0.72, 0.35, suffix.c_str());

            const float maxY = 0.6*MaxResize(histos, false);
            TLine line2(1.0, 0., 1.0, 0.7*maxY);
            line2.SetLineColor(kRed);
            line2.SetLineStyle(2);
            line2.SetLineWidth(3);
            line2.Draw("same");

            pad1.RedrawAxis();

            pad2.cd();

            std::vector<std::unique_ptr<TH1D> > ratios;
            for (auto& ihist : histos) {
                ratios.emplace_back(static_cast<TH1D*>(ihist->Clone()));
            }

            PlotRatioComparison(ratios, &pad2, axis, 0.3, true);

            /// Draw line
            const float min = ratios.at(0)->GetXaxis()->GetBinLowEdge(1);
            const float max = ratios.at(0)->GetXaxis()->GetBinUpEdge(ratios.at(0)->GetNbinsX());
            TLine line(min, 1, max, 1);
            line.SetLineColor(kBlack);
            line.SetLineStyle(2);
            line.SetLineWidth(3);
            line.Draw("same");

            pad2.RedrawAxis();

            canvas.Print((m_output_directory+"/Inclusive/Response/"+name+"_eta_"+std::to_string(i)+"_pt_"+std::to_string(j)+"_"+suffix+".png").c_str());

        } // loop over pt

        if (i != 0) continue;
        std::cout << "Fitted mu: " << suffix << ":\n";
        std::string mu("{");
        for (const auto imu : fitted_mu) {
            mu += std::to_string(imu) + ", ";
        }
        mu.resize(mu.size() - 2);
        mu += "}";
        std::cout << mu << "\n";
        std::cout << "Fitted sigmas: " << suffix << ":\n";
        std::string s("{");
        for (const auto isig : fitted_sigma) {
            s += std::to_string(isig) + ", ";
        }
        s.resize(s.size() - 2);
        s += "}";
        std::cout << s << "\n\n";
    } // loop over eta
}

void Plotter::PlotTemplatePlots(const std::string& name,
                                const std::string& axis,
                                const std::string& elements,
                                const std::string& units,
                                const bool& is_1D,
                                const bool& is_s,
                                const bool& normalise,
                                const bool& doing_response) const {

    std::vector<std::string> pt_intervals_1D = {"20_35_20_35","35_50_35_50","50_70_50_70","70_100_70_100","100_150_100_150","150_200_150_200"};
    std::vector<std::vector<std::string>> pt_intervals_2D = {{"20_35_35_50","20_35_50_70","20_35_70_100","20_35_100_150","20_35_150_200"},
                                                             {"35_50_50_70","35_50_70_100","35_50_100_150","35_50_150_200"},
                                                             {"50_70_70_100","50_70_100_150","50_70_150_200"},
                                                             {"70_100_100_150","70_100_150_200"},
                                                             {"100_150_150_200"}};
    
    std::vector<int> colours{kOrange, kOrange+1, kAzure-9, kAzure+9, kGray+1};

    if (is_1D) {
        std::size_t number_of_pt_intervals = pt_intervals_1D.size();
        for (std::size_t pt = 0; pt < number_of_pt_intervals; ++pt) {
            std::cout << "Plotter::PlotTemplatePlots: Plotting "<< name << " templates plot for 1D pt interval " << pt_intervals_1D.at(pt) << "\n"; //name = W_mass

            std::vector<std::unique_ptr<TH1D> >  template_histos;

            if (doing_response) {
                colours.at(1) = colours.at(3);
                colours.at(2) = colours.at(4);
            }

            std::vector<std::size_t> indices;
            std::vector<double> values;

            if(is_s) {
                if (doing_response) {
                    indices = {0, 3, 5};
                    values = {0.950, 1.000, 1.05};
                }
                else {
                    indices = {0, 3, 5, 7, 10};
                    values = {0.95, 0.98, 1.00, 1.02, 1.05};
                }
            } else {
                if (doing_response) {
                    indices = {0, 3, 5};
                    values = {0.6, 1.0, 1.4};
                }
                else {
                    indices = {0, 3, 5, 7, 10};
                    values = {0.60, 0.84, 1.00, 1.16, 1.40};
                }
            }
            float nominal_ttbar_histo_integral = 0;
            for (std::size_t i = 0; i < indices.size(); ++i) {
                std::string suffix;
                std::string long_name;
                if (is_s) {
                    if (doing_response) {
                        long_name = name+"_s_"+std::to_string(indices.at(i))+"_isol_0p5";
                        suffix = "_jes";
                    } else {
                        long_name = name+"_s_"+std::to_string(indices.at(i))+"_isol_0p5";
                        suffix = "_jes";
                    }
                } else {
                    if (doing_response) {
                        long_name = name+"_r_"+std::to_string(indices.at(i))+"_isol_0p5";
                        suffix = "_jer";
                    } else {
                        long_name = name+"_r_"+std::to_string(indices.at(i))+"_isol_0p5";
                        suffix = "_jer";
                    }
                }           

                std::string region = "_Inclusive_no_met_cut";
                if (doing_response) region = "_Inclusive";
                std::unique_ptr<TH1D> ttbar_histo(static_cast<TH1D*>(m_ttbar_files.at(m_nominal_ttbar_index)->Get(("NOSYS/"+long_name+"_pt_"+pt_intervals_1D.at(pt)+suffix+region).c_str())));
                if (normalise && i == 2) {
                    nominal_ttbar_histo_integral = ttbar_histo->Integral();
                }
                std::cerr << "Plotter::PlotTemplatePlots: Getting: " << long_name+"_pt_"+pt_intervals_1D.at(pt)+suffix+region <<std::endl;
                if (!ttbar_histo) {
                    std::cerr << "Plotter::PlotTemplatePlots: Cannot read template histo: " << long_name+"_pt_"+pt_intervals_1D.at(pt)+suffix+region << ", skipping\n";
                    return;
                }
                template_histos.emplace_back(std::move(ttbar_histo));
            }

            if (template_histos.size() == 0) {
                std::cerr << "Plotter::PlotTemplatePlots: template_histos size is zero!" << std::endl;
                return;
            }

            if (template_histos.size() > colours.size()) {
                std::cerr << "Plotter::PlotTemplatePlots: more template histos than colours" << std::endl;
                return;
            }

            //normalise number of events in template histos to the nominal histogram s=1, r=1
            if (normalise) {
                for (std::size_t i = 0; i < template_histos.size(); ++i) {
                    template_histos.at(i)->Scale(nominal_ttbar_histo_integral/template_histos.at(i)->Integral());
                }
            }

            if (!doing_response) {
                const float bin_width = template_histos.at(0)->GetBinWidth(1);
                if (bin_width < 1) {
                    template_histos.at(0)->GetYaxis()->SetTitle(Form("%s / %.2f %s", elements.c_str(), bin_width, units.c_str()));
                } else {
                    template_histos.at(0)->GetYaxis()->SetTitle(Form("%s / %.f %s", elements.c_str(), bin_width, units.c_str()));
                }
            }

            /// Canvas
            TCanvas c("","",800,600);
            TPad pad1("pad1","pad1",0.0, 0.3, 1.0, 1.00);
            TPad pad2("pad2","pad2", 0.0, 0.010, 1.0, 0.3);
            pad1.SetBottomMargin(0.001);
            pad1.SetBorderMode(0);
            pad2.SetBottomMargin(0.5);
            pad1.SetTicks(1,1);
            pad2.SetTicks(1,1);
            pad1.Draw();
            pad2.Draw();

            for (std::size_t i = 0; i < template_histos.size(); ++i) {
                template_histos.at(i)->SetLineColor(colours.at(i));
            }

            DrawUpperTemplatePlot(&pad1, template_histos);

            pad1.RedrawAxis();
            /// Draw legend
            TLegend leg(0.70, 0.5, 0.85, 0.9);
            for (std::size_t i = 0; i < template_histos.size(); ++i) {
                std::string label;
                if (is_s) {
                    label = "s =";
                } else {
                    label = "r =";
                }
                leg.AddEntry(template_histos.at(i).get(), Form("%s %.3f", label.c_str(), values.at(i)), "l");
            }

            leg.SetFillColor(0);
            leg.SetLineColor(0);
            leg.SetBorderSize(0);
            leg.SetTextFont(72);
            leg.SetTextSize(0.045);
            leg.Draw("same");

            std::vector<std::unique_ptr<TH1D> > ratios;
            for (const auto& ihist : template_histos) {
                ratios.emplace_back(static_cast<TH1D*>(ihist->Clone()));
            }

            DrawRatioTemplatePlot(&pad2, ratios, axis, is_s, doing_response);

            /// Draw line
            const float min = ratios.at(0)->GetXaxis()->GetBinLowEdge(1);
            const float max = ratios.at(0)->GetXaxis()->GetBinUpEdge(ratios.at(0)->GetNbinsX());
            TLine line (min, 1, max, 1);
            line.SetLineColor(kBlack);
            line.SetLineStyle(2);
            line.SetLineWidth(3);
            line.Draw("same");

            std::string folder = "/Inclusive_no_met_cut/WmassTemplates/";
            if (doing_response) folder = "/Inclusive/ResponseTemplates/";

            if (is_s && normalise) {
                c.Print((m_output_directory+folder+name+"_s_variations_pt_"+pt_intervals_1D.at(pt)+"_normalised.png").c_str());
                //c.Print((m_output_directory+folder+name+"_s_variations_pt_"+pt_intervals_1D.at(pt)+"_normalised.pdf").c_str());
            } else if (is_s){
                c.Print((m_output_directory+folder+name+"_s_variations_pt_"+pt_intervals_1D.at(pt)+".png").c_str());
                //c.Print((m_output_directory+folder+name+"_s_variations_pt_"+pt_intervals_1D.at(pt)+".pdf").c_str());
            } else if (normalise) {
                c.Print((m_output_directory+folder+name+"_r_variations_pt_"+pt_intervals_1D.at(pt)+"_normalised.png").c_str());
                //c.Print((m_output_directory+folder+name+"_r_variations_pt_"+pt_intervals_1D.at(pt)+"_normalised.pdf").c_str());
            } else {
                c.Print((m_output_directory+folder+name+"_r_variations_pt_"+pt_intervals_1D.at(pt)+".png").c_str());
                //c.Print((m_output_directory+folder+name+"_r_variations_pt_"+pt_intervals_1D.at(pt)+".pdf").c_str());
            }
        } // 1D loop over pt intervals
    } else { //if 2D

        for (std::size_t pt_group = 0; pt_group < 5; ++pt_group) {
            for (std::size_t pt_interval = 0; pt_interval < 5 - pt_group; ++pt_interval) {

                std::vector<std::unique_ptr<TH1D> >  template_histos1;
                std::vector<std::unique_ptr<TH1D> >  template_histos2;
        
                std::cout << "Plotter::PlotTemplatePlots: Plotting "<< name << " templates plot for 2D pt interval " << pt_intervals_2D.at(pt_group).at(pt_interval) << "\n"; //name = W_mass

                if (doing_response) {
                    colours.at(1) = colours.at(3);
                    colours.at(2) = colours.at(4);
                }

                std::vector<std::size_t> indices;
                std::vector<double> values;

                if(is_s) {
                    if (doing_response) {
                        indices = {0, 3, 5};
                        values = {0.950, 1.000, 1.05};
                    } else {
                        indices = {0, 3, 5, 7, 10};
                        values = {0.95, 0.98, 1.00, 1.02, 1.05};
                    }
                } else {
                    if (doing_response) {
                        indices = {0, 3, 5};
                        values = {0.6, 1.0, 1.4};
                    } else {
                        indices = {0, 3, 5, 7, 10};
                        values = {0.60, 0.84, 1.00, 1.16, 1.40}; 
                    }   
                }

                if (indices.size() != values.size()) {
                    std::cerr << "Plotter::PlotTemplatePlots: indices and values sizes do not match!" << std::endl;
                    return;
                }

                float nominal_ttbar_histo_integral = 0;
                for (std::size_t i = 0; i < indices.size(); ++i) {
                    unsigned int nominal_index = indices.at(indices.size()/2);
                    std::string suffix;
                    std::string long_name1;
                    std::string long_name2;
                    if (is_s) {
                        if (doing_response) {
                            long_name1 = name+"_s1_"+std::to_string(indices.at(i))+"_s2_3_isol_0p5";
                            long_name2 = name+"_s1_3_s2_"+std::to_string(indices.at(i))+"_isol_0p5";
                            suffix = "_jes";
                        } else {
                            long_name1 = name+"_s1_"+std::to_string(indices.at(i))+"_s2_5_isol_0p5";
                            long_name2 = name+"_s1_5_s2_"+std::to_string(indices.at(i))+"_isol_0p5";
                            suffix = "_jes";
                        }
                    } else {
                        if (doing_response) {
                            long_name1 = name+"_r1_"+std::to_string(indices.at(i))+"_r2_3_isol_0p5";
                            long_name2 = name+"_r1_3_r2_"+std::to_string(indices.at(i))+"_isol_0p5";
                            suffix = "_jer";
                        } else {
                            long_name1 = name+"_r1_"+std::to_string(indices.at(i))+"_r2_5_isol_0p5";
                            long_name2 = name+"_r1_5_r2_"+std::to_string(indices.at(i))+"_isol_0p5";
                            suffix = "_jer";
                        }
                    }           

                    std::string region = "_Inclusive_no_met_cut";
                    if (doing_response) region = "_Inclusive";
                    std::unique_ptr<TH1D> ttbar_histo1(static_cast<TH1D*>(m_ttbar_files.at(m_nominal_ttbar_index)->Get(("NOSYS/"+long_name1+"_pt_"+pt_intervals_2D.at(pt_group).at(pt_interval)+suffix+region).c_str())));
                    std::unique_ptr<TH1D> ttbar_histo2(static_cast<TH1D*>(m_ttbar_files.at(m_nominal_ttbar_index)->Get(("NOSYS/"+long_name2+"_pt_"+pt_intervals_2D.at(pt_group).at(pt_interval)+suffix+region).c_str())));
                    if (normalise && i == nominal_index ) nominal_ttbar_histo_integral = ttbar_histo1->Integral();
                    std::cerr << "Plotter::PlotTemplatePlots: Getting: " << long_name1+"_pt_"+pt_intervals_2D.at(pt_group).at(pt_interval)+suffix+region <<std::endl;
                    if (!ttbar_histo1) {
                        std::cerr << "Plotter::PlotTemplatePlots: Cannot read template histo: " << long_name1+"_pt_"+pt_intervals_2D.at(pt_group).at(pt_interval)+suffix+region << ", skipping\n";
                        return;
                    }
                    template_histos1.emplace_back(std::move(ttbar_histo1));
                
                    std::cerr << "Plotter::PlotTemplatePlots: Getting: " << long_name2+"_pt_"+pt_intervals_2D.at(pt_group).at(pt_interval)+suffix+region <<std::endl;
                    if (!ttbar_histo2) {
                        std::cerr << "Plotter::PlotTemplatePlots: Cannot read template histo: " << long_name2+"_pt_"+pt_intervals_2D.at(pt_group).at(pt_interval)+suffix+region << ", skipping\n";
                        return;
                    }
                    template_histos2.emplace_back(std::move(ttbar_histo2));
                }

                if (template_histos1.size() == 0) {
                    std::cerr << "Plotter::PlotTemplatePlots: template_histos1 size is zero!" << std::endl;
                    return;
                }

                if (template_histos2.size() == 0) {
                    std::cerr << "Plotter::PlotTemplatePlots: template_histos2 size is zero!" << std::endl;
                    return;
                }

                if (template_histos1.size() > colours.size() || template_histos2.size() > colours.size()) {
                    std::cerr << "Plotter::PlotTemplatePlots: more template histos than colours" << std::endl;
                    return;
                }

                //normalise number of events in template histos to the nominal histogram s=1, r=1
                if (normalise) {
                    for (std::size_t i = 0; i < template_histos1.size(); ++i) {
                        template_histos1.at(i)->Scale(nominal_ttbar_histo_integral/template_histos1.at(i)->Integral());
                        template_histos2.at(i)->Scale(nominal_ttbar_histo_integral/template_histos2.at(i)->Integral());
                    }
                }

                if (!doing_response) {
                    const float bin_width1 = template_histos1.at(0)->GetBinWidth(1);
                    const float bin_width2 = template_histos2.at(0)->GetBinWidth(1);
                    if (bin_width1 < 1) {
                        template_histos1.at(0)->GetYaxis()->SetTitle(Form("%s / %.2f %s", elements.c_str(), bin_width1, units.c_str()));
                    } else {
                        template_histos1.at(0)->GetYaxis()->SetTitle(Form("%s / %.f %s", elements.c_str(), bin_width1, units.c_str()));
                    }
                    if (bin_width2 < 1) {
                        template_histos2.at(0)->GetYaxis()->SetTitle(Form("%s / %.2f %s", elements.c_str(), bin_width2, units.c_str()));
                    } else {
                        template_histos2.at(0)->GetYaxis()->SetTitle(Form("%s / %.f %s", elements.c_str(), bin_width2, units.c_str()));
                    }
                }

                /// Canvas
                TCanvas c1("","",800,600);
                TPad pad1("pad1","pad1",0.0, 0.3, 1.0, 1.00);
                TPad pad2("pad2","pad2", 0.0, 0.010, 1.0, 0.3);
                pad1.SetBottomMargin(0.001);
                pad1.SetBorderMode(0);
                pad2.SetBottomMargin(0.5);
                pad1.SetTicks(1,1);
                pad2.SetTicks(1,1);
                pad1.Draw();
                pad2.Draw();

                for (std::size_t i = 0; i < template_histos1.size(); ++i) {
                    template_histos1.at(i)->SetLineColor(colours.at(i));
                }

                DrawUpperTemplatePlot(&pad1, template_histos1);

                pad1.RedrawAxis();
                /// Draw legend
                TLegend leg1(0.70, 0.5, 0.85, 0.9);
                for (std::size_t i = 0; i < template_histos1.size(); ++i) {
                    std::string label;
                    if (is_s) {
                        label = "s =";
                    } else {
                        label = "r =";
                    }   
                    leg1.AddEntry(template_histos1.at(i).get(), Form("%s %.3f", label.c_str(), values.at(i)), "l");
                }

                leg1.SetFillColor(0);
                leg1.SetLineColor(0);
                leg1.SetBorderSize(0);
                leg1.SetTextFont(72);
                leg1.SetTextSize(0.045);
                leg1.Draw("same");

                std::vector<std::unique_ptr<TH1D> > ratios1;
                for (const auto& ihist : template_histos1) {
                    ratios1.emplace_back(static_cast<TH1D*>(ihist->Clone()));
                }

                DrawRatioTemplatePlot(&pad2, ratios1, axis, is_s, doing_response);

                /// Draw line
                const float min1 = ratios1.at(0)->GetXaxis()->GetBinLowEdge(1);
                const float max1 = ratios1.at(0)->GetXaxis()->GetBinUpEdge(ratios1.at(0)->GetNbinsX());
                TLine line1 (min1, 1, max1, 1);
                line1.SetLineColor(kBlack);
                line1.SetLineStyle(2);
                line1.SetLineWidth(3);
                line1.Draw("same");

                /// Canvas
                TCanvas c2("","",800,600);
                TPad pad3("pad3","pad3",0.0, 0.3, 1.0, 1.00);
                TPad pad4("pad4","pad4", 0.0, 0.010, 1.0, 0.3);
                pad3.SetBottomMargin(0.001);
                pad3.SetBorderMode(0);
                pad4.SetBottomMargin(0.5);
                pad3.SetTicks(1,1);
                pad4.SetTicks(1,1);
                pad3.Draw();
                pad4.Draw();

                for (std::size_t i = 0; i < template_histos2.size(); ++i) {
                    template_histos2.at(i)->SetLineColor(colours.at(i));
                }

                DrawUpperTemplatePlot(&pad3, template_histos2);

                pad3.RedrawAxis();
                /// Draw legend
                TLegend leg2(0.70, 0.5, 0.85, 0.9);
                for (std::size_t i = 0; i < template_histos2.size(); ++i) {
                    std::string label;
                    if (is_s) {
                        label = "s =";
                    } else {
                        label = "r =";
                    }   
                    leg2.AddEntry(template_histos2.at(i).get(), Form("%s %.3f", label.c_str(), values.at(i)), "l");
                }

                leg2.SetFillColor(0);
                leg2.SetLineColor(0);
                leg2.SetBorderSize(0);
                leg2.SetTextFont(72);
                leg2.SetTextSize(0.045);
                leg2.Draw("same");

                std::vector<std::unique_ptr<TH1D> > ratios2;
                for (const auto& ihist : template_histos2) {
                    ratios2.emplace_back(static_cast<TH1D*>(ihist->Clone()));
                }

                DrawRatioTemplatePlot(&pad4, ratios2, axis, is_s, doing_response);

                /// Draw line
                const float min2 = ratios2.at(0)->GetXaxis()->GetBinLowEdge(1);
                const float max2 = ratios2.at(0)->GetXaxis()->GetBinUpEdge(ratios2.at(0)->GetNbinsX());
                TLine line2 (min2, 1, max2, 1);
                line2.SetLineColor(kBlack);
                line2.SetLineStyle(2);
                line2.SetLineWidth(3);
                line2.Draw("same");

                std::string folder = "/Inclusive_no_met_cut/WmassTemplates/";
                if (doing_response) folder = "/Inclusive/ResponseTemplates/";

                if (is_s && normalise) {
                    c1.Print((m_output_directory+folder+name+"_s1_variations_pt_"+pt_intervals_2D.at(pt_group).at(pt_interval)+"_normalised.png").c_str());
                    //c1.Print((m_output_directory+folder+name+"_s1_variations_pt_"+pt_intervals_2D.at(pt_group).at(pt_interval)+"_normalised.pdf").c_str());
                    c2.Print((m_output_directory+folder+name+"_s2_variations_pt_"+pt_intervals_2D.at(pt_group).at(pt_interval)+"_normalised.png").c_str());
                    //c2.Print((m_output_directory+folder+name+"_s2_variations_pt_"+pt_intervals_2D.at(pt_group).at(pt_interval)+"_normalised.pdf").c_str());
                } else if (is_s){
                    c1.Print((m_output_directory+folder+name+"_s1_variations_pt_"+pt_intervals_2D.at(pt_group).at(pt_interval)+".png").c_str());
                    //c1.Print((m_output_directory+folder+name+"_s1_variations_pt_"+pt_intervals_2D.at(pt_group).at(pt_interval)+".pdf").c_str());
                    c2.Print((m_output_directory+folder+name+"_s2_variations_pt_"+pt_intervals_2D.at(pt_group).at(pt_interval)+".png").c_str());
                    //c2.Print((m_output_directory+folder+name+"_s2_variations_pt_"+pt_intervals_2D.at(pt_group).at(pt_interval)+".pdf").c_str());
                } else if (normalise) {
                    c1.Print((m_output_directory+folder+name+"_r1_variations_pt_"+pt_intervals_2D.at(pt_group).at(pt_interval)+"_normalised.png").c_str());
                    //c1.Print((m_output_directory+folder+name+"_r1_variations_pt_"+pt_intervals_2D.at(pt_group).at(pt_interval)+"_normalised.pdf").c_str());
                    c2.Print((m_output_directory+folder+name+"_r2_variations_pt_"+pt_intervals_2D.at(pt_group).at(pt_interval)+"_normalised.png").c_str());
                    //c2.Print((m_output_directory+folder+name+"_r2_variations_pt_"+pt_intervals_2D.at(pt_group).at(pt_interval)+"_normalised.pdf").c_str());
                } else {
                    c1.Print((m_output_directory+folder+name+"_r1_variations_pt_"+pt_intervals_2D.at(pt_group).at(pt_interval)+".png").c_str());
                    //c1.Print((m_output_directory+folder+name+"_r1_variations_pt_"+pt_intervals_2D.at(pt_group).at(pt_interval)+".pdf").c_str());
                    c2.Print((m_output_directory+folder+name+"_r2_variations_pt_"+pt_intervals_2D.at(pt_group).at(pt_interval)+".png").c_str());
                    //c2.Print((m_output_directory+folder+name+"_r2_variations_pt_"+pt_intervals_2D.at(pt_group).at(pt_interval)+".pdf").c_str());
                }

            } //1st loop over 2D pt intervals
        } // 2nd loop over 2D pt intervals 
    }
}

void Plotter::PlotDataMCPlots(const std::string& name,
                              const std::string& axis,
                              const std::string& elements,
                              const std::string& units,
                              const bool& logY,
                              const std::string& region) const {
    std::cout << "Plotter::PlotDataMCPlots: Plotting data/MC plot: " << name << "\n";

    std::unique_ptr<TH1D> ttbar_histo(static_cast<TH1D*>(m_ttbar_files.at(m_nominal_ttbar_index)->Get(("NOSYS/"+name+"_"+region).c_str())));
    if (!ttbar_histo) {
        std::cerr << "Plotter::PlotDataMCPlots: Cannot read ttbar histo: " << name << " in "<< region << ", skipping\n";
        return;
    }

    if (name != "W_mass_peak" && name != "best_Chi2") Common::AddOverflow(ttbar_histo.get());

    std::vector<std::unique_ptr<TH1D> > background_histos;
    for (const auto& ibkg : m_background_files) {
        TH1D* h = static_cast<TH1D*>(ibkg->Get(("NOSYS/"+name+"_"+region).c_str()));
        if (!h) {
            std::cerr << "Plotter::PlotDataMCPlots: Cannot read background histos: " << name << ", skipping\n";
            return;
        }

        if (name != "W_mass_peak") Common::AddOverflow(h);
        background_histos.emplace_back(std::move(h));
    }

    std::unique_ptr<TH1D> data_histo(nullptr);
    if (m_data_file) {
        data_histo.reset(static_cast<TH1D*>(m_data_file->Get(("NOSYS/"+name+"_"+region).c_str())));
        if (!data_histo) {
            std::cerr << "Plotter::PlotDataMCPlots: Cannot read data histogram: " << name << ", skipping\n";
            return;
        }
        if (name != "W_mass_peak") Common::AddOverflow(data_histo.get());
    } else {
        std::cerr << "Plotter::PlotDataMCPlots: No data file, skipping \n";
        return;
    }

    /// Stack background histograms
    for (std::size_t ibkg = 0; ibkg < background_histos.size(); ++ibkg) {
        for (std::size_t i = ibkg + 1; i < background_histos.size(); ++i) {
            background_histos.at(ibkg)->Add(background_histos.at(i).get());
        }
    }

    /// Stack signal as well
    ttbar_histo->Add(background_histos.at(0).get());

    if (m_syst_shape_only) {
        if (data_histo->Integral() > 1e-6 && ttbar_histo->Integral() > 1e-6) {
            ttbar_histo->Scale(data_histo->Integral()/ttbar_histo->Integral());
        }
    }

    /// Cosmetics
    ttbar_histo->SetFillColor(0);
    ttbar_histo->SetMarkerStyle(21);
    ttbar_histo->SetLineColor(1);
    ttbar_histo->SetLineWidth(2);
    if (logY) {
        ttbar_histo->SetMinimum(1.001);
        ttbar_histo->SetMaximum(1e4*ttbar_histo->GetMaximum());
    } else {
        ttbar_histo->SetMinimum(0.001);
        ttbar_histo->SetMaximum(1.8*ttbar_histo->GetMaximum());
    }

    const float bin_width = ttbar_histo->GetBinWidth(1);
    if (bin_width < 1) {
        if (m_syst_shape_only) {
            ttbar_histo->GetYaxis()->SetTitle(Form("Normalised to data / %.2f %s", bin_width, units.c_str()));
        } else {
            ttbar_histo->GetYaxis()->SetTitle(Form("%s / %.2f %s", elements.c_str(), bin_width, units.c_str()));
        }
    } else {
        if (m_syst_shape_only) {
            ttbar_histo->GetYaxis()->SetTitle(Form("Normalised to data / %.f %s", bin_width, units.c_str()));
        } else {
            ttbar_histo->GetYaxis()->SetTitle(Form("%s / %.f %s", elements.c_str(), bin_width, units.c_str()));
        }
    }

    for (std::size_t ibkg = 0; ibkg < m_background_names.size(); ++ibkg) {
        auto itr = m_colour_map.find(m_background_names.at(ibkg));
        if (itr == m_colour_map.end()) {
            std::cerr << "Plotter::PlotDataMCPlots: Cannot find " << m_background_names.at(ibkg) << ", in the colour map\n";
            exit(EXIT_FAILURE);
        }

        background_histos.at(ibkg)->SetFillColor(itr->second);
        background_histos.at(ibkg)->SetMarkerStyle(21);
        background_histos.at(ibkg)->SetLineColor(itr->second);
        background_histos.at(ibkg)->SetLineWidth(2);
    }

    /// Canvas
    TCanvas c("","",800,600);
    TPad pad1("pad1","pad1",0.0, 0.3, 1.0, 1.00);
    TPad pad2("pad2","pad2", 0.0, 0.010, 1.0, 0.3);
    pad1.SetBottomMargin(0.001);
    pad1.SetBorderMode(0);
    pad2.SetBottomMargin(0.5);
    pad1.SetTicks(1,1);
    pad2.SetTicks(1,1);
    pad1.Draw();
    pad2.Draw();

    /// Remove MC stat uncertainties
    for (int ibin = 1; ibin <= ttbar_histo->GetNbinsX(); ++ibin) {
        ttbar_histo->SetBinError(ibin, 0);
    }

    std::unique_ptr<TH1D> total_up  (static_cast<TH1D*>(ttbar_histo->Clone()));
    std::unique_ptr<TH1D> total_down(static_cast<TH1D*>(ttbar_histo->Clone()));

    if (m_run_syst) {
        GetTotalUpDownUncertainty(total_up.get(), total_down.get(), name, region);
    }

    TGraphAsymmErrors error(ttbar_histo.get());

    if (m_run_syst) {
        TransformErrorHistoToTGraph(&error, total_up.get(), total_down.get());
    }

    error.SetFillStyle(3002);
    error.SetFillColor(kBlack);
    error.SetMarkerStyle(0);
    error.SetLineWidth(2);

    DrawUpperDataMCPlot(&pad1, ttbar_histo.get(), background_histos, data_histo.get(), &error);

    pad1.RedrawAxis();

    /// Draw legend
    TLegend leg(0.70, 0.5, 0.85, 0.9);
    leg.AddEntry(data_histo.get(), "Data", "ep");
    leg.AddEntry(ttbar_histo.get(), "t#bar{t}", "f");
    for (std::size_t ibkg = 0; ibkg < m_background_names.size(); ++ibkg) {
        auto label_itr = m_label_map.find(m_background_names.at(ibkg));
        if (label_itr == m_label_map.end()) {
            leg.AddEntry(background_histos.at(ibkg).get(), m_background_names.at(ibkg).c_str(), "f");
        } else {
            leg.AddEntry(background_histos.at(ibkg).get(), label_itr->second.c_str(), "f");
        }
    }
    if (m_run_syst) {
        if (m_syst_shape_only) {
            leg.AddEntry(&error, "Shape unc.", "f");
        } else {
            leg.AddEntry(&error, "Uncertainty", "f");
        }
    }

    leg.SetFillColor(0);
    leg.SetLineColor(0);
    leg.SetBorderSize(0);
    leg.SetTextFont(72);
    leg.SetTextSize(0.045);
    leg.Draw("same");

    std::unique_ptr<TH1D> combined_ratio (static_cast<TH1D*>(ttbar_histo->Clone()));
    std::unique_ptr<TH1D> data_ratio (static_cast<TH1D*>(data_histo->Clone()));

    TGraphAsymmErrors error_ratio(combined_ratio.get());
    for (int ibin = 0; ibin < error_ratio.GetN(); ++ibin) {
        double x,y;
        error_ratio.GetPoint(ibin, x, y);
        error_ratio.SetPoint(ibin, x, 1.);
    }

    std::unique_ptr<TH1D> error_up(static_cast<TH1D*>(total_up->Clone()));
    std::unique_ptr<TH1D> error_down(static_cast<TH1D*>(total_down->Clone()));
    error_up->Divide(combined_ratio.get());
    error_down->Divide(combined_ratio.get());

    error_ratio.SetFillStyle(3002);
    error_ratio.SetFillColor(kBlack);
    error_ratio.SetMarkerStyle(0);
    error_ratio.SetLineWidth(2);

    if (m_run_syst) {
        TransformErrorHistoToTGraph(&error_ratio, error_up.get(), error_down.get());
    }

    std::vector<std::unique_ptr<TArrow> > arrows;
    DrawRatioDataMCPlot(&pad2, combined_ratio.get(), data_ratio.get(), axis, &error_ratio, arrows);

    /// Draw line
    const float min = data_ratio->GetXaxis()->GetBinLowEdge(1);
    const float max = data_ratio->GetXaxis()->GetBinUpEdge(data_ratio->GetNbinsX());
    TLine line (min, 1, max, 1);
    line.SetLineColor(kRed);
    line.SetLineStyle(2);
    line.SetLineWidth(3);
    line.Draw("same");

    std::string log_label("");
    if (logY) {
        pad1.SetLogy();
        log_label = "_log";
    }

    c.Print((m_output_directory+"/"+region+"/DataMCplots/"+name+log_label+".png").c_str());
    //c.Print((m_output_directory+"/"+region+"/DataMCplots/"+name+log_label+".pdf").c_str());
}

void Plotter::PlotDataMCPlots(const std::string& name,
                              const std::string& axis,
                              const std::string& elements,
                              const std::string& units,
                              const bool& logY) const {
    //loop over regions
    for (const auto& ireg : m_regions) {
        if (ireg == "Inclusive_no_met_cut") continue;
        this->PlotDataMCPlots(name, axis, elements, units, logY, ireg);
    }
}

void Plotter::PlotRecoEfficiencies(const std::string& region) const {

    std::cout << "Plotter::PlotRecoEfficiencies: Creating html table with reco efficiencies for region "<<region<<"\n";

    std::vector<float> efficiencies;
    std::unique_ptr<TH1D> h(static_cast<TH1D*>(m_ttbar_files.at(m_nominal_ttbar_index)->Get(("NOSYS/match_success_"+region).c_str())));
    if (!h) {
        std::cerr << "Plotter::PlotRecoEfficiencies: Cannot read the reco efficiencies for region "<<region<<"\n";
        return;
    }
    std::vector<float> tmp;
  const double notMatchable = h->GetBinContent(5) + h->GetBinContent(6);
    const double failedBoth = h->GetBinContent(1);
    const double failedSecond = h->GetBinContent(2);
    const double failedFirst = h->GetBinContent(3);
    const double matchedBoth = h->GetBinContent(4);

    const double total = notMatchable + failedBoth + failedFirst + failedSecond + matchedBoth;
    const double matchable = total - notMatchable;

    efficiencies.emplace_back(total);
    efficiencies.emplace_back(matchable);
    efficiencies.emplace_back(failedBoth);
    efficiencies.emplace_back(failedSecond);
    efficiencies.emplace_back(failedFirst);
    efficiencies.emplace_back(matchedBoth);

    /// open html
    std::unique_ptr<std::ofstream> html = std::make_unique<std::ofstream>();
    html->open((m_output_directory+"/RecoEfficiencies_"+region+".html").c_str(), std::ios::trunc);
    if (!html->is_open() || !html->good()) {
        std::cerr << "Plotter::PlotRecoEfficiencies: Cannot open html page at: " << m_output_directory+"/RecoEfficiencies_"+region+".html" << std::endl;
        exit(EXIT_FAILURE);
    }

    *html << "<html><head><title>RecoEfficiencies</title></head><body>"
          << "<table border = 1> <tr>"
          << "</tr>\n";
    *html << "<td>";
    *html << "<table border style=\"float&#58;left;margin-right:7cm;margin-left:5cm;\"> \n";
    *html << std::fixed << std::setprecision(2);
    *html << "<tr> <td> Region: " <<region;
    *html << "<tr> <td> Category";
    *html << " <td> ttbar";
    *html << "\n<tr> <td> Jet1 matchable and Jet 2 matchable vs passed selection";
    *html << "<td> " << efficiencies.at(1)/efficiencies.at(0);
    *html << "\n<tr bgcolor=lightblue> <td> Jet1 failed and Jet 2 failed vs passed selection";
    *html << "<td> " << efficiencies.at(2)/efficiencies.at(0);
    *html << "\n<tr bgcolor=pink> <td> Jet1 failed and Jet 2 failed vs matchable";
    *html << "<td> " << efficiencies.at(2)/efficiencies.at(1);
    *html << "\n<tr bgcolor=lightblue> <td> Jet1 matched and Jet 2 failed vs passed selection";
    *html << "<td> " << efficiencies.at(3)/efficiencies.at(0);
    *html << "\n<tr bgcolor=pink> <td> Jet1 matched and Jet 2 failed vs matchable";
    *html << "<td> " << efficiencies.at(3)/efficiencies.at(1);
    *html << "\n<tr bgcolor=lightblue> <td> Jet1 failed and Jet 2 matched vs passed selection";
    *html << "<td> " << efficiencies.at(4)/efficiencies.at(0);
    *html << "\n<tr bgcolor=pink> <td> Jet1 failed and Jet 2 matched vs matchable";
    *html << "<td> " << efficiencies.at(4)/efficiencies.at(1);
    *html << "\n<tr bgcolor=lightblue> <td> Jet1 matched and Jet 2 matched vs passed selection";
    *html << "<td> " << efficiencies.at(5)/efficiencies.at(0);
    *html << "\n<tr bgcolor=pink> <td> Jet1 matched and Jet 2 matched vs matchable";
    *html << "<td> " << efficiencies.at(5)/efficiencies.at(1);
    *html << "\n";

    *html << "</tr>\n";
    *html << "</table>\n";
    *html << "</td>\n";

    html->close();
}

void Plotter::PlotRecoEfficiencies() const {
    //loop over regions
    for (const auto& ireg : m_regions) {
        if (ireg == "Inclusive_no_met_cut") continue;
        this->PlotRecoEfficiencies(ireg);
    }
}

void Plotter::PlotJetPairing() const {
    std::cout << "Plotter::PlotJetPairing: Creating html table with reco to truth jet pairing efficiencies\n";
    std::unique_ptr<TH1D> hist_up  (m_ttbar_files.at(m_nominal_ttbar_index)->Get<TH1D>("NOSYS/pairing_status_up_Inclusive"));
    std::unique_ptr<TH1D> hist_down(m_ttbar_files.at(m_nominal_ttbar_index)->Get<TH1D>("NOSYS/pairing_status_down_Inclusive"));

    if (!hist_up || !hist_down) {
        std::cerr << "Plotter::PlotJetPairing: Histograms are nullptrs!\n";
        return;
    }

    const double failed_lepton_up = hist_up->GetBinContent(2);
    const double failed_lepton_down = hist_down->GetBinContent(2);
    const double failed_truth_pair_up = hist_up->GetBinContent(3);
    const double failed_truth_pair_down = hist_down->GetBinContent(3);
    const double failed_truth_isol_up = hist_up->GetBinContent(4);
    const double failed_truth_isol_down = hist_down->GetBinContent(4);
    const double passed_up = hist_up->GetBinContent(5);
    const double passed_down = hist_down->GetBinContent(5);

    const double total_up = failed_lepton_up + failed_truth_pair_up + failed_truth_isol_up + passed_up;
    const double total_down = failed_lepton_down + failed_truth_pair_down + failed_truth_isol_down + passed_down;
    const double total = total_up + total_down;

    // open html
    std::unique_ptr<std::ofstream> html = std::make_unique<std::ofstream>();
    html->open((m_output_directory+"/JetTruthJetPairing.html").c_str(), std::ios::trunc);
    if (!html->is_open() || !html->good()) {
        std::cerr << "Plotter::PlotJetPairing: Cannot open html page at: " << m_output_directory+"/JetTruthJetPairing.html" << std::endl;
        exit(EXIT_FAILURE);
    }

    *html << "<html><head><title>JetPairingEfficiencies</title></head><body>"
          << "<table border = 1> <tr>"
          << "</tr>\n";
    *html << "<td>";
    *html << "<table border style=\"float&#58;left;margin-right:7cm;margin-left:5cm;\"> \n";
    *html << std::fixed << std::setprecision(3);
    *html << "<tr> <td> Category <td> Up jet <td> Down jet <td> Up and Down jet";

    *html << "\n<tr> <td> Failed truth lepton in jet";
    *html << "<td> " << failed_lepton_up/total_up << "<td>" << failed_lepton_down/total_down << "<td>" << (failed_lepton_up+failed_lepton_down)/total;

    *html << "\n<tr> <td> Failed jet match";
    *html << "<td> " << failed_truth_pair_up/total_up << "<td>" << failed_truth_pair_down/total_down << "<td>" << (failed_truth_pair_up+failed_truth_pair_down)/total;

    *html << "\n<tr> <td> Failed truth isolation";
    *html << "<td> " << failed_truth_isol_up/total_up << "<td>" << failed_truth_isol_down/total_down << "<td>" << (failed_truth_isol_up+failed_truth_isol_down)/total;

    *html << "\n<tr> <td> Passed";
    *html << "<td> " << passed_up/total_up << "<td>" << passed_down/total_down << "<td>" << (passed_up+passed_down)/total;

    *html << "\n";
    *html << "</tr>\n";
    *html << "</table>\n";
    *html << "</td>\n";

    html->close();
}

void Plotter::CalculateYields() const {
    for (const auto& ireg : m_regions) {
        if (ireg == "Inclusive_no_met_cut") continue;
        this->CalculateYields(ireg);
    }
}

void Plotter::CalculateYields(const std::string& region) const {
    std::cout << "Plotter::CalculateYields: Calculating event yields for region: " << region << "\n";

    std::unique_ptr<std::ofstream> html = std::make_unique<std::ofstream>();
    html->open((m_output_directory+"/"+region+"/EventYields.html").c_str(), std::ios::trunc);
    if (!html->is_open() || !html->good()) {
        std::cerr << "Plotter::CalculateYields: Cannot open html page at: " << m_output_directory+"/"+region+"/EventYields.html" << std::endl;
       exit(EXIT_FAILURE);
    }
    *html << "<html><head><title>EventYields</title></head><body>"
    			<< "<table border = 1> <tr>"
    			<< "</tr>\n";
    *html << "<td>";
    *html << "<table border style=\"float&#58;left;margin-right:7cm;margin-left:5cm;\">\n";
    *html << "<tr>  ";
    *html << "<td> Type <td> EventYields\n";

    float total(0);

    const std::string histoName = "NOSYS/number_of_jets_" + region;
    /// Read the event yields
    std::unique_ptr<TH1D> ttbar(dynamic_cast<TH1D*>(m_ttbar_files.at(m_nominal_ttbar_index)->Get(histoName.c_str())));
    if (!ttbar) {
        std::cerr << "Plotter::CalculateYields: Cannot read ttbar histo!" << std::endl;
        exit(EXIT_FAILURE);
    }
    total+= ttbar->Integral();
    *html << "<tr> <td> " << m_ttbar_names.at(m_nominal_ttbar_index) << " <td> " << std::fixed << std::setprecision(1) << ttbar->Integral() << "\n";

    /// Backgrounds
    for (std::size_t ibkg = 0; ibkg < m_background_files.size(); ++ibkg) {
        std::unique_ptr<TH1D> h(dynamic_cast<TH1D*>(m_background_files.at(ibkg)->Get(histoName.c_str())));
        if (!h) {
            std::cerr << "Plotter::CalculateYields: Cannot read bkg histos file!" << std::endl;
            exit(EXIT_FAILURE);
        }
        total+= h->Integral();
        *html << "<tr> <td> " << m_background_names.at(ibkg) << " <td> " << std::fixed << std::setprecision(1) << h->Integral() << "\n";
    }

    /// Total
    *html << "<tr bgcolor=lightblue> <td> Total pred. <td> " << std::fixed << std::setprecision(1) << total << "\n";

    if (m_data_file) {
        /// Data
        std::unique_ptr<TH1D> data(dynamic_cast<TH1D*>(m_data_file->Get(histoName.c_str())));
        if (!data) {
            std::cerr << "Plotter::CalculateYields: Cannot read data histo!" << std::endl;
            exit(EXIT_FAILURE);
        }
        *html << "<tr bgcolor=pink> <td> Data <td> " << std::fixed << std::setprecision(1) << data->Integral() << "\n";
        *html << "<tr bgcolor=#66b3ff> <td> " << "Diff.[%] " << " <td> " << std::fixed << std::setprecision(2) << 100*(total/data->Integral() -1) << "\n";
    } else {
        std::cout << "No data file provided for yields calculation, skipping data from the table\n";
    }

    *html << "</tr>\n";
    *html << "</table>\n";
    *html << "</td>\n";
    html->close();
}

void Plotter::CloseRootFiles() {
    for (auto& ifile : m_ttbar_files) {
        ifile->Close();
    }
    for (auto& ifile : m_background_files) {
        ifile->Close();
    }
    for (auto& ifile : m_special_files) {
        ifile->Close();
    }
    if (m_data_file) m_data_file->Close();
}

void Plotter::PlotUpperComparison(const std::vector<std::unique_ptr<TH1D> >& histos,
                                  TPad* pad,
                                  const std::string& axis) const {

    if (histos.size() == 0) return;

    pad->cd();

    const float resize = MaxResize(histos, false);

    histos.at(0)->GetYaxis()->SetRangeUser(0.00001, resize);
    histos.at(0)->GetXaxis()->SetTitle(axis.c_str());

    for (std::size_t i = 0; i < histos.size(); ++i) {
        histos.at(i)->SetLineWidth(2);
        if (i < m_styles.size()) {
            histos.at(i)->SetLineColor(m_styles.at(i).first);
            histos.at(i)->SetLineStyle(m_styles.at(i).second);
        } else {
            std::cerr << "Plotter::PlotUpperComparison: No map for the plot style. Will use some random colours.\n";
            histos.at(i)->SetLineColor(i);
            histos.at(i)->SetLineStyle(i);
        }

        if (i == 0) {
            histos.at(0)->Draw("HIST");
        } else {
            histos.at(i)->Draw("HIST same");
        }
    }

    DrawLabels(pad, 0.2 , 0.85, true);
}

void Plotter::PlotRatioComparison(const std::vector<std::unique_ptr<TH1D> >& histos,
                                  TPad* pad,
                                  const std::string& axis,
                                  const float& range,
                                  const bool& force) const {

    pad->cd();

    histos.at(0)->GetXaxis()->SetLabelFont(42);
    histos.at(0)->GetXaxis()->SetLabelSize(0.15);
    histos.at(0)->GetXaxis()->SetLabelOffset(0.01);
    histos.at(0)->GetXaxis()->SetTitleFont(42);
    histos.at(0)->GetXaxis()->SetTitleSize(0.15);
    histos.at(0)->GetXaxis()->SetTitleOffset(1.2);
    histos.at(0)->GetXaxis()->SetTitle(axis.c_str());

    histos.at(0)->GetYaxis()->SetLabelFont(42);
    histos.at(0)->GetYaxis()->SetLabelSize(0.15);
    histos.at(0)->GetYaxis()->SetLabelOffset(0.03);
    histos.at(0)->GetYaxis()->SetTitleFont(42);
    histos.at(0)->GetYaxis()->SetTitleSize(0.15);
    histos.at(0)->GetYaxis()->SetTitleOffset(0.5);
    histos.at(0)->GetYaxis()->SetNdivisions(505);
    histos.at(0)->GetYaxis()->SetTitle("ratio");

    float max(-10);
    float min(100);
    std::unique_ptr<TH1D> tmp(static_cast<TH1D*>(histos.at(0)->Clone()));
    for (std::size_t ihist = 0; ihist < histos.size(); ++ihist) {
        histos.at(ihist)->Divide(tmp.get());
        if (histos.at(ihist)->GetMaximum() > max) {
            max = histos.at(ihist)->GetMaximum();
        }
        if (histos.at(ihist)->GetMinimum() < min) {
            min = histos.at(ihist)->GetMinimum();
        }
        if (ihist == 0) {
            histos.at(0)->Draw("HIST");
        } else {
            histos.at(ihist)->Draw("HIST same");
        }
    }

    if (force) {
        histos.at(0)->GetYaxis()->SetRangeUser(1-range, 1.+range);
    } else {
        histos.at(0)->GetYaxis()->SetRangeUser(0.8*min, 1.2*max);
    }
}

void Plotter::FillStyleMap() {
    m_styles.push_back(std::make_pair(kBlack, 1));
    m_styles.push_back(std::make_pair(kOrange, 1));
    m_styles.push_back(std::make_pair(kOrange+1, 1));
    m_styles.push_back(std::make_pair(kAzure-9, 1));
    m_styles.push_back(std::make_pair(kAzure+9, 1));
}

float Plotter::MaxResize(const std::vector<std::unique_ptr<TH1D> >& histos, const bool is_log) const {
    float result(-9999);

    for (const auto& ihist : histos) {
        if (result < ihist->GetMaximum()) {
            result = ihist->GetMaximum();
        }
    }

    if (is_log) return result*1e6;

    return result*1.6;
}

void Plotter::DrawLabels(TPad *pad, const float& x, const float& y, const bool& add_lumi) const{
    pad->cd();

    TLatex l1;
    l1.SetTextAlign(9);
    l1.SetTextFont(72);
    l1.SetTextSize(0.06);
    l1.SetNDC();
    l1.DrawLatex(x, y, "ATLAS");

    TLatex l2;
    l2.SetTextAlign(9);
    l2.SetTextSize(0.06);
    l2.SetTextFont(42);
    l2.SetNDC();
    l2.DrawLatex(x+0.11, y, m_atlas_label.c_str());

    TLatex l3;
    l3.SetTextAlign(9);
    l3.SetTextSize(0.06);
    l3.SetTextFont(42);
    l3.SetNDC();
    if (add_lumi) {
        l3.DrawLatex(x, y-0.07, ("#sqrt{s} = 13 TeV, "+m_lumi_label+" fb^{-1}").c_str());
    } else {
        l3.DrawLatex(x, y-0.07, "#sqrt{s} = 13 TeV");
    }

    std::string collection;
    /// Add collection name
    if (m_collection == "topo")  {
        collection = "Anti-kt, R=0.4, EMTopo";
    } else if (m_collection == "pflow") {
        collection = "Anti-kt, R=0.4, EMPFlow";
    } else {
        std::cout << "Plotter::DrawLabels: Collection not set, setting to 'pflow'\n";
        collection = "Anti-kt, R=0.4, EMPFlow";
    }

    l2.DrawLatex(x, y-0.15, collection.c_str());
    l2.DrawLatex(x, y-0.25, "Release 24");
}

void Plotter::DrawUpperTemplatePlot(TPad* pad,
                                  const std::vector<std::unique_ptr<TH1D> >& template_histos) const {

    pad->cd();

    const float resize = MaxResize(template_histos, false);

    template_histos.at(0)->GetYaxis()->SetRangeUser(0.01, resize);
    template_histos.at(0)->GetYaxis()->SetLabelSize(0.05);
    template_histos.at(0)->GetYaxis()->SetLabelOffset(0);
    template_histos.at(0)->GetYaxis()->SetLabelFont(42);
    template_histos.at(0)->GetYaxis()->SetTitleOffset(1.9);
    template_histos.at(0)->SetTitleFont(42);
    template_histos.at(0)->SetTitleSize(0.07);


    for (std::size_t i = 0; i < template_histos.size(); i++) {
        if (i == 0) template_histos.at(0)->Draw("HIST");
        template_histos.at(i)->Draw("HIST SAME");
    }

    DrawLabels(pad, 0.21, 0.88, true);
}

void Plotter::DrawRatioTemplatePlot(TPad* pad,
                                    const std::vector<std::unique_ptr<TH1D> >& ratios,
                                    const std::string& axis,
                                    const bool& is_s,
                                    const bool& doing_response) const {
    pad->cd();
    ratios.at(0)->Divide(ratios.at(2).get());
    ratios.at(0)->GetXaxis()->SetLabelFont(42);
    ratios.at(0)->GetXaxis()->SetLabelSize(0.15);
    ratios.at(0)->GetXaxis()->SetLabelOffset(0.01);
    ratios.at(0)->GetXaxis()->SetTitleFont(42);
    ratios.at(0)->GetXaxis()->SetTitleSize(0.20);
    ratios.at(0)->GetXaxis()->SetTitleOffset(0.9);
    ratios.at(0)->GetXaxis()->SetNdivisions(505);
    ratios.at(0)->GetXaxis()->SetTitle(axis.c_str());
    if (doing_response) {
        ratios.at(0)->GetYaxis()->SetRangeUser(0.2,1.8);
    }
    else {
        if (is_s) ratios.at(0)->GetYaxis()->SetRangeUser(0.7,1.3);
        else ratios.at(0)->GetYaxis()->SetRangeUser(0.8,1.2);
    }
    if (doing_response) {
        ratios.at(0)->GetYaxis()->SetLabelSize(0.15);
    }
    else {
        if (is_s) ratios.at(0)->GetYaxis()->SetLabelSize(0.15);
        else ratios.at(0)->GetYaxis()->SetLabelSize(0.1);
    }
    ratios.at(0)->GetYaxis()->SetLabelFont(42);
    ratios.at(0)->GetYaxis()->SetLabelOffset(0.03);
    ratios.at(0)->GetYaxis()->SetTitleFont(42);
    ratios.at(0)->GetYaxis()->SetTitleSize(0.15);
    ratios.at(0)->GetYaxis()->SetTitleOffset(0.5);
    ratios.at(0)->GetYaxis()->SetNdivisions(505);
    if (is_s) ratios.at(0)->GetYaxis()->SetTitle("#splitline{Variation}{w.r.t s = 1 }");
    else ratios.at(0)->GetYaxis()->SetTitle("#splitline{Variation}{w.r.t r = 1 }");
    ratios.at(0)->Draw("HIST");

    for (std::size_t i = 1; i < ratios.size(); ++i) {
        if (i == 2) continue;
        ratios.at(i)->Divide(ratios.at(2).get());
        ratios.at(i)->Draw("HIST same");
    }
}

void Plotter::DrawUpperDataMCPlot(TPad* pad,
                                  TH1D* ttbar_histo,
                                  const std::vector<std::unique_ptr<TH1D> >& background_histos,
                                  TH1D* data_histo,
                                  TGraphAsymmErrors* error) const {

    pad->cd();
    data_histo->SetMarkerStyle(20);
    data_histo->SetMarkerSize(1.45);
    data_histo->SetLineColor(kBlack);
    data_histo->SetLineStyle(1);

    ttbar_histo->GetYaxis()->SetLabelSize(0.05);
    ttbar_histo->GetYaxis()->SetLabelFont(42);
    ttbar_histo->GetYaxis()->SetTitleFont(42);
    ttbar_histo->GetYaxis()->SetTitleSize(0.07);
    ttbar_histo->GetYaxis()->SetTitleOffset(1.1);

    ttbar_histo->Draw("HIST");
    for (auto& ibkg : background_histos) {
        ibkg->Draw("HIST SAME");
    }

    data_histo->Draw("X0EPZ SAME");
    error->Draw("E2 SAME");

    DrawLabels(pad, 0.21, 0.88, true);
}

void Plotter::DrawRatioDataMCPlot(TPad* pad,
                                  const TH1D* combined,
                                  TH1D* data,
                                  const std::string& axis,
                                  TGraphAsymmErrors* error,
                                  std::vector<std::unique_ptr<TArrow> >& arrows) const {
    pad->cd();

    data->Divide(combined);
    data->SetMarkerStyle(8);
    data->SetMarkerSize(1);
    data->GetXaxis()->SetLabelFont(42);
    data->GetXaxis()->SetLabelSize(0.15);
    data->GetXaxis()->SetLabelOffset(0.01);
    data->GetXaxis()->SetTitleFont(42);
    data->GetXaxis()->SetTitleSize(0.20);
    data->GetXaxis()->SetTitleOffset(0.9);
    data->GetXaxis()->SetNdivisions(505);
    data->GetXaxis()->SetTitle(axis.c_str());

    data->GetYaxis()->SetRangeUser(0.7,1.3);
    data->GetYaxis()->SetLabelFont(42);
    data->GetYaxis()->SetLabelSize(0.15);
    data->GetYaxis()->SetLabelOffset(0.03);
    data->GetYaxis()->SetTitleFont(42);
    data->GetYaxis()->SetTitleSize(0.15);
    data->GetYaxis()->SetTitleOffset(0.5);
    data->GetYaxis()->SetNdivisions(505);
    data->GetYaxis()->SetTitle("data/pred.");

    data->Draw("EP SAME");
    error->Draw("E2 SAME");

    const double min = 0.7;
    const double max = 1.3;

    for (int ibin = 1; ibin <= data->GetNbinsX(); ++ibin) {
        const double val = data->GetBinContent(ibin);
        if (combined->GetBinContent(ibin) < 1e-9) continue;

        int isUp(0);
        if (val < min) {
            isUp = -1;
        } else if (val > max) {
            isUp = 1;
        }

        if (isUp == 0) continue;
        std::unique_ptr<TArrow> arrow(nullptr);
        if (isUp == 1) arrow = std::make_unique<TArrow>(data->GetXaxis()->GetBinCenter(ibin),max-0.05*(max-min), data->GetXaxis()->GetBinCenter(ibin),max,0.02,"|>");
        else arrow = std::make_unique<TArrow>(data->GetXaxis()->GetBinCenter(ibin),min+0.05*(max-min), data->GetXaxis()->GetBinCenter(ibin),min,0.02,"|>");
        arrow->SetFillColor(10);
        arrow->SetFillStyle(1001);
        arrow->SetLineColor(kBlue-7);
        arrow->SetLineWidth(2);
        arrow->SetAngle(40);
        arrow->Draw();
        arrows.emplace_back(std::move(arrow));
    }

}

void Plotter::SetColourMap() {
    m_colour_map["SingleTop_s_PP8_FS"] = kAzure-9;
    m_colour_map["SingleTop_t_PP8_FS"] = kAzure+9;
    m_colour_map["SingleTop_tW_DR_PP8_FS"] = kAzure+3;
    m_colour_map["Wjets_Sherpa_FS"] = kOrange;
    m_colour_map["Zjets_Sherpa_FS"] = kOrange+1;
    m_colour_map["Diboson_Sherpa_FS"] = kGray+1;
    m_colour_map["Multijet"] = kGray;
}

void Plotter::GetTotalUpDownUncertainty(TH1D* total_up, TH1D* total_down, const std::string& name, const std::string& region) const {
    /// Set bin content to 0
    const int nbins = total_up->GetNbinsX();

    if (total_down->GetNbinsX() != nbins) {
        std::cerr << "Plotter::GetTotalUpDownUncertainty: Up and Down histograms have different number of bins!" << std::endl;
        exit(EXIT_FAILURE);
    }

    for (int ibin = 1; ibin <= nbins; ++ibin) {
        total_up->SetBinContent(ibin, 0);
        total_up->SetBinError(ibin, 0);
        total_down->SetBinContent(ibin, 0);
        total_down->SetBinError(ibin, 0);
    }

    /// Loop over systematics and add uncertainties
    for (const auto& isyst : m_systematics) {
        AddSingleSyst(total_up, total_down, name, isyst, region);
    }
}

void Plotter::AddSingleSyst(TH1D* total_up, TH1D* total_down, const std::string& name, const Systematic& systematic, const std::string& region) const {
    std::string in_file_up = systematic.up_file;
    std::string in_file_down = systematic.down_file;
    if (in_file_down == "") in_file_down = in_file_up;


    std::string up_name = systematic.up_histo + "/" + name + "_" + region;
    std::string down_name = systematic.down_histo + "/" + name + "_" + region;
    if (systematic.type == SYSTEMATICTYPE::TWOSIDED && down_name == "") {
        std::cerr << "Plotter::AddSingleSyst: You need to provide both up and down name for TWOSIDED systematic" << std::endl;
        exit(EXIT_FAILURE);
    }
    if (down_name == "") down_name = up_name;

    std::string reference_file = systematic.reference_file;
    if (reference_file == "") reference_file = in_file_up;

    std::string reference_histo = "NOSYS/" +name + "_" + region;
    if (systematic.reference_histo != "") reference_histo = systematic.reference_histo + "/" + name + "_" + region;

    /// Read the histograms
    std::unique_ptr<TH1D> histo_nominal = GetHistoFromAll(reference_file, reference_histo);
    std::unique_ptr<TH1D> histo_up(nullptr);
    std::unique_ptr<TH1D> histo_down(nullptr);

    if (!histo_nominal) {
        std::cerr << "Plotter::AddSingleSyst: Cannot read the necessary histograms for " << name << std::endl;
        exit(EXIT_FAILURE);
    }


    if (systematic.type == SYSTEMATICTYPE::TWOSIDED) {
        histo_up   = GetHistoFromAll(in_file_up, up_name);
        histo_down = GetHistoFromAll(in_file_down, down_name);
    } else if (systematic.type == SYSTEMATICTYPE::ONESIDED) {
        histo_up   = GetHistoFromAll(in_file_up, up_name);
        /// Symmetrise it
        std::unique_ptr<TH1D> tmp(static_cast<TH1D*>(histo_up->Clone()));
        tmp->Add(histo_nominal.get(), -1);
        histo_down = std::unique_ptr<TH1D>(static_cast<TH1D*>(histo_nominal->Clone()));
        histo_down->Add(tmp.get(), -1);
    } else if (systematic.type == SYSTEMATICTYPE::NORMALISATION) {
        histo_up   = std::unique_ptr<TH1D>(static_cast<TH1D*>(histo_nominal->Clone()));
        histo_down = std::unique_ptr<TH1D>(static_cast<TH1D*>(histo_nominal->Clone()));
        histo_up  ->Scale(1.+systematic.norm_up);
        histo_down->Scale(1.-systematic.norm_down);
    } else if (systematic.type == SYSTEMATICTYPE::MCSTAT) {
        histo_up   = std::unique_ptr<TH1D>(static_cast<TH1D*>(histo_nominal->Clone()));
        histo_down = std::unique_ptr<TH1D>(static_cast<TH1D*>(histo_nominal->Clone()));

        for (int ibin = 1; ibin <= histo_nominal->GetNbinsX(); ++ibin) {
            const double nominal = histo_nominal->GetBinContent(ibin);
            const double unc = histo_nominal->GetBinError(ibin);
            histo_up  ->SetBinContent(ibin, nominal + unc);
            histo_down->SetBinContent(ibin, nominal - unc);
        }
    }

    if (!histo_up || !histo_down) {
        std::cerr << "Plotter::AddSingleSyst: Cannot read the necessary up/down histograms for " << name  << std::endl;
        exit(EXIT_FAILURE);
    }

    if (total_up->GetNbinsX() != histo_down->GetNbinsX()) {
        std::cerr << "Plotter::AddSingleSyst: Total up and up syst histograms have different binning" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (histo_up->GetNbinsX() != histo_down->GetNbinsX()) {
        std::cerr << "Plotter::AddSingleSyst: Up and down syst histograms have different binning" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (histo_up->GetNbinsX() != histo_nominal->GetNbinsX()) {
        std::cerr << "Plotter::AddSingleSyst: Up syst and nominal histograms have different binning" << std::endl;
        exit(EXIT_FAILURE);
    }

    /// subtract the pseudodata, i.e. Variation - PseudoData + nominal
    if (systematic.subtractUpName != "") {
        std::string sub_up_name = systematic.subtractUpName + "/" + name + "_" + region;
        std::unique_ptr<TH1D> sub = GetHistoFromAll(in_file_up, sub_up_name);
        if (!sub) {
            std::cerr << "Plotter::AddSingleSyst: Cannot find the subtract histogram for file: " << in_file_up << ", histo: " <<  sub_up_name << "\n";
            exit(EXIT_FAILURE);
        }
        histo_up->Add(sub.get(), -1);
        histo_up->Add(histo_nominal.get());
    }

    if (systematic.subtractDownName != "") {
        std::string sub_down_name = systematic.subtractDownName + "/" + name + "_" + region;
        std::unique_ptr<TH1D> sub = GetHistoFromAll(in_file_down, sub_down_name);
        if (!sub) {
            std::cerr << "Plotter::AddSingleSyst: Cannot find the subtract histogram for file: " << in_file_down << ", histo: " <<  sub_down_name << "\n";
            exit(EXIT_FAILURE);
        }
        histo_down->Add(sub.get(), -1);
        histo_down->Add(histo_nominal.get());
    }

    if (m_syst_shape_only) {
        if (std::abs(histo_up->Integral()) > 1e-6) histo_up->Scale(histo_nominal->Integral()/histo_up->Integral());
        if (std::abs(histo_down->Integral()) > 1e-6) histo_down->Scale(histo_nominal->Integral()/histo_down->Integral());
    }

    if (name != "W_mass_peak") {
        Common::AddOverflow(histo_up.get());
        Common::AddOverflow(histo_down.get());
        Common::AddOverflow(histo_nominal.get());
    }

    AddHistosInSquares(total_up, total_down, histo_up.get(), histo_down.get(), histo_nominal.get());
}

std::unique_ptr<TH1D> Plotter::GetHistoFromAll(const std::string& file_name, const std::string& histo_name) const {
    /// search signal files
    auto itr_signal = std::find(m_ttbar_names.begin(), m_ttbar_names.end(), file_name);
    if (itr_signal != m_ttbar_names.end()) {
        const std::size_t pos_signal = std::distance(m_ttbar_names.begin(), itr_signal);
        return std::unique_ptr<TH1D>(static_cast<TH1D*>(m_ttbar_files.at(pos_signal)->Get(histo_name.c_str())));
    }

    /// Try bkg file
    auto itr_background = std::find(m_background_names.begin(), m_background_names.end(), file_name);

    if (itr_background != m_background_names.end()) {
        const std::size_t pos_bkg = std::distance(m_background_names.begin(), itr_background);
        return std::unique_ptr<TH1D>(static_cast<TH1D*>(m_background_files.at(pos_bkg)->Get(histo_name.c_str())));
    }

    /// Has to be special
    auto itr_special = std::find(m_special_names.begin(), m_special_names.end(), file_name);
    if (itr_special == m_special_names.end()) {
        std::cerr << "Plotter::GetHistoFromAll: Cannot find file: " << file_name << std::endl;
        exit(EXIT_FAILURE);
    }

    const std::size_t pos_special = std::distance(m_special_names.begin(), itr_special);

    return std::unique_ptr<TH1D>(static_cast<TH1D*>(m_special_files.at(pos_special)->Get(histo_name.c_str())));
}

void Plotter::AddHistosInSquares(TH1D* total_up,  TH1D* total_down, const TH1D* histo_up, const TH1D* histo_down, const TH1D* histo_nominal) const {

    /// We know the histograms have the same binning
    const int nbins = total_up->GetNbinsX();
    for (int ibin = 1; ibin <= nbins; ++ibin) {
        double up(0);
        double down(0);
        if (histo_up->GetBinContent(ibin) > histo_nominal->GetBinContent(ibin)) {
            up = histo_up->GetBinContent(ibin) - histo_nominal->GetBinContent(ibin);
        } else {
            down = histo_nominal->GetBinContent(ibin) - histo_up->GetBinContent(ibin);
        }
        if (histo_down->GetBinContent(ibin) > histo_nominal->GetBinContent(ibin)) {
            up = std::hypot(up, histo_down->GetBinContent(ibin) - histo_nominal->GetBinContent(ibin));
        } else {
            down = std::hypot(down, histo_nominal->GetBinContent(ibin) - histo_down->GetBinContent(ibin));
        }

        /// now add it to the total hist
        const double current_up   = total_up->GetBinContent(ibin);
        const double current_down = total_down->GetBinContent(ibin);

        total_up  ->SetBinContent(ibin,  std::hypot(current_up, up));
        total_down->SetBinContent(ibin, -std::hypot(current_down, down));
    }
}

void Plotter::TransformErrorHistoToTGraph(TGraphAsymmErrors* error, const TH1D* up, const TH1D* down) const {
    const int nbins = error->GetN();

    for (int ibin = 0; ibin < nbins; ++ibin) {
        error->SetPointEYhigh(ibin, up->GetBinContent(ibin+1));
        error->SetPointEYlow (ibin, -down->GetBinContent(ibin+1));
    }
}

void Plotter::SetLabelMap() {
    m_label_map["ttbar_PP8_FS"] = "t#bar{t}";
    m_label_map["Wjets_Sherpa_FS"] = "W+jets";
    m_label_map["Zjets_Sherpa_FS"] = "Z+jets";
    m_label_map["Diboson_Sherpa_FS"] = "Diboson";
    m_label_map["SingleTop_s_PP8_FS"] = "SingleTop s-chan";
    m_label_map["SingleTop_t_PP8_FS"] = "SingleTop t-chan";
    m_label_map["SingleTop_tW_DR_PP8_FS"] = "SingleTop tW-chan";
    m_label_map["Multijet"] = "Fake leptons";
}
