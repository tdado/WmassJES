#include "WmassJES/Fitter.h"

#include "TH1D.h"

#include <iostream>

Fitter::Fitter(TH1D* histo) :
    m_hist(histo),
    m_mean(-9999),
    m_sigma(-1),
    m_chi2(-1),
    m_factor(2.)
{
}

void Fitter::Fit(const int color) {
    if (!m_hist) {
        std::cerr << "Fitter::Fit: Passed nullptr!\n";
        return;
    }

    const float mean = m_hist->GetMean();
    const float rms  = m_hist->GetRMS();
    const float min = mean - m_factor*rms;
    const float max = mean + m_factor*rms;

    m_formula = std::make_unique<TF1>("","gaus(0)", min, max);

    m_formula->SetLineColor(color);
    m_formula->SetParameter(0, 1.);
    m_formula->SetParameter(1, mean);
    m_formula->SetParameter(2, rms);

    m_hist->Fit(m_formula.get(), "RQ");

    m_mean  = m_formula->GetParameter(1);
    m_sigma = m_formula->GetParameter(2);
    m_chi2  = m_formula->GetChisquare() / m_formula->GetNDF();
}
