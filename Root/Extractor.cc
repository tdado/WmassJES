#include "WmassJES/Extractor.h"
#include "WmassJES/Common.h"
#include "AtlasUtils/AtlasLabels.h"
#include "AtlasUtils/AtlasStyle.h"
#include "AtlasUtils/AtlasUtils.h"

#include "TCanvas.h"
#include "TF3.h"
#include "TGraphAsymmErrors.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TLine.h"
#include "TPad.h"
#include "TRandom3.h"
#include "TSystem.h"

#include <algorithm>
#include <iomanip>
#include <iostream>

Extractor::Extractor(const std::string& folder) :
    m_debug(false),
    m_folder(folder),
    m_outputFolder("Results"),
    m_fit_data(false),
    m_use_syst(false),
    m_PE_number(-1),
    m_ttbar_name(""),
    m_obs_name("W_m"),
    m_template_name("W_m"),
    m_param_s{},
    m_param_r{},
    m_ttbar_file(nullptr),
    m_data_file(nullptr) {

    SetAtlasStyle();
}

void Extractor::SetParameterS(const int steps,
                              const float min,
                              const float max) {
    m_param_s.steps = steps;
    m_param_s.min = min;
    m_param_s.max = max;
}

void Extractor::SetParameterR(const int steps,
                              const float min,
                              const float max) {
    m_param_r.steps = steps;
    m_param_r.min = min;
    m_param_r.max = max;
}

void Extractor::ExtractJES() {

    OpenRootFiles();

    FFFitter fitter{};
    fitter.SetDebug(m_debug);
    fitter.SetParameterS(m_param_s.steps, m_param_s.min, m_param_s.max);
    fitter.SetParameterR(m_param_r.steps, m_param_r.min, m_param_r.max);
    gSystem->mkdir(m_outputFolder.c_str());

    const std::string orig_folder = m_outputFolder;

    fitter.SetProfile(m_profile);
    fitter.SetIs1DFit(m_is_1D_fit);
    fitter.SetOutputFolder(m_outputFolder);
    fitter.SetSingleRegions(m_single_regions);
    fitter.SetCrossRegions(m_cross_regions);
    fitter.SetStartIndex(m_start_index);
    fitter.SetIsJesFit(m_is_jes_fit);
    fitter.SetSingleRegionTemplates(this->GetNominalTemplateSingleRegion(fitter), this->GetSingleRegionNames());
    fitter.SetCrossRegionTemplates(this->GetNominalTemplateCrossRegion(fitter), this->GetCrossRegionNames());


    if (m_profile) {
        fitter.SetNominalSingleRegion(this->GetAsimovSingleRegions());
        fitter.SetNominalCrossRegion(this->GetAsimovCrossRegions());
        this->RunProfiledFit(&fitter);
    } else {
        fitter.PrepareCombinedFitParametrisation();
        fitter.PrepareWSCombined();
        this->AsimovFitCombined(&fitter);

        if (m_PE_number > 0) {
            this->RunPseudoExperimentsCombined(&fitter);
        }

        if (m_fit_data) {
            this->DataFitCombined(&fitter);
        }
    }

    CloseRootFiles();
}

std::unique_ptr<TH1D> Extractor::GetSystShiftedHist(const std::vector<Systematic>& syst,
                                                    const std::string& observable,
                                                    const bool is_up,
                                                    const bool use_ref,
                                                    const bool use_subtract) const {
    std::unique_ptr<TH1D> result(nullptr);

    /// Process ttbar
    int index = Common::GetSystematicIndex(syst, m_ttbar_name);
    const bool is_special = (syst.at(0).target != syst.at(0).up_file) && ((syst.at(0).target != syst.at(0).down_file));
    std::size_t special_index(0);
    if (use_ref || is_special) {
        if (syst.size() != 1) {
            std::cerr << "Extractor::GetSystShiftedHist: Reference systematic is used but there are more than one... This should not happen" << std::endl;
            exit(EXIT_FAILURE);
        }

        index = -1;

        auto itr = std::find(m_special_names.begin(), m_special_names.end(), syst.at(0).up_file);
        if (itr == m_special_names.end()) {
            std::cerr << "Extractor::GetSystShiftedHist: Cannot find special syst in the list of special systs: " << syst.at(0).up_file << std::endl;
            exit(EXIT_FAILURE);
        }

        if (use_ref) {
            itr= std::find(m_special_names.begin(), m_special_names.end(), syst.at(0).reference_file);
            if (itr == m_special_names.end()) {
                std::cerr << "Extractor::GetSystShiftedHist: Cannot find reference in the list of special systs: " << syst.at(0).reference_file << std::endl;
                exit(EXIT_FAILURE);
            }
        }

        special_index = std::distance(m_special_names.begin(), itr);
    }

    const std::string nominal_name = "NOSYS/"+observable;

    if (index < 0) { // doesnt exist
        std::string path_sig = use_ref ? syst.at(0).reference_histo : (is_up ? syst.at(0).up_histo : syst.at(0).down_histo);
        if (use_subtract) {
            path_sig = is_up ? syst.at(index).subtractUpName : syst.at(index).subtractDownName;
        }
        const std::string syst_name = path_sig+"/"+observable;
        if (is_special && (syst.at(0).target == m_ttbar_name)) {
            result = std::unique_ptr<TH1D>(m_special_files.at(special_index)->Get<TH1D>(syst_name.c_str()));
        } else if (syst.at(0).target == m_ttbar_name) {
            result = std::unique_ptr<TH1D>(m_ttbar_file->Get<TH1D>(syst_name.c_str()));
        } else {
            result = std::unique_ptr<TH1D>(m_ttbar_file->Get<TH1D>(nominal_name.c_str()));
        }
    } else {
        std::string path_sig = is_up ? syst.at(index).up_histo : syst.at(index).down_histo;

        if (use_subtract) {
            path_sig = is_up ? syst.at(index).subtractUpName : syst.at(index).subtractDownName;
        }

        const std::string syst_name = path_sig+"/"+observable;

        if (use_ref) {
            if (syst.at(0).target == m_ttbar_name) {
                result = std::unique_ptr<TH1D>(m_special_files.at(special_index)->Get<TH1D>(syst_name.c_str()));
            } else {
                result = std::unique_ptr<TH1D>(m_ttbar_file->Get<TH1D>(syst_name.c_str()));
            }
        } else {
            result = std::unique_ptr<TH1D>(m_ttbar_file->Get<TH1D>(syst_name.c_str()));
        }

        if (!result) {
            std::cerr << "Extractor::GetSystShiftedHist: Cannot read ttbar file!" << std::endl;
            exit(EXIT_FAILURE);
        }

        if (syst.at(index).type == SYSTEMATICTYPE::NORMALISATION) {
           if (is_up) {
               result->Scale(1+syst.at(index).norm_up);
           } else {
               result->Scale(1-syst.at(index).norm_down);
           }
        }
    }

    /// process bkgs
    for (std::size_t ibkg = 0; ibkg < m_background_names.size(); ++ibkg) {
        index = Common::GetSystematicIndex(syst, m_background_names.at(ibkg));
        if (index < 0) {
            std::unique_ptr<TH1D> tmp(nullptr);
            std::string path_bkg = is_up ? syst.at(0).up_histo : syst.at(0).down_histo;
            if (use_subtract) {
                path_bkg = is_up ? syst.at(index).subtractUpName : syst.at(index).subtractDownName;
            }
            const std::string syst_name = path_bkg+"/"+observable;
            if (is_special && (syst.at(0).target == m_background_names.at(ibkg))) {
                tmp = std::unique_ptr<TH1D>(m_special_files.at(special_index)->Get<TH1D>(syst_name.c_str()));
            } else if (syst.at(0).target == m_background_names.at(ibkg)) {
                tmp = std::unique_ptr<TH1D>(m_background_files.at(ibkg)->Get<TH1D>(syst_name.c_str()));
            } else {
                tmp = std::unique_ptr<TH1D>(m_background_files.at(ibkg)->Get<TH1D>(nominal_name.c_str()));
            }
            if (!tmp) {
                std::cerr << "Extractor::GetSystShiftedHist: Cannot find histogram: " << syst_name << " for bkg: " << m_background_names.at(ibkg) << std::endl;
                exit(EXIT_FAILURE);
            }
            result->Add(tmp.get());
        } else {
            std::string path_bkg = is_up ? syst.at(index).up_histo : syst.at(index).down_histo;
            if (use_subtract) {
                path_bkg = is_up ? syst.at(index).subtractUpName : syst.at(index).subtractDownName;
            }
            const std::string syst_name = path_bkg+"/"+observable;
            std::unique_ptr<TH1D> tmp(nullptr);
            if (use_ref) {
                if (syst.at(0).target == m_background_names.at(ibkg)) {
                    tmp = std::unique_ptr<TH1D>(m_special_files.at(special_index)->Get<TH1D>(syst_name.c_str()));
                } else {
                    tmp = std::unique_ptr<TH1D>(m_background_files.at(ibkg)->Get<TH1D>(syst_name.c_str()));
                }
            } else {
                tmp = std::unique_ptr<TH1D>(m_background_files.at(ibkg)->Get<TH1D>(syst_name.c_str()));
            }
            if (!tmp) {
                std::cerr << "Extractor::GetSystShiftedHist: Cannot find histogram: " << syst_name << " for bkg: " << m_background_names.at(ibkg) << std::endl;
                exit(EXIT_FAILURE);
            }
            if (syst.at(index).type == SYSTEMATICTYPE::NORMALISATION) {
               if (is_up) {
                   tmp->Scale(1+syst.at(index).norm_up);
               } else {
                   tmp->Scale(1-syst.at(index).norm_down);
               }
            }
            tmp->SetDirectory(nullptr);
            result->Add(tmp.get());
        }
    }

    result->SetDirectory(nullptr);

    return result;
}

void Extractor::OpenRootFiles() {
    const std::string path = m_folder+"/";
    m_ttbar_file = std::unique_ptr<TFile>(TFile::Open((path+m_ttbar_name+".root").c_str()));

    if (!m_ttbar_file) {
        std::cerr << "Extractor::OpenRootFiles: Cannot open ttbar file" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (m_fit_data) {
        m_data_file = std::unique_ptr<TFile>(TFile::Open((path+"Data.root").c_str()));
        if (!m_data_file) {
            std::cerr << "Extractor::OpenRootFiles: Cannot open data file" << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    for (const auto& ibkg : m_background_names) {
        std::unique_ptr<TFile> tmp(TFile::Open((path+ibkg+".root").c_str()));
        if (!tmp) {
            std::cerr << "Extractor::OpenRootFiles: Cannot open " << ibkg << " file" << std::endl;
            exit(EXIT_FAILURE);
        }

        m_background_files.emplace_back(std::move(tmp));
    }

    for (const auto& ispecial : m_special_names) {
        std::unique_ptr<TFile> tmp(TFile::Open((path+ispecial+".root").c_str()));
        if (!tmp) {
            std::cerr << "Extractor::OpenRootFiles: Cannot open " << ispecial << " file" << std::endl;
            exit(EXIT_FAILURE);
        }

        m_special_files.emplace_back(std::move(tmp));
    }

    gSystem->mkdir((m_outputFolder).c_str());
}

void Extractor::CloseRootFiles() {
    m_ttbar_file->Close();
    if (m_data_file) {
        m_data_file->Close();
    }

    for (auto& ibkg : m_background_files) {
        ibkg->Close();
    }

    for (auto& ispecial : m_special_files) {
        ispecial->Close();
    }
}

std::vector<std::pair<std::vector<TH1D>, std::string> > Extractor::GetNominalTemplateSingleRegion(const FFFitter& fitter) const {
    std::vector<std::pair<std::vector<TH1D>, std::string> > result;
    for (const auto& ireg : m_single_regions) {
        std::vector<TH1D> tmp_vec;
        for (int i = 0; i < (m_is_jes_fit ? fitter.GetSsteps() : fitter.GetRsteps()); ++i) {
            const std::string template_name = "NOSYS/"+m_template_name+"_"+ (m_is_jes_fit ? "s" : "r")+"_" + std::to_string(i) + "_isol_0p5_" + ireg.first + "_" + (m_is_jes_fit ? "jes" : "jer") + "_" + m_region;
            std::unique_ptr<TH1D> tmp(static_cast<TH1D*>(m_ttbar_file->Get(template_name.c_str())));
            if (!tmp) {
                std::cerr << "Extractor::GetNominalTemplateSingleRegion: Cannot read histogram: " << template_name << std::endl;
                exit(EXIT_FAILURE);
            }

            tmp->SetDirectory(nullptr);
            std::string bkg_name = "NOSYS/"+m_obs_name + "_Jet_" + ireg.first + "_eta_0p8";
            /// Add backgrounds
            for (auto& ifile : m_background_files) {
                std::unique_ptr<TH1D> tmpBkg(static_cast<TH1D*>(ifile->Get(bkg_name.c_str())));
                if (!tmpBkg) {
                    std::cerr << "Extractor::GetNominalTemplateSingleRegion: Cannot read histogram: " << bkg_name << std::endl;
                    exit(EXIT_FAILURE);
                }
                tmpBkg->SetDirectory(nullptr);
                tmp->Add(tmpBkg.get());
            }
            tmp->GetXaxis()->SetRange(0,0);
            tmp_vec.emplace_back(std::move(*tmp));
        }
        result.emplace_back(std::make_pair(std::move(tmp_vec), ireg.second));
    }

    return result;
}

std::vector<std::pair<std::vector<std::vector<TH1D> >, std::pair<std::string, std::string> > > Extractor::GetNominalTemplateCrossRegion(const FFFitter& fitter) const {
    std::vector<std::pair<std::vector<std::vector<TH1D> >, std::pair<std::string, std::string> > > result;
    for (const auto& ireg : m_cross_regions) {
        std::vector<std::vector<TH1D> > tmp_vec;
        for (int i = 0; i < (m_is_jes_fit ? fitter.GetSsteps() : fitter.GetRsteps()); ++i) {
            std::vector<TH1D> tmp_vec2;
            for (int j = 0; j < (m_is_jes_fit ? fitter.GetSsteps() : fitter.GetRsteps()); ++j) {
                const std::string template_name = "NOSYS/"+m_template_name+"_"+ (m_is_jes_fit ? "s1" : "r1")+"_" + std::to_string(i) + "_" + (m_is_jes_fit ? "s2" : "r2") + "_" + std::to_string(j) + "_isol_0p5_" + ireg.first + "_" + (m_is_jes_fit ? "jes" : "jer") + "_" + m_region;
                std::unique_ptr<TH1D> tmp(static_cast<TH1D*>(m_ttbar_file->Get(template_name.c_str())));
                if (!tmp) {
                    std::cerr << "Extractor::GetNominalTemplateCrossRegion: Cannot read histogram: " << template_name << std::endl;
                    exit(EXIT_FAILURE);
                }

                tmp->SetDirectory(nullptr);
                std::string bkg_name = "NOSYS/"+m_obs_name + "_Jet_" + ireg.first + "_eta_0p8";
                /// Add backgrounds
                for (auto& ifile : m_background_files) {
                    std::unique_ptr<TH1D> tmpBkg(static_cast<TH1D*>(ifile->Get(bkg_name.c_str())));
                    if (!tmpBkg) {
                        std::cerr << "Extractor::GetNominalTemplateCrossRegion: Cannot read histogram: " << bkg_name << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    tmpBkg->SetDirectory(nullptr);
                    tmp->Add(tmpBkg.get());
                }
                tmp->GetXaxis()->SetRange(0,0);
                tmp_vec2.emplace_back(std::move(*tmp));
            }
            tmp_vec.emplace_back(std::move(tmp_vec2));
        }
        result.emplace_back(std::make_pair(std::move(tmp_vec), ireg.second));
    }

    return result;
}

std::vector<std::string> Extractor::GetSingleRegionNames() const {
    std::vector<std::string> result;
    for (const auto& i : m_single_regions) {
        result.emplace_back(i.first);
    }

    return result;
}

std::vector<std::string> Extractor::GetCrossRegionNames() const {
    std::vector<std::string> result;
    for (const auto& i : m_cross_regions) {
        result.emplace_back(i.first);
    }

    return result;
}

void Extractor::AsimovFitCombined(FFFitter* fitter) {

    m_combined_fit_uncertainty_all.clear();
    m_combined_fit_uncertainty_all.clear();

    auto single_regions_histos = this->GetAsimovSingleRegions();
    auto cross_regions_histos = this->GetAsimovCrossRegions();
    //auto single_regions_histos = this->GetClosureHistogramsSingleRegions("JE2", "JER2", 5, 5, false);
    //auto cross_regions_histos = this->GetClosureHistogramsCrossRegions("JE2", "JER2", 5, 5, false);
    m_combined_fit_central = fitter->FitCombinedModel(single_regions_histos, cross_regions_histos, false);

    std::map<std::string, double> closure_asimov;

    for (int i = m_start_index; i < 7; ++i) {
        const std::string param = (m_is_jes_fit ? "JES" : "JER") + std::to_string(i);
        auto itr = m_combined_fit_central.find(param);
        if (itr == m_combined_fit_central.end()) {
            std::cerr << "Extractor::AsimovFitCombined: Cannot find parameter: " << param << "\n";
            return;
        }

        // we expect 1 from asimov
        const double diff = itr->second - 1.0;
        closure_asimov.insert({param, diff});
    }

    if (!fitter->FitIsOkay()) {
        return;
    }

    // fill the maps with the parameters;
    auto results = fitter->GetCombinedFitResults();
    for (const auto& iparam : results) {
        std::map<std::string, std::pair<double, double> > tmp;
        const double error = iparam.second.error;
        tmp.insert({"STAT", {error, -error}});
        m_combined_fit_stat.insert({iparam.first, {error, -error}});

        m_combined_fit_uncertainty_all.insert({iparam.first, {}});
        m_combined_fit_uncertainty_category.insert({iparam.first, tmp});
    }

    if (m_run_closure) {
        this->RunClosureCombined(fitter, m_is_jes_fit, closure_asimov);
    }

    if (m_use_syst) {
        /// Calculate syst uncertainty
        std::cout << "Extractor::AsimovFitCOmbined: Will process systematics\n\n";
        /// Loop over unique systematics
        const std::vector<std::string>& unique_systs = Common::GetSystUniqueNames(m_systematics);
        for (const auto& isyst : unique_systs) {
            std::cout << "Extractor::AsimovFitCombined: Processing systematic: " << isyst << "\n";
            this->ProcessSingleSystematicCombined(fitter, isyst);
        }
        std::cout << "\nExtractor::AsimovFitCombined: Finished processing systematics \n\n";
    }

    this->PrintUncertaintiesCombined();

    this->DrawCorrelationMatrix(*fitter, nullptr, true);

    this->PlotFinalUncertaintyCombined(nullptr, true);
}

std::vector<TH1D> Extractor::GetAsimovSingleRegions() const {
    std::vector<TH1D> result;

    for (const auto& ireg : m_single_regions) {
        const std::string obs = m_obs_name + "_Jet_" + ireg.first + "_eta_0p8";
        auto asimov = this->GetAsimovSingleHisto("NOSYS", obs);
        Common::SetErrorsToExpected(asimov.get());
        asimov->GetXaxis()->SetRange(0,0);
        result.emplace_back(std::move(*asimov));
    }


    return result;
}

std::vector<TH1D> Extractor::GetAsimovCrossRegions() const {
    std::vector<TH1D> result;

    for (const auto& ireg : m_cross_regions) {
        const std::string obs = m_obs_name + "_Jet_" + ireg.first + "_eta_0p8";
        auto asimov = this->GetAsimovSingleHisto("NOSYS", obs);
        Common::SetErrorsToExpected(asimov.get());
        asimov->GetXaxis()->SetRange(0,0);
        result.emplace_back(std::move(*asimov));
    }

    return result;
}

void Extractor::ProcessSingleSystematicCombined(FFFitter* fitter, const std::string& syst) {
    const std::vector<Systematic>& systs = Common::GetSystSameName(m_systematics, syst);
    /// Now each systematic should impact only one sample!
    if (!Common::SystematicsAreConsistent(systs)) {
        std::cerr << "Extractor::ProcessSingleSystematicCombined: There is something really weird happening, investigate!" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (systs.size() == 0) return;

    const std::string& categoryName = systs.at(0).category;

    if (systs.at(0).type == SYSTEMATICTYPE::ONESIDED) {
        auto hist_single_regions = this->GetShiftedHistSingleRegions(systs, true, false);
        auto hist_cross_regions = this->GetShiftedHistCrossRegions(systs, true, false);

        std::map<std::string, double> current_central = fitter->FitCombinedModel(hist_single_regions, hist_cross_regions, false, -1);

        if (systs.at(0).target == systs.at(0).reference_file) {
            this->UpdateUncertainties(current_central, {}, m_combined_fit_central, syst, categoryName, true);
        } else {
            hist_single_regions = this->GetShiftedHistSingleRegions(systs, true, true);
            hist_cross_regions = this->GetShiftedHistCrossRegions(systs, true, true);

            std::map<std::string, double> reference_central = fitter->FitCombinedModel(hist_single_regions, hist_cross_regions, false, -1);

            this->UpdateUncertainties(current_central, {}, reference_central, syst, categoryName, true);
        }
    } else if (systs.at(0).type == SYSTEMATICTYPE::TWOSIDED || systs.at(0).type == SYSTEMATICTYPE::NORMALISATION) {
        auto hist_single_regions_up = this->GetShiftedHistSingleRegions(systs, true, false);
        auto hist_cross_regions_up = this->GetShiftedHistCrossRegions(systs, true, false);
        auto hist_single_regions_down = this->GetShiftedHistSingleRegions(systs, false, false);
        auto hist_cross_regions_down = this->GetShiftedHistCrossRegions(systs, false, false);

        std::map<std::string, double> current_central_up = fitter->FitCombinedModel(hist_single_regions_up, hist_cross_regions_up, false, -1);
        std::map<std::string, double> current_central_down = fitter->FitCombinedModel(hist_single_regions_down, hist_cross_regions_down, false, -1);
        this->UpdateUncertainties(current_central_up, current_central_down, m_combined_fit_central, syst, categoryName, false);

    } else {
        std::cerr << "Extractor::ProcessSingleSystematicCombined: Unknown systematic type!" << std::endl;
        exit(EXIT_FAILURE);
    }
}

std::vector<TH1D> Extractor::GetShiftedHistSingleRegions(const std::vector<Systematic>& systs,
                                                         const bool is_up,
                                                         const bool use_ref) const {

    std::vector<TH1D> result;
    for (const auto& ireg : m_single_regions) {
        const std::string obs = m_obs_name + "_Jet_" + ireg.first + "_eta_0p8";
        auto hist = this->GetSystShiftedHist(systs, obs, is_up, use_ref);
        if (!hist) {
            std::cerr << "Cannot read syst histogram: " << obs << "\n";
            exit(1);
        }

        if (!systs.at(0).subtractDownName.empty()) {
            auto sub = this->GetSystShiftedHist(systs, obs, is_up, false, true);
            auto nominal = this->GetAsimovSingleHisto("NOSYS", obs);

            hist->Add(sub.get(), -1);
            hist->Add(nominal.get());
        }
        Common::SetErrorsToExpected(hist.get());
        hist->GetXaxis()->SetRange(0,0);

        result.emplace_back(std::move(*hist));
    }

    return result;
}

std::vector<TH1D> Extractor::GetShiftedHistCrossRegions(const std::vector<Systematic>& systs,
                                                        const bool is_up,
                                                        const bool use_ref) const {

    std::vector<TH1D> result;
    for (const auto& ireg : m_cross_regions) {
        const std::string obs = m_obs_name + "_Jet_" + ireg.first + "_eta_0p8";
        auto hist = this->GetSystShiftedHist(systs, obs, is_up, use_ref);
        if (!hist) {
            std::cerr << "Cannot read syst histogram: " << obs << "\n";
            exit(1);
        }

        if (!systs.at(0).subtractDownName.empty()) {
            auto sub = this->GetSystShiftedHist(systs, obs, is_up, false, true);
            auto nominal = this->GetAsimovSingleHisto("NOSYS", obs);

            hist->Add(sub.get(), -1);
            hist->Add(nominal.get());
        }
        Common::SetErrorsToExpected(hist.get());

        hist->GetXaxis()->SetRange(0,0);

        result.emplace_back(std::move(*hist));
    }

    return result;
}

void Extractor::UpdateUncertainties(const std::map<std::string, double>& up,
                                    const std::map<std::string, double>& down,
                                    const std::map<std::string, double>& central,
                                    const std::string& syst,
                                    const std::string& category,
                                    const bool is_one_sided) {
    for (const auto& param : up) {
        const std::string name = param.first;
        const double value_up = param.second;
        auto itr = central.find(name);
        if (itr == central.end()) {
            std::cerr << "Extractor::UpdateUncertainties: Cannot find parameter: " << name << "\n";
            return;
        }
        const double uncertainty_up = value_up - itr->second;
        double uncertainty_down(0);

        if (is_one_sided) {
            uncertainty_down = -uncertainty_up;
        } else {
            auto itrDown = down.find(name);
            if (itrDown == down.end()) {
                std::cerr << "Extractor::UpdateUncertainties: Cannot find parameter: " << name << ", for down\n";
                return;
            }

            const double value_down = itrDown->second;
            uncertainty_down = value_down - itr->second;
        }

        // now update the maps
        auto itrAll = m_combined_fit_uncertainty_all.find(name);
        if (itrAll == m_combined_fit_uncertainty_all.end()) {
            std::cerr << "Extractor::UpdateUncertainties: Cannot find parameter : " << name << " in the all uncertainties map\n";
            return;
        }

        itrAll->second.insert({syst, {uncertainty_up, uncertainty_down}});

        auto itrCat = m_combined_fit_uncertainty_category.find(name);
        if (itrCat == m_combined_fit_uncertainty_category.end()) {
            std::cerr << "Extractor::UpdateUncertainties: Cannot find parameter : " << name << " in the category uncertainties map\n";
            return;
        }

        Common::AddMapUncertainty(itrCat->second, uncertainty_up, uncertainty_down, category);
    }

}

void Extractor::PrintUncertaintiesCombined() {
    for (int i = m_start_index; i < 7; ++i) {
        std::string name = (m_is_jes_fit ? "JES" : "JER") + std::to_string(i);

        auto itrCentral = m_combined_fit_central.find(name);
        if (itrCentral == m_combined_fit_central.end()) {
            std::cerr << "Extractor::PrintUncertaintiesCombined: Cannot find parameter for central: " << name << "\n";
            return;
        }

        const double central = itrCentral->second;

        auto itrStat = m_combined_fit_stat.find(name);
        if (itrStat == m_combined_fit_stat.end()) {
            std::cerr << "Extractor::PrintUncertaintiesCombined: Cannot find parameter for stat: " << name << "\n";
            return;
        }

        const double stat_up = itrStat->second.first;
        const double stat_down = itrStat->second.second;

        std::cout << "=================================================================================\n";
        std::cout << "                       ASIMOV   parameter: " << name << "\n";
        std::cout << "=================================================================================\n";

        auto itrSyst = m_combined_fit_uncertainty_all.find(name);
        if (itrSyst == m_combined_fit_uncertainty_all.end()) {
            std::cerr << "Extractor::PrintUncertaintiesCombined: Cannot find parameter for syst: " << name << "\n";
            return;
        }

        std::cout << "All uncertainties\n";
        for (const auto& isyst : itrSyst->second) {
            std::cout << "Syst: " << isyst.first << " uncertainty: +" << isyst.second.first << " " << isyst.second.second << "\n";
        }

        const double syst_up = Common::SumCategories(itrSyst->second, true);
        const double syst_down = -Common::SumCategories(itrSyst->second, false);

        const double total_up = std::hypot(stat_up, syst_up);
        const double total_down = -std::hypot(stat_down, syst_down);

        auto itrCat = m_combined_fit_uncertainty_category.find(name);
        if (itrCat == m_combined_fit_uncertainty_category.end()) {
            std::cerr << "Extractor::PrintUncertaintiesCombined: Cannot find parameter for category: " << name << "\n";
            return;
        }

        std::cout << "\nCategories\n";
        for (const auto& icat : itrCat->second) {
            std::cout << "Category: " << icat.first << " uncertainty: +" << icat.second.first << " " << icat.second.second << "\n";
        }

        itrCat->second.insert(std::make_pair("SYST", std::make_pair(syst_up, syst_down)));
        itrCat->second.insert(std::make_pair("TOTAL", std::make_pair(total_up, total_down)));

        std::cout << "=================================================================================\n";
        std::cout << "                                FINAL RESULTS\n";
        std::cout << "=================================================================================\n";
        std::cout << name << " = " << central << " +"  << stat_up << " " << stat_down << " (stat)\n";
        std::cout << name << " total syst: +" << syst_up << " " << syst_down << "\n";
        std::cout << name << " = " << central << " +"  << total_up << " " << total_down << " (total)\n";
        std::cout << "=================================================================================\n\n";
    }
}

void Extractor::DataFitCombined(FFFitter* fitter) {
    auto single_region_histos = this->GetDataHistogramSingleRegion();
    auto cross_region_histos = this->GetDataHistogramCrossRegion();

    m_combined_fit_central_data = fitter->FitCombinedModel(single_region_histos, cross_region_histos, false);

    const auto results = fitter->GetCombinedFitResults();
    for (int iparam = m_start_index; iparam < 7; ++iparam) {
        std::string name = (m_is_jes_fit ? "JES" : "JER") + std::to_string(iparam);

        auto itr = results.find(name);
        if (itr == results.end()) {
            std::cerr << "Extractor::DataFitCombined: Cannot find parameter: " << name << "\n";
            return;
        }

        const double central = itr->second.central;
        const double error = itr->second.error;
        const double up = itr->second.positive_error;
        const double down = itr->second.negative_error;

        std::cout << "=================================================================================\n";
        std::cout << "                         DATA   parameter: " << name << "\n";
        std::cout << "=================================================================================\n";
        std::cout << name << " = " << central << " +-" << error <<  " (+" << up << " " << down << ")\n";
        std::cout << "=================================================================================\n";

        m_combined_fit_data.insert({name, {central, error}});

        // update the stat and total eror

        auto itrCat = m_combined_fit_uncertainty_category.find(name);
        if (itrCat == m_combined_fit_uncertainty_category.end()) {
            std::cerr << "Extractor::DataFitCombined: Cannot find parameter: " << name << " in the category uncertainty\n";
            return;
        }

        auto itrStat = itrCat->second.find("STAT");
        if (itrStat == itrCat->second.end()) {
            std::cerr << "Extractor::DataFitCombined: Cannot find stat uncertainty\n";
            return;
        }
        auto itrSyst = itrCat->second.find("SYST");
        if (itrSyst == itrCat->second.end()) {
            std::cerr << "Extractor::DataFitCombined: Cannot find syst uncertainty\n";
            return;
        }
        auto itrTotal = itrCat->second.find("TOTAL");
        if (itrTotal == itrCat->second.end()) {
            std::cerr << "Extractor::DataFitCombined: Cannot find total uncertainty\n";
            return;
        }

        const double syst_up = itrSyst->second.first;
        const double syst_down = itrSyst->second.second;

        itrStat->second.first = up;
        itrStat->second.second = down;

        const double total_new_up = std::hypot(syst_up, up);
        const double total_new_down = std::hypot(syst_down, down);

        itrTotal->second.first = total_new_up;
        itrTotal->second.second = total_new_down;
    }

    const std::string out_file_name = m_outputFolder + "/" + (m_is_jes_fit ? "JES" : "JER") + "_results.root";
    std::unique_ptr<TFile> out(TFile::Open(out_file_name.c_str(), "recreate"));

    this->StoreResults(out.get());
    this->DrawCorrelationMatrix(*fitter, out.get(), false);
    this->PlotFinalUncertaintyCombined(out.get(), false);

    auto nominal_single = this->GetAsimovSingleRegions();
    auto nominal_cross = this->GetAsimovCrossRegions();

    this->DrawPrePost(*fitter, nominal_single, nominal_cross, single_region_histos, cross_region_histos);

    out->Close();
}

std::vector<TH1D> Extractor::GetDataHistogramSingleRegion() const {
    std::vector<TH1D> result;
    for (const auto& ireg : m_single_regions) {
        const std::string suffix = "Jet_" + ireg.first + "_eta_0p8";
        std::unique_ptr<TH1D> hist(m_data_file->Get<TH1D>(("NOSYS/" + m_obs_name + "_" + suffix).c_str()));
        if (!hist) {
            std::cerr << "Cannot read data histogram with suffix: " << suffix << "\n";
            exit(1);
        }

        result.emplace_back(std::move(*hist));
    }

    return result;
}

std::vector<TH1D> Extractor::GetDataHistogramCrossRegion() const {
    std::vector<TH1D> result;
    for (const auto& ireg : m_cross_regions) {
        const std::string suffix = "Jet_" + ireg.first + "_eta_0p8";
        std::unique_ptr<TH1D> hist(m_data_file->Get<TH1D>(("NOSYS/" + m_obs_name + "_" + suffix).c_str()));
        if (!hist) {
            std::cerr << "Cannot read data histogram with suffix: " << suffix << "\n";
            exit(1);
        }

        result.emplace_back(std::move(*hist));
    }

    return result;
}

std::vector<TH1D> Extractor::GetClosureHistogramsSingleRegions(const std::string& region1,
                                                               const std::string& region2,
                                                               const int index1,
                                                               const int index2,
                                                               const bool is_jes) const {

    std::vector<TH1D> result;
    for (const auto& ireg : m_single_regions) {
        std::string name = "NOSYS/"+m_template_name + "_" + (is_jes ? "s" : "r") + "_";
        if (ireg.second == region1 ) {
            name += std::to_string(index1);
        } else if (ireg.second == region2) {
            name += std::to_string(index2);
        } else {
            name += "5"; // central
        }

        name += std::string("_isol_0p5_") + ireg.first + "_" + (is_jes ? "jes" : "jer") + "_" + m_region;

        std::unique_ptr<TH1D> tmp(static_cast<TH1D*>(m_ttbar_file->Get(name.c_str())));
        if (!tmp) {
            std::cerr << "Extractor::GetClosureHistogramsSingleRegions: Cannot read histogram: " << name << "\n";
            exit(1);
        }

        std::string bkg_name = "NOSYS/"+m_obs_name + "_Jet_" + ireg.first + "_eta_0p8";
        /// Add backgrounds
        for (auto& ifile : m_background_files) {
            std::unique_ptr<TH1D> tmp_bkg(static_cast<TH1D*>(ifile->Get(bkg_name.c_str())));
            if (!tmp_bkg) {
                std::cerr << "Extractor::GetClosureHistogramsSingleRegions: Cannot read histogram: " << bkg_name << std::endl;
                exit(EXIT_FAILURE);
            }
            tmp->Add(tmp_bkg.get());
        }

        Common::SetErrorsToExpected(tmp.get());
        tmp->GetXaxis()->SetRange(0,0);

        result.emplace_back(std::move(*tmp));
    }

    return result;
}

std::vector<TH1D> Extractor::GetClosureHistogramsCrossRegions(const std::string& region1,
                                                              const std::string& region2,
                                                              const int index1,
                                                              const int index2,
                                                              const bool is_jes) const {

    std::vector<TH1D> result;
    for (const auto& ireg : m_cross_regions) {
        std::string name = "NOSYS/"+m_template_name + "_" + (is_jes ? "s1" : "r1") + "_";
        if (ireg.second.first == region1) {
            name += std::to_string(index1);
        } else if (ireg.second.first == region2) {
            name += std::to_string(index2);
        } else {
            name += "5"; // central
        }
        name += std::string("_") + (is_jes ? "s2" : "r2") + "_";
        if (ireg.second.second == region1) {
            name += std::to_string(index1);
        } else if (ireg.second.second == region2) {
            name += std::to_string(index2);
        } else {
            name += "5"; // central
        }

        name += std::string("_isol_0p5_") + ireg.first + "_" + (is_jes ? "jes" : "jer") + "_" + m_region;

        std::unique_ptr<TH1D> tmp(static_cast<TH1D*>(m_ttbar_file->Get(name.c_str())));
        if (!tmp) {
            std::cerr << "Extractor::GetClosureHistogramsCrossRegions: Cannot read histogram: " << name << "\n";
            exit(1);
        }

        std::string bkg_name = "NOSYS/"+m_obs_name + "_Jet_" + ireg.first + "_eta_0p8";
        /// Add backgrounds
        for (auto& ifile : m_background_files) {
            std::unique_ptr<TH1D> tmp_bkg(static_cast<TH1D*>(ifile->Get(bkg_name.c_str())));
            if (!tmp_bkg) {
                std::cerr << "Extractor::GetClosureHistogramsCrossRegions: Cannot read histogram: " << bkg_name << std::endl;
                exit(EXIT_FAILURE);
            }
            tmp->Add(tmp_bkg.get());
        }

        Common::SetErrorsToExpected(tmp.get());
        tmp->GetXaxis()->SetRange(0,0);

        result.emplace_back(std::move(*tmp));
    }

    return result;
}

std::map<std::string, double> Extractor::RunClosureCombined(FFFitter* fitter, const bool is_jes, std::map<std::string, double> result) {
    std::cout << "Extractor::RunClosureCombined: Running closure varying " << (is_jes ? "JES" : "JER") << "\n";

    std::vector<std::string> regions;
    for (int i = m_start_index; i < 7; ++i) {
        regions.emplace_back((m_is_jes_fit? "JES" : "JER") + std::to_string(i));
    }

    for (std::size_t i = 0; i < regions.size(); ++i) {
        for (std::size_t j = i; j < regions.size(); ++j) {
            const std::string region1 = regions.at(i);
            const std::string region2 = regions.at(j);

            for (const int index1 : m_closure_indices_combined) {
                for (const int index2 : m_closure_indices_combined) {

                    if ((region1 == region2) && (index1 != index2)) continue;

                    std::vector<TH1D> histos_single_regions = this->GetClosureHistogramsSingleRegions(region1, region2, index1, index2, is_jes);
                    std::vector<TH1D> histos_cross_regions = this->GetClosureHistogramsCrossRegions(region1, region2, index1, index2, is_jes);

                    const std::string param1 = region1;
                    const std::string param2 = region2;

                    std::map<std::string, double> results = fitter->FitCombinedModel(histos_single_regions, histos_cross_regions, false, -1);

                    for (const auto& iresult : results) {
                        const double par_value = iresult.second;

                        double input(1);

                        if (iresult.first == param1) {
                            input = fitter->IndexToValue(index1, m_is_jes_fit? FoldingParameter::PARAMETER::S : FoldingParameter::PARAMETER::R);
                        }
                        if (iresult.first == param2) {
                            input = fitter->IndexToValue(index2, m_is_jes_fit? FoldingParameter::PARAMETER::S : FoldingParameter::PARAMETER::R);
                        }
                        if (m_is_jes_fit && !is_jes) {
                            input = 1.0;
                        }
                        if (!m_is_jes_fit && is_jes) {
                            input = 1.0;
                        }

                        //std::cout << std::fixed << std::setprecision(4) << "Param: " << iresult.first << " input: " << input << " fitted: " << par_value << "\n";

                        const double diff = std::abs(par_value - input);

                        auto itr1 = result.find(iresult.first);
                        if (itr1 == result.end()) {
                            result.insert({iresult.first, diff});
                        } else {
                            if (itr1->second < diff) {
                                itr1->second = diff;
                            }
                        }
                    }
                    //std::cout << "\n";
                }
            }
        }
    }

    for (int i = m_start_index; i < 7; ++i) {
        const std::string name = (m_is_jes_fit ? "JES" : "JER") + std::to_string(i);
        auto itr = result.find(name);
        if (itr == result.end()) {
            std::cerr << "Extractor::RunClosureCombined: Cannot find parameter in the result: " << name << "\n";
            return result;
        }

        const double unc = itr->second;

        auto itrAll = m_combined_fit_uncertainty_all.find(name);
        if (itrAll == m_combined_fit_uncertainty_all.end()) {
            std::cerr << "Extractor::RunClosureCombined: Cannot find param: " << name << " in all uncertainties\n";
            return result;
        }

        itrAll->second.insert({"non-closure", {unc, -unc}});

        auto itrCat = m_combined_fit_uncertainty_category.find(name);
        if (itrCat == m_combined_fit_uncertainty_category.end()) {
            std::cerr << "Extractor::RunClosureCombined: Cannot find param: " << name << " in cattegory uncertainties\n";
            return result;
        }

        itrCat->second.insert({"non-closure", {unc, -unc}});

        std::cout << "Maximum difference varying " << (is_jes ? "JES" : "JER") <<" for parameter " << name << ": " << unc << "\n";
    }

    return result;
}

void Extractor::SetSingleRegions(const std::vector<std::pair<std::string, std::string> >& regions) {
    for (const auto& ireg : regions)  {
        const std::string to = m_is_jes_fit ? "JES" : "JER";
        m_single_regions.emplace_back(std::make_pair(ireg.first, Common::ReplaceString(ireg.second, "param", to)));
    }
}

void Extractor::SetCrossRegions(const std::vector<std::pair<std::string, std::pair<std::string, std::string> > >& regions) {
    for (const auto& ireg : regions)  {
        const std::string to = m_is_jes_fit ? "JES" : "JER";
        std::pair<std::string, std::string> pair = std::make_pair(Common::ReplaceString(ireg.second.first, "param", to), Common::ReplaceString(ireg.second.second, "param", to));
        m_cross_regions.emplace_back(std::make_pair(ireg.first, pair));
    }
}

void Extractor::PlotFinalUncertaintyCombined(TFile* out, const bool is_asimov) const {
    TCanvas c("", "", 800, 800);

    const std::string print_name = std::string("Results/FinalUncertaintyCombined_") + (m_is_jes_fit ? "JES" : "JER") + "_" + (is_asimov ? "Asimov" : "Data");

    TGraphAsymmErrors total(m_combined_fit_uncertainty_category.size());
    TGraphAsymmErrors stat(m_combined_fit_uncertainty_category.size());

    total.SetMinimum(m_is_jes_fit ? 0.9 : 0.6);
    total.SetMaximum(m_is_jes_fit ? 1.05 : 1.4);

    total.SetMarkerStyle(2);
    stat.SetMarkerStyle(2);

    total.SetLineWidth(3);
    stat.SetLineWidth(3);

    total.SetLineColor(kRed);
    total.SetMarkerColor(kRed);
    stat.SetLineColor(kBlue);
    stat.SetMarkerColor(kBlue);

    total.GetYaxis()->SetTitle((std::string("Parameter ") + (m_is_jes_fit? "JES" : "JER")).c_str());
    total.GetYaxis()->SetTitleOffset(1.3*total.GetYaxis()->GetTitleOffset());
    total.GetYaxis()->SetTitleSize(1.5*total.GetYaxis()->GetTitleSize());
    total.GetXaxis()->SetTitle("jet p_{T} [GeV]");
    total.GetXaxis()->SetTitleOffset(1.2*total.GetXaxis()->GetTitleOffset());
    total.GetXaxis()->SetTitleSize(1.5*total.GetXaxis()->GetTitleSize());

    for (std::size_t ipoint = m_start_index; ipoint < 7; ++ipoint) {
        const std::string param = (m_is_jes_fit ? "JES" : "JER") + std::to_string(ipoint);

        auto itrUnc = m_combined_fit_uncertainty_category.find(param);
        if (itrUnc == m_combined_fit_uncertainty_category.end()) {
            std::cerr << "Extractor::PlotFinalUncertaintyCombined: Cannot find parameter: " << param << "\n";
            return;
        }

        auto uncTotal = itrUnc->second.find("TOTAL");
        auto uncStat = itrUnc->second.find("STAT");

        if (uncTotal == itrUnc->second.end() || uncStat == itrUnc->second.end()) {
            std::cerr << "Extractor::PlotFinalUncertaintyCombined: Cannot find total or stat uncertainty\n";
            return;
        }

        std::string region("");
        if (ipoint == 1) {
            //region = "pt_20_30_20_30";
            region = "pt_20_35_20_35";
        } else {
            auto itr = std::find_if(m_single_regions.begin(), m_single_regions.end(), [&param](const auto& element){return element.second == param;});
            if (itr == m_single_regions.end()) {
                std::cerr << "Extractor::PlotFinalUncertaintyCombined: Cannot find region for parameter: " << param << "\n";
                return;
            }

            region = itr->first;
        }

        const auto range = Common::RegionToRangeValue(region);
        const double x = 0.5*(range.second + range.first);
        const double x_error = 0.5*(range.second - range.first);

        auto itrCentral = is_asimov ? m_combined_fit_central.find(param) : m_combined_fit_central_data.find(param);
        if (itrCentral == (is_asimov ? m_combined_fit_central.end() : m_combined_fit_central_data.end())) {
            std::cerr << "Extractor::PlotFinalUncertaintyCombined: Cannot find param: " << param << ", in the central values\n";
            return;
        }

        total.SetPointX(ipoint, x);
        total.SetPointEXhigh(ipoint, x_error);
        total.SetPointEXlow(ipoint, x_error);
        total.SetPointY(ipoint, itrCentral->second);
        total.SetPointEYhigh(ipoint, std::abs(uncTotal->second.first));
        total.SetPointEYlow(ipoint, std::abs(uncTotal->second.second));

        stat.SetPointX(ipoint, x);
        stat.SetPointEXhigh(ipoint, x_error);
        stat.SetPointEXlow(ipoint, x_error);
        stat.SetPointY(ipoint, itrCentral->second);
        stat.SetPointEYhigh(ipoint, std::abs(uncStat->second.first));
        stat.SetPointEYlow(ipoint, std::abs(uncStat->second.second));

        //std::cout << "x: " << x << ", stat: " << uncStat->second.first << ", down: " << uncStat->second.second << ", total: " << uncTotal->second.first << ", down: " << uncTotal->second.second << "\n";
    }

    total.Draw("AP");
    stat.Draw("P same");

    ATLASLabel(0.2,0.90, "Internal");

    TLatex l;
    l.SetTextAlign(9);
    l.SetTextSize(0.03);
    l.SetTextFont(42);
    l.SetNDC();
    l.DrawLatex(0.2, 0.85, "#sqrt{s} = 13 TeV, 140 fb^{-1}");

    TLegend leg(0.7, 0.8,0.9,0.9);
    leg.AddEntry(&stat, "Stat. uncertainty", "p");
    leg.AddEntry(&total, "Total uncertainty", "p");
    leg.SetFillColor(0);
    leg.SetLineColor(0);
    leg.SetBorderSize(0);
    leg.SetTextFont(42);
    leg.SetTextSize(0.025);
    leg.Draw("same");

    const double min_line = 0;
    const double max_line = 220;

    TLine line(min_line,1,max_line,1);
    line.SetLineWidth(3);
    line.SetLineStyle(2);
    line.SetLineColor(kBlack);
    if (is_asimov) {
        line.Draw("same");
    }

    if (out) {
        out->cd();
        stat.Write("tgraph_stat_uncertainty");
        total.Write("tgraph_total_uncertainty");
    }

    c.Print((print_name+".png").c_str());
}

std::unique_ptr<TH1D> Extractor::GetAsimovSingleHisto(const std::string& folder, const std::string& obs) const {

    const std::string final_name = folder + "/" + obs;
    std::unique_ptr<TH1D> asimov(m_ttbar_file->Get<TH1D>((final_name).c_str()));
    if (!asimov) {
        std::cerr << "Extractor::GetAsimovSingleHisto: Cannot read 'ttbar' histogram: " << final_name << std::endl;
        exit(EXIT_FAILURE);
    }

    const std::string bkg_name = "NOSYS/" + obs;
    // add backgrounds
    for (const auto& ibkg : m_background_files) {
        std::unique_ptr<TH1D> bkg(ibkg->Get<TH1D>((bkg_name).c_str()));
        if (!bkg) {
            std::cerr << "Extractor::GetAsimovSingleHisto: Cannot read 'bkg' histogram: " << bkg_name << std::endl;
            exit(EXIT_FAILURE);
        }
        bkg->SetDirectory(nullptr);
        asimov->Add(bkg.get());
    }

    asimov->SetDirectory(nullptr);

    return asimov;
}

void Extractor::RunPseudoExperimentsCombined(FFFitter* fitter) {
    auto single_regions_histos = this->GetAsimovSingleRegions();
    auto cross_regions_histos = this->GetAsimovCrossRegions();

    std::map<std::string, std::vector<double> > toys = fitter->RunCombinedPseudoExperiments(single_regions_histos, cross_regions_histos, m_PE_number);

    const std::string prefix = m_outputFolder+"/PseudoExperiments_";

    for (int i = m_start_index; i < 7; ++i) {
        const std::string param = (m_is_jes_fit ? "JES" : "JER") + std::to_string(i);

        auto itr = toys.find(param);
        if (itr == toys.end()) {
            std::cerr << "Extractor::RunPseudoExperimentsCombined: Cannot find parameter: " << param << "\n";
            return;
        }

        const std::vector<double> results = itr->second;

        const std::string path = prefix+param;

        auto itrStat = m_combined_fit_uncertainty_category.find(param);
        if (itrStat == m_combined_fit_uncertainty_category.end()) {
            std::cerr << "Cannot find parameter: " << param << " in the category map\n";
            return;
        }

        auto itrError = itrStat->second.find("STAT");
        if (itrError == itrStat->second.end()) {
            std::cerr << "Cannot find \"STAT\" category\n";
            return;
        }

        const double error = 0.5*(itrError->second.second - itrError->second.first);

        const double min = 1 - 5*error;
        const double max = 1 + 5*error;

        double error_PE(0);

        const double sigma_PE = Common::GetSigmaGaus(results, min, max, error_PE, path, m_is_jes_fit, "");

        std::cout << "Parameter: " << param << " sigma from toys: " << sigma_PE << "\n";
    }
}

void Extractor::DrawCorrelationMatrix(const FFFitter& fitter, TFile* out, const bool is_asimov) const {
    auto hist = fitter.GetCorrelationMatrix();

    if (!hist) {
        std::cerr << "Extractor::DrawCorrelationMatrix: Histo is nullptr\n";
        return;
    }

    TCanvas canvas("","", 800, 600);
    canvas.SetLeftMargin(0.1);
    canvas.SetBottomMargin(0.11);
    canvas.SetTopMargin(0.1);
    canvas.SetTickx(0);
    canvas.SetTicky(0);
    canvas.SetGrid();

    gStyle->SetPalette(87);
    gStyle->SetPaintTextFormat(".2f");

    hist->GetXaxis()->LabelsOption("v");
    hist->Draw("COL text");

    ATLASLabel(0.2,0.95, "Internal");

    TLatex l;
    l.SetTextAlign(9);
    l.SetTextSize(0.03);
    l.SetTextFont(42);
    l.SetNDC();
    l.DrawLatex(0.5, 0.95, "#sqrt{s} = 13 TeV, 140 fb^{-1}");

    canvas.RedrawAxis("g");

    if (out) {
        out->cd();
        hist->Write("correlation_matrix");
    }

    const std::string print_name = std::string("Results/Correlations_") + (m_is_jes_fit ? "JES" : "JER") + "_" + (is_asimov ? "Asimov" : "Data") + ".png";
    canvas.Print(print_name.c_str());
}

void Extractor::RunProfiledFit(FFFitter* fitter) {
    if (m_use_syst) {
        fitter->SetImpactMap(this->GetImpacts());
    }
    fitter->PrepareCombinedFitParametrisation();
    fitter->PrepareWSCombined();

    auto single_histos = m_fit_data ? this->GetDataHistogramSingleRegion() : this->GetAsimovSingleRegions();
    auto cross_histos = m_fit_data ? this->GetDataHistogramCrossRegion() : this->GetAsimovCrossRegions();

    auto result = fitter->FitCombinedModel(single_histos, cross_histos, false);
    //this->PlotFinalUncertaintyCombined(m_fit_data);
}

std::map<std::string, std::pair<std::vector<double>, std::vector<double> > > Extractor::GetImpacts() const {
    std::cout << "Extractor::GetImpacts: Getting impacts for all uncertainties\n";

    const std::string plot_folder = m_outputFolder + "/Systematics/";
    gSystem->mkdir(plot_folder.c_str());

    const std::vector<std::string>& unique_systs = Common::GetSystUniqueNames(m_systematics);

    auto nominal_single_regions = this->GetAsimovSingleRegions();
    auto nominal_cross_regions = this->GetAsimovCrossRegions();

    std::vector<TH1D> data_single;
    std::vector<TH1D> data_cross;

    if (m_data_file) {
        data_single = this->GetDataHistogramSingleRegion();
        data_cross = this->GetDataHistogramCrossRegion();
    }

    auto nominal_single = Common::GetMeanRMS(nominal_single_regions, m_is_jes_fit);
    auto nominal_cross  = Common::GetMeanRMS(nominal_cross_regions, m_is_jes_fit);

    std::map<std::string, std::pair<std::vector<double>, std::vector<double> > > result;

    for (const auto& isyst : unique_systs) {

        std::vector<double> single_impact;
        std::vector<double> cross_impact;

        const std::vector<Systematic>& systs = Common::GetSystSameName(m_systematics, isyst);
        /// Now each systematic should impact only one sample!
        if (!Common::SystematicsAreConsistent(systs)) {
            std::cerr << "Extractor::GetImpacts: There is something really weird happening for systematic: " << isyst << ", investigate!" << std::endl;
            exit(EXIT_FAILURE);
        }

        if (systs.size() == 0) continue;

        if (systs.at(0).type == SYSTEMATICTYPE::ONESIDED) {
            auto hist_single_regions = this->GetShiftedHistSingleRegions(systs, true, false);
            auto hist_cross_regions = this->GetShiftedHistCrossRegions(systs, true, false);

            auto up_single_impact = Common::GetMeanRMS(hist_single_regions, m_is_jes_fit);
            auto up_cross_impact = Common::GetMeanRMS(hist_cross_regions, m_is_jes_fit);

            if (systs.at(0).target == systs.at(0).reference_file) {

                for (std::size_t i = 0; i < up_single_impact.size(); ++i) {
                    single_impact.emplace_back(up_single_impact.at(i) - nominal_single.at(i));

                }
                for (std::size_t i = 0; i < up_cross_impact.size(); ++i) {
                    cross_impact.emplace_back(up_cross_impact.at(i) - nominal_cross.at(i));
                }

                this->PlotRedBlue(isyst,
                                  nominal_single_regions,
                                  nominal_cross_regions,
                                  hist_single_regions,
                                  hist_cross_regions,
                                  {},
                                  {},
                                  data_single,
                                  data_cross,
                                  std::make_pair(single_impact, cross_impact));
            } else {
                auto ref_single_regions = this->GetShiftedHistSingleRegions(systs, true, true);
                auto ref_cross_regions = this->GetShiftedHistCrossRegions(systs, true, true);

                auto ref_single = Common::GetMeanRMS(ref_single_regions, m_is_jes_fit);
                auto ref_cross = Common::GetMeanRMS(ref_cross_regions, m_is_jes_fit);

                //if (isyst == "ttbar_PowhegHerwig") {
                //    for (std::size_t i = 0; i < hist_single_regions.size(); ++i) {
                //        std::vector<double> tmp_impact_single(hist_single_regions.size(), 0);
                //        std::vector<double> tmp_impact_cross(hist_cross_regions.size(), 0);
                //        const std::string name = isyst + "_single_bin_" + std::to_string(i);
                //        tmp_impact_single.at(i) = up_single_impact.at(i) - nominal_single.at(i);
                //        result.insert({name, std::make_pair(tmp_impact_single, tmp_impact_cross)});
                //    }
                //    for (std::size_t i = 0; i < hist_cross_regions.size(); ++i) {
                //        std::vector<double> tmp_impact_single(hist_single_regions.size(), 0);
                //        std::vector<double> tmp_impact_cross(hist_cross_regions.size(), 0);
                //        const std::string name = isyst + "_cross_bin_" + std::to_string(i);
                //        tmp_impact_cross.at(i) = up_cross_impact.at(i) - nominal_cross.at(i);
                //        result.insert({name, std::make_pair(tmp_impact_single, tmp_impact_cross)});
                //    }
                //    continue;
                //} else {
                    for (std::size_t i = 0; i < up_single_impact.size(); ++i) {
                        single_impact.emplace_back(up_single_impact.at(i) - ref_single.at(i));
                        //std::cout << "bin: " << i << ", up: " << up_single_impact.at(i) << ", nominal: " << nominal_single.at(i) << ", reference: " << ref_single.at(i) << ", diff: " << up_single_impact.at(i) - ref_single.at(i) << "\n";
                    }
                    for (std::size_t i = 0; i < up_cross_impact.size(); ++i) {
                        cross_impact.emplace_back(up_cross_impact.at(i) - ref_cross.at(i));
                    }
                //}

                this->PlotRedBlue(isyst,
                                  ref_single_regions,
                                  ref_cross_regions,
                                  hist_single_regions,
                                  hist_cross_regions,
                                  {},
                                  {},
                                  data_single,
                                  data_cross,
                                  std::make_pair(single_impact, cross_impact));
            }
        } else if (systs.at(0).type == SYSTEMATICTYPE::TWOSIDED || systs.at(0).type == SYSTEMATICTYPE::NORMALISATION) {
            auto hist_single_regions_up = this->GetShiftedHistSingleRegions(systs, true, false);
            auto hist_cross_regions_up = this->GetShiftedHistCrossRegions(systs, true, false);
            auto hist_single_regions_down = this->GetShiftedHistSingleRegions(systs, false, false);
            auto hist_cross_regions_down = this->GetShiftedHistCrossRegions(systs, false, false);

            auto up_single_impact = Common::GetMeanRMS(hist_single_regions_up, m_is_jes_fit);
            auto up_cross_impact = Common::GetMeanRMS(hist_cross_regions_up, m_is_jes_fit);
            auto down_single_impact = Common::GetMeanRMS(hist_single_regions_down, m_is_jes_fit);
            auto down_cross_impact = Common::GetMeanRMS(hist_cross_regions_down, m_is_jes_fit);

            auto GetFinalImpact = [&isyst, this](const double up, const double down, int i, const bool is_single) {
                double final_impact(0);
                if (up * down > 0) {
                    // same sign

                    if (std::abs(up) > std::abs(down)) {
                        final_impact = up;
                    } else {
                        final_impact = down;
                    }
                    if (this->m_debug) {
                        if (std::abs(up) > 1e-3 || std::abs(down) > 1e-3) {
                            std::cout << "Warning: uncertainty: " << isyst << ", " << (is_single ? "single" : "cross") << " bin: " << i << ", has asymmetric impact\n";
                            std::cout << "Impact up: " << up << ", down: " << down << "\n";
                            std::cout << "using: " << final_impact << "\n";
                        }
                    }
                } else {
                    final_impact = 0.5*(up - down);
                }

                return final_impact;
            };

            for (std::size_t i = 0; i < up_single_impact.size(); ++i) {
                double impact_up = up_single_impact.at(i) - nominal_single.at(i);
                double impact_down = down_single_impact.at(i) - nominal_single.at(i);
                single_impact.emplace_back(GetFinalImpact(impact_up, impact_down, i, true));
            }
            for (std::size_t i = 0; i < up_cross_impact.size(); ++i) {
                double impact_up = up_cross_impact.at(i) - nominal_cross.at(i);
                double impact_down = down_cross_impact.at(i) - nominal_cross.at(i);
                cross_impact.emplace_back(GetFinalImpact(impact_up, impact_down, i, false));
            }
            this->PlotRedBlue(isyst,
                              nominal_single_regions,
                              nominal_cross_regions,
                              hist_single_regions_up,
                              hist_cross_regions_up,
                              hist_single_regions_down,
                              hist_cross_regions_down,
                              data_single,
                              data_cross,
                              std::make_pair(single_impact, cross_impact));
        } else {
            std::cerr << "Extractor::GetImpacts: Unknown systematic type!" << std::endl;
            exit(EXIT_FAILURE);
        }

        result.insert(std::make_pair(isyst, std::make_pair(single_impact, cross_impact)));
    }

    for (std::size_t i = 0; i < nominal_single_regions.size(); ++i) {
        const std::string name = "mc_stat_single_bin_" + std::to_string(i);
        const double impact = m_is_jes_fit ? nominal_single_regions.at(i).GetMeanError() : nominal_single_regions.at(i).GetRMSError();
        std::vector<double> mc_stat_single(nominal_single_regions.size(), 0.);
        std::vector<double> tmp(nominal_cross_regions.size(), 0.);
        mc_stat_single.at(i) = impact;

        result.insert(std::make_pair(name, std::make_pair(mc_stat_single, tmp)));
    }

    for (std::size_t i = 0; i < nominal_cross_regions.size(); ++i) {
        const std::string name = "mc_stat_cross_bin_" + std::to_string(i);
        const double impact = m_is_jes_fit ? nominal_cross_regions.at(i).GetMeanError() : nominal_cross_regions.at(i).GetRMSError();
        std::vector<double> tmp(nominal_single_regions.size(), 0.);
        std::vector<double> mc_stat_cross(nominal_cross_regions.size(), 0.);
        mc_stat_cross.at(i) = impact;

        result.insert(std::make_pair(name, std::make_pair(tmp, mc_stat_cross)));
    }

    std::cout << "Extractor::GetImpacts: Done getting impacts for all uncertainties\n";

    return result;
}

void Extractor::PlotRedBlue(const std::string& name,
                            const std::vector<TH1D>& nominal_single,
                            const std::vector<TH1D>& nominal_cross,
                            const std::vector<TH1D>& syst_up_single,
                            const std::vector<TH1D>& syst_up_cross,
                            const std::vector<TH1D>& syst_down_single,
                            const std::vector<TH1D>& syst_down_cross,
                            const std::vector<TH1D>& data_single,
                            const std::vector<TH1D>& data_cross,
                            const std::pair<std::vector<double>, std::vector<double> >& impacts) const {

    const int bins_single = nominal_single.size();
    const int bins_cross = nominal_cross.size();
    const int bins_total = bins_single + bins_cross;

    TH1D nominal("","", bins_total, 0.5, bins_total+0.5);
    TH1D up("","", bins_total, 0.5, bins_total+0.5);
    TH1D down("","", bins_total, 0.5, bins_total+0.5);
    TGraphErrors data{};
    for (int ibin = 1; ibin <= bins_single; ++ibin) {
        const double value_nominal = m_is_jes_fit ? nominal_single.at(ibin-1).GetMean() : nominal_single.at(ibin-1).GetRMS();
        nominal.SetBinContent(ibin, value_nominal);
        const double value_up = m_is_jes_fit ? syst_up_single.at(ibin-1).GetMean() : syst_up_single.at(ibin-1).GetRMS();
        const double error_up = m_is_jes_fit ? syst_up_single.at(ibin-1).GetMeanError() : syst_up_single.at(ibin-1).GetRMSError();
        up.SetBinContent(ibin, value_up);
        up.SetBinError(ibin, error_up);
        if (!syst_down_single.empty()) {
            const double value_down = m_is_jes_fit ? syst_down_single.at(ibin-1).GetMean() : syst_down_single.at(ibin-1).GetRMS();
            const double error_down = m_is_jes_fit ? syst_down_single.at(ibin-1).GetMeanError() : syst_down_single.at(ibin-1).GetRMSError();
            down.SetBinContent(ibin, value_down);
            down.SetBinError(ibin, error_down);
        }
        if (!data_single.empty()) {
            const double data_value = m_is_jes_fit ? data_single.at(ibin -1).GetMean() : data_single.at(ibin-1).GetRMS();
            const double data_error = m_is_jes_fit ? data_single.at(ibin -1).GetMeanError() : data_single.at(ibin-1).GetRMSError();
            data.AddPointError(ibin, data_value, 0, data_error);
        }
    }
    for (int ibin = 1; ibin <= bins_cross; ++ibin) {
        const double value_nominal = m_is_jes_fit ? nominal_cross.at(ibin-1).GetMean() : nominal_cross.at(ibin-1).GetRMS();
        nominal.SetBinContent(ibin + bins_single, value_nominal);
        const double value_up = m_is_jes_fit ? syst_up_cross.at(ibin-1).GetMean() : syst_up_cross.at(ibin-1).GetRMS();
        const double error_up = m_is_jes_fit ? syst_up_cross.at(ibin-1).GetMeanError() : syst_up_cross.at(ibin-1).GetRMSError();
        up.SetBinContent(ibin + bins_single, value_up);
        up.SetBinError(ibin + bins_single, error_up);
        if (!syst_down_cross.empty()) {
            const double value_down = m_is_jes_fit ? syst_down_cross.at(ibin-1).GetMean() : syst_down_cross.at(ibin-1).GetRMS();
            const double error_down = m_is_jes_fit ? syst_down_cross.at(ibin-1).GetMeanError() : syst_down_cross.at(ibin-1).GetRMSError();
            down.SetBinContent(ibin + bins_single, value_down);
            down.SetBinError(ibin + bins_single, error_down);
        }
        if (!data_cross.empty()) {
            const double data_value = m_is_jes_fit ? data_cross.at(ibin -1).GetMean() : data_cross.at(ibin-1).GetRMS();
            const double data_error = m_is_jes_fit ? data_cross.at(ibin -1).GetMeanError() : data_cross.at(ibin-1).GetRMSError();
            data.AddPointError(ibin + bins_single, data_value, 0, data_error);
        }
    }

    nominal.SetLineColor(kBlack);
    up.SetLineColor(kRed);
    down.SetLineColor(kBlue);

    TCanvas c("","",800,600);
    TPad pad1("pad1","pad1",0.0, 0.3, 1.0, 1.00);
    TPad pad2("pad2","pad2", 0.0, 0.010, 1.0, 0.3);
    pad1.SetBottomMargin(0.001);
    pad1.SetBorderMode(0);
    pad2.SetBottomMargin(0.5);
    pad1.SetTicks(1,1);
    pad2.SetTicks(1,1);
    pad1.Draw();
    pad2.Draw();

    pad1.cd();

    nominal.GetYaxis()->SetRangeUser(70.1, 85);
    nominal.GetYaxis()->SetTitle(m_is_jes_fit ? "Mean" : "RMS");
    nominal.SetMarkerSize(0);
    nominal.Draw("hist");
    up.SetMarkerSize(0);
    up.Draw("hist e same");
    if (!syst_down_single.empty()) {
        down.SetMarkerSize(0);
        down.Draw("hist e same");
    }
    if (!data_single.empty()) {
        data.Draw("PE same");
    }

    TLegend leg(0.6, 0.2,0.9,0.5);
    leg.AddEntry(&nominal, "Nominal (reference)", "l");
    leg.AddEntry(&up, "Uncertainty up", "l");
    if (!syst_down_cross.empty()) {
        leg.AddEntry(&down, "Uncertainty down", "l");
    }
    if (!data_single.empty()) {
        leg.AddEntry(&data, "Data", "p");
    }
    leg.SetFillColor(0);
    leg.SetLineColor(0);
    leg.SetBorderSize(0);
    leg.SetTextFont(42);
    leg.SetTextSize(0.025);
    leg.Draw("same");

    pad2.cd();

    TH1D impact("","", bins_total, 0.5, bins_total+0.5);

    for (int ibin = 1; ibin <= bins_single; ++ibin) {
        impact.SetBinContent(ibin, impacts.first.at(ibin-1));
    }
    for (int ibin = 1; ibin <= bins_cross; ++ibin) {
        impact.SetBinContent(ibin + bins_single, impacts.second.at(ibin-1));
    }

    impact.GetYaxis()->SetRangeUser(-0.2, 0.2);
    impact.GetYaxis()->SetTitle("Impact");
    impact.GetYaxis()->SetNdivisions(505);

    impact.Draw("hist");

    TLine line(0.5, 0, bins_total + 0.5, 0);
    line.SetLineWidth(2);
    line.SetLineStyle(2);
    line.Draw("same");

    const std::string output_name = m_outputFolder + "/Systematics/" + name + "_" + (m_is_jes_fit ? "JES" : "JER") + ".png";
    c.Print(output_name.c_str());
}

void Extractor::StoreResults(TFile* out) const {
    if (!out) {
        std::cerr << "Extractor::StoreResults: TFile is nullptr\n";
        return;
    }

    std::cout << "Extractor::StoreResults: Storing fit results\n";

    static const std::vector<double> edges = {20, 35, 50, 70, 100, 150, 200};

    // central
    TH1D central("","", edges.size() -1 , edges.data());
    TH1D stat("","",edges.size() -1 , edges.data());
    for (int i = m_start_index; i < 7; ++i) {
        const std::string param = (m_is_jes_fit ? "JES" : "JER") + std::to_string(i);
        auto itr = m_combined_fit_central_data.find(param);
        if (itr == m_combined_fit_central_data.end()) {
            std::cerr << "Extractor::StoreResults: Cannot find param: " << param << "\n";
            return;
        }

        auto itr_stat = m_combined_fit_data.find(param);
        if (itr_stat == m_combined_fit_data.end()) {
            std::cerr << "Extractor::StoreResults: Cannot find param: " << param << " in data\n";
            return;
        }

        central.SetBinContent(i, itr->second);
        central.SetBinError(i, 0);

        stat.SetBinContent(i, itr_stat->second.second);
        stat.SetBinError(i, 0);
    }

    // uncertainties
    const std::string test_param = m_is_jes_fit ? "JES2" : "JER2";
    auto itr_test = m_combined_fit_uncertainty_all.find(test_param);
    if (itr_test == m_combined_fit_uncertainty_all.end()) {
        std::cerr << "Extractor::StoreResults: Cannot find param: " << test_param << "\n";
        return;
    }

    for (const auto& isyst : itr_test->second) {
        const std::string syst = isyst.first;
        TH1D syst_hist("","", edges.size() -1 , edges.data());
        for (int i = m_start_index; i < 7; ++i) {
            const std::string param = (m_is_jes_fit ? "JES" : "JER") + std::to_string(i);
            auto itr = m_combined_fit_uncertainty_all.find(param);
            if (itr == m_combined_fit_uncertainty_all.end()) {
                std::cerr << "Extractor::StoreResults: Cannot find param: " << param << "\n";
                return;
            }

            auto itr_syst = itr->second.find(syst);
            if (itr_syst == itr->second.end()) {
                std::cerr << "Extractor::StoreResults: Cannot find systematic: " << syst << "\n";
                return;
            }

            const double up = itr_syst->second.first;
            const double down = itr_syst->second.second;

            double error(0);

            if (up*down > 0) {
                if (std::abs(up) > std::abs(down)) {
                    error = up;
                } else {
                    error = down;
                }
            } else {
                error = 0.5*(up-down);
            }

            syst_hist.SetBinContent(i, error);
            syst_hist.SetBinError(i, 0);
        }

        out->cd();
        syst_hist.GetXaxis()->SetTitle("jet p_{T} [GeV]");
        syst_hist.GetYaxis()->SetTitle("Uncertainty");
        syst_hist.Write(syst.c_str());
    }

    out->cd();
    central.GetXaxis()->SetTitle("jet p_{T} [GeV]");
    central.GetYaxis()->SetTitle("Central value");
    central.Write("central_values");
    stat.GetXaxis()->SetTitle("jet p_{T} [GeV]");
    stat.GetYaxis()->SetTitle("Uncertainty");
    stat.Write("stat");
}

void Extractor::DrawPrePost(const FFFitter& fitter,
                            const std::vector<TH1D>& nominal_single,
                            const std::vector<TH1D>& nominal_cross,
                            const std::vector<TH1D>& data_single,
                            const std::vector<TH1D>& data_cross) const {

    std::cout << "Extractor::DrawPrePost: Drawing pre-post fit\n";

    const int bins_single = nominal_single.size();
    const int bins_cross = nominal_cross.size();
    const int bins_total = bins_single + bins_cross;

    const auto single_functions = fitter.GetSingleRegionFunctions();
    const auto cross_functions = fitter.GetCrossRegionFunctions();

    TH1D nominal("","", bins_total, 0.5, bins_total+0.5);
    TH1D post("","", bins_total, 0.5, bins_total+0.5);
    TH1D data("","", bins_total, 0.5, bins_total+0.5);
    for (int ibin = 1; ibin <= bins_single; ++ibin) {
        const double value_nominal = m_is_jes_fit ? nominal_single.at(ibin-1).GetMean() : nominal_single.at(ibin-1).GetRMS();
        nominal.SetBinContent(ibin, value_nominal);
        const double data_value = m_is_jes_fit ? data_single.at(ibin -1).GetMean() : data_single.at(ibin-1).GetRMS();
        const double data_error = m_is_jes_fit ? data_single.at(ibin -1).GetMeanError() : data_single.at(ibin-1).GetRMSError();
        data.SetBinContent(ibin, data_value);
        data.SetBinError(ibin, data_error);

        const std::string param = (m_is_jes_fit ? "JES" : "JER") + std::to_string(ibin+1);
        auto itr_central = m_combined_fit_central_data.find(param);
        if (itr_central == m_combined_fit_central_data.end()) {
            std::cerr << "Extractor::DrawPrePost: Cannot find parameter: " << param << " in data\n";
            return;
        }
        auto itr_total = m_combined_fit_uncertainty_category.find(param);
        if (itr_total == m_combined_fit_uncertainty_category.end()) {
            std::cerr << "Extractor::DrawPrePost: Cannot find parameter: " << param << " in uncertainty\n";
            return;
        }

        const double central = single_functions.at(ibin-1).Eval(itr_central->second);
        const double total_up = single_functions.at(ibin-1).Eval(itr_central->second + itr_total->second.at("TOTAL").first);

        post.SetBinContent(ibin, central);
        post.SetBinError(ibin, total_up - central);
    }
    for (int ibin = 1; ibin <= bins_cross; ++ibin) {
        const double value_nominal = m_is_jes_fit ? nominal_cross.at(ibin-1).GetMean() : nominal_cross.at(ibin-1).GetRMS();
        nominal.SetBinContent(ibin + bins_single, value_nominal);
        const double data_value = m_is_jes_fit ? data_cross.at(ibin -1).GetMean() : data_cross.at(ibin-1).GetRMS();
        const double data_error = m_is_jes_fit ? data_cross.at(ibin -1).GetMeanError() : data_cross.at(ibin-1).GetRMSError();
        data.SetBinContent(ibin + bins_single, data_value);
        data.SetBinError(ibin + bins_single, data_error);

        const std::string param1 = m_cross_regions.at(ibin-1).second.first;
        const std::string param2 = m_cross_regions.at(ibin-1).second.second;
        auto itr_central1 = m_combined_fit_central_data.find(param1);
        if (itr_central1 == m_combined_fit_central_data.end()) {
            std::cerr << "Extractor::DrawPrePost: Cannot find parameter: " << param1 << " in data\n";
            return;
        }
        auto itr_total1 = m_combined_fit_uncertainty_category.find(param1);
        if (itr_total1 == m_combined_fit_uncertainty_category.end()) {
            std::cerr << "Extractor::DrawPrePost: Cannot find parameter: " << param1 << " in uncertainty\n";
            return;
        }
        auto itr_central2 = m_combined_fit_central_data.find(param2);
        if (itr_central2 == m_combined_fit_central_data.end()) {
            std::cerr << "Extractor::DrawPrePost: Cannot find parameter: " << param2 << " in data\n";
            return;
        }
        auto itr_total2 = m_combined_fit_uncertainty_category.find(param2);
        if (itr_total2 == m_combined_fit_uncertainty_category.end()) {
            std::cerr << "Extractor::DrawPrePost: Cannot find parameter: " << param2 << " in uncertainty\n";
            return;
        }

        const double up_1 = itr_total1->second.at("TOTAL").first;
        const double up_2 = itr_total2->second.at("TOTAL").first;

        TRandom3 rand(12345);
        std::vector<double> means;
        for (int i = 0; i < 1000; ++i) {
            const double value1 = rand.Gaus(itr_central1->second, up_1);
            const double value2 = rand.Gaus(itr_central2->second, up_2);

            const double mean = cross_functions.at(ibin-1).Eval(value1, value2);
            means.emplace_back(mean);
        }
        const double min = *std::min_element(means.begin(), means.end());
        const double max = *std::max_element(means.begin(), means.end());

        TH1D tmp("","", 20, min, max);
        for (const auto value : means) {
            tmp.Fill(value);
        }

        post.SetBinContent(ibin + bins_single, tmp.GetMean());
        post.SetBinError(ibin + bins_single, tmp.GetRMS());
    }

    TGraphAsymmErrors error(&post);
    error.SetFillStyle(3002);
    error.SetFillColor(kGray+2);
    error.SetMarkerStyle(0);
    error.SetLineWidth(2);

    for (int ibin = 0; ibin < post.GetNbinsX(); ++ibin) {
        error.SetPointEYhigh(ibin, post.GetBinError(ibin+1));
        error.SetPointEYlow(ibin, post.GetBinError(ibin+1));
    }

    nominal.SetLineColor(kBlue);
    post.SetLineColor(kRed);

    nominal.GetXaxis()->SetTickSize(0);
    //nominal.GetYaxis()->SetTitleSize(1.5* nominal.GetYaxis()->GetTitleSize());
    nominal.GetYaxis()->SetTitleOffset(1.4* nominal.GetYaxis()->GetTitleOffset());

    TCanvas c("","",800,600);
    c.SetRightMargin(0.1);
    TPad pad1("pad1","pad1",0.0, 0.3, 1.0, 1.00);
    TPad pad2("pad2","pad2", 0.0, 0.010, 1.0, 0.3);
    pad1.SetBottomMargin(0.01);
    pad1.SetRightMargin(0.1);
    pad2.SetRightMargin(0.1);
    pad1.SetBorderMode(0);
    pad2.SetBottomMargin(0.3);
    pad1.SetTicks(1,1);
    pad2.SetTicks(1,1);
    pad1.Draw();
    pad2.Draw();

    pad1.cd();

    nominal.GetYaxis()->SetRangeUser(m_is_jes_fit ? 70.1 : 4.01, m_is_jes_fit ? 100 : 15);
    nominal.GetYaxis()->SetTitle(m_is_jes_fit ? "Mean" : "RMS");
    nominal.SetMarkerSize(0);
    nominal.Draw("hist");
    post.Draw("hist same");
    error.Draw("E2 same");
    data.Draw("X0EPZ same");

    TLegend leg(0.6, 0.6,0.9,0.9);
    leg.AddEntry(&data, "Data", "p");
    leg.AddEntry(&nominal, "Pre-fit model", "l");
    leg.AddEntry(&post, "Post-fit model", "l");
    leg.AddEntry(&error, "Uncertainty", "f");
    leg.SetFillColor(0);
    leg.SetLineColor(0);
    leg.SetBorderSize(0);
    leg.SetTextFont(42);
    leg.SetTextSize(0.04);
    leg.Draw("same");

    ATLASLabel(0.2,0.885, "Internal");

    TLatex l;
    l.SetTextAlign(9);
    l.SetTextSize(0.05);
    l.SetTextFont(42);
    l.SetNDC();
    l.DrawLatex(0.2, 0.80, "#sqrt{s} = 13 TeV, 140 fb^{-1}");
    pad1.Modified();
    pad1.RedrawAxis();

    pad2.cd();

    std::unique_ptr<TH1D> ratio(static_cast<TH1D*>(post.Clone()));

    // set all errors to zero to get the proper ratio uncertainty
    for (int ibin = 1; ibin <= ratio->GetNbinsX(); ++ibin) {
        ratio->SetBinError(ibin, 0);
    }

    std::unique_ptr<TH1D> data_ratio(static_cast<TH1D*>(data.Clone()));
    TGraphAsymmErrors error_ratio(ratio.get());
    for (int ibin = 0; ibin < error_ratio.GetN(); ++ibin) {
        double x,y;
        error_ratio.GetPoint(ibin, x, y);
        error_ratio.SetPoint(ibin, x, 1.);
        const double err_up   = (post.GetBinContent(ibin+1) + post.GetBinError(ibin+1))/post.GetBinContent(ibin+1) - 1;
        const double err_down = (post.GetBinContent(ibin+1) - post.GetBinError(ibin+1))/post.GetBinContent(ibin+1) - 1;
        error_ratio.SetPointEYhigh(ibin, err_up);
        error_ratio.SetPointEYlow(ibin, -err_down);
    }
    error_ratio.SetFillStyle(3002);
    error_ratio.SetFillColor(kGray+2);
    error_ratio.SetMarkerStyle(0);
    error_ratio.SetLineWidth(2);

    data_ratio->Divide(ratio.get());
    data_ratio->SetMarkerStyle(8);
    data_ratio->SetMarkerSize(1);
    data_ratio->GetXaxis()->SetLabelFont(42);
    data_ratio->GetXaxis()->SetLabelSize(0.11);
    data_ratio->GetXaxis()->SetLabelOffset(0.02);
    data_ratio->GetXaxis()->SetTitleFont(42);
    data_ratio->GetXaxis()->SetTitleSize(0.20);
    data_ratio->GetXaxis()->SetTitleOffset(0.9);
    data_ratio->GetXaxis()->SetNdivisions(505);

    data_ratio->GetYaxis()->SetRangeUser(m_is_jes_fit ? 0.99 : 0.9, m_is_jes_fit ? 1.01 : 1.1);
    data_ratio->GetYaxis()->SetLabelFont(42);
    data_ratio->GetYaxis()->SetLabelSize(0.15);
    data_ratio->GetYaxis()->SetLabelOffset(0.04);
    data_ratio->GetYaxis()->SetTitleFont(42);
    data_ratio->GetYaxis()->SetTitleSize(0.15);
    data_ratio->GetYaxis()->SetTitleOffset(0.5);
    data_ratio->GetYaxis()->SetNdivisions(503);
    data_ratio->GetYaxis()->SetTitle("data/pred.");

    data_ratio->Draw("EPX0");
    error_ratio.Draw("E2 SAME");

    for (std::size_t i = 0; i < m_single_regions.size(); ++i) {
        const std::string name = Common::RegionToRangeTxT(m_single_regions.at(i).first);
        data_ratio->GetXaxis()->SetBinLabel(i+1, name.c_str());
    }

    for (std::size_t i = 0; i < m_cross_regions.size(); ++i) {
        const std::string name = Common::RegionToRangeTxT(m_cross_regions.at(i).first);
        data_ratio->GetXaxis()->SetBinLabel(i+1 + bins_single, name.c_str());
    }
    //data_ratio->GetXaxis()->LabelsOption("v");

    TLine line(0.5, 1, bins_total + 0.5, 1);
    line.SetLineColor(kRed);
    line.SetLineWidth(2);
    line.SetLineStyle(2);
    line.Draw("same");

    pad2.Modified();
    pad2.RedrawAxis();

    const std::string print_name = m_outputFolder + "/PrePost_" + (m_is_jes_fit ? "JES" : "JER") + ".png";
    c.Print(print_name.c_str());
}
