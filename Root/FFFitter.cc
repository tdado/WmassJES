#include "WmassJES/FFFitter.h"
#include "WmassJES/Common.h"

#include "TCanvas.h"
#include "TGraph2DErrors.h"
#include "TH1.h"
#include "TH2D.h"
#include "TLegend.h"
#include "TLine.h"
#include "TMatrixDSym.h"
#include "TSystem.h"

#include "RooAbsReal.h"
#include "RooArgSet.h"
#include "RooFitResult.h"
#include "RooFormulaVar.h"
#include "RooGaussian.h"
#include "RooPoisson.h"
#include "RooHistPdf.h"
#include "RooMinimizer.h"
#include "RooMomentMorphFuncND.h"
#include "RooMultiVarGaussian.h"
#include "RooProdPdf.h"
#include "RooRealVar.h"
#include "RooWrapperPdf.h"

#include "AtlasUtils/AtlasLabels.h"
#include "AtlasUtils/AtlasStyle.h"
#include "AtlasUtils/AtlasUtils.h"

#include <iostream>
#include <fstream>
#include <sstream>

FFFitter::FFFitter() :
    m_param_r(nullptr),
    m_param_s(nullptr),
    m_seed(0),
    m_fit_is_okay(true),
    m_debug(false) {

    RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR);
    RooMsgService::instance().getStream(1).removeTopic(RooFit::ObjectHandling);
}

void FFFitter::SetParameterR(const int steps,
                             const double min,
                             const double max) {

    if (steps <= 1) {
        std::cerr << "FFFitter::SetParameterR: ERROR Number of steps <= 1!" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (min >= max) {
        std::cerr << "FFFitter::SetParameterR: ERROR Min >= max!" << std::endl;
        exit(EXIT_FAILURE);
    }

    m_param_r = std::make_unique<FoldingParameter>(steps, min, max);
}

void FFFitter::SetParameterS(const int steps,
                             const double min,
                             const double max) {

    if (steps <= 1) {
        std::cerr << "FFFitter::SetParameterS: ERROR Number of steps <= 1!" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (min >= max) {
        std::cerr << "FFFitter::SetParameterS: ERROR Min >= max!" << std::endl;
        exit(EXIT_FAILURE);
    }
    m_param_s = std::make_unique<FoldingParameter>(steps, min, max);
}

double FFFitter::IndexToValue(const int index, const FoldingParameter::PARAMETER& par) const {
    double result(9999);

    if (par == FoldingParameter::PARAMETER::S) {
        result = m_param_s->GetMin() + index*m_param_s->GetDelta();
    } else if (par == FoldingParameter::PARAMETER::R) {
        result = m_param_r->GetMin() + index*m_param_r->GetDelta();
    } else {
        std::cerr << "FFFitter::IndexToValue: ERROR Unknown parameter, returning -1" << std::endl;
        return -1;
    }

    return result;
}

void FFFitter::Poissonise(TH1D* h, TRandom3* rnd) const {
    const int nbins = h->GetNbinsX();

    for (int ibin = 1; ibin <= nbins; ++ibin) {
        const double poissonised = rnd->Poisson(h->GetBinContent(ibin));
        h->SetBinContent(ibin, poissonised);
        h->SetBinError(ibin, std::sqrt(poissonised));
    }
}

void FFFitter::Gaussinise(TH1D* h, TRandom3* rand) const {

    for (int ibin = 1; ibin <= h->GetNbinsX(); ++ibin) {
        const double content = h->GetBinContent(ibin);
        const double error = h->GetBinError(ibin);
        const double smeared = rand->Gaus(content, error);
        h->SetBinContent(ibin, smeared);
        h->SetBinError(ibin, 0);
    }
}

void FFFitter::SetSingleRegionTemplates(const std::vector<std::pair<std::vector<TH1D>, std::string> >& templates,
                                        const std::vector<std::string>& names) {
    m_single_templates.clear();
    for (const auto& itemp : templates) {
        std::vector<TH1D> hist_tmp;
        for (const auto& ihist : itemp.first) {
            hist_tmp.emplace_back(*static_cast<TH1D*>(ihist.Clone()));
        }
        std::pair<std::vector<TH1D>, std::string> tmp = std::make_pair(std::move(hist_tmp), itemp.second);
        m_single_templates.emplace_back(std::move(tmp));
    }

    m_single_region_names = names;
}

void FFFitter::SetCrossRegionTemplates(const std::vector<std::pair<std::vector<std::vector<TH1D> >, std::pair<std::string, std::string> > >& templates,
                                       const std::vector<std::string>& names) {
    m_cross_templates.clear();
    for (const auto& itemp : templates) {
        std::vector<std::vector<TH1D> > hist_tmp;
        for (const auto& i : itemp.first) {
            std::vector<TH1D> hist_tmp2;
            for (const auto& ihist : i) {
                hist_tmp2.emplace_back(*static_cast<TH1D*>(ihist.Clone()));
            }
            hist_tmp.emplace_back(std::move(hist_tmp2));
        }
        std::pair<std::vector<std::vector<TH1D> >, std::pair<std::string, std::string> > tmp = std::make_pair(std::move(hist_tmp), itemp.second);
        m_cross_templates.emplace_back(std::move(tmp));
    }

    m_cross_region_names = names;
}

void FFFitter::PrepareCombinedFitParametrisation() {
    std::cout << "FFFitter::PrepareCombinedFitParametrisation: Preparing bin parametrisation\n";
    this->PrepareCombinedFitParametrisationSingleRegion();
    this->PrepareCombinedFitParametrisationCrossRegion();
    std::cout << "FFFitter::PrepareCombinedFitParametrisation: Done preparing bin parametrisation\n";
}

void FFFitter::PrepareCombinedFitParametrisationSingleRegion() {
    m_combined_formula_single_region.clear();
    gSystem->mkdir((m_output_folder+"/ParametrisationCombined_" + (m_is_jes_fit ? "JES" : "JER")+"/").c_str());
    for (std::size_t i = 0; i < m_single_templates.size(); ++i) {
        this->ProcessCombinedFitSingleRegion(i);
    }
}

void FFFitter::PrepareCombinedFitParametrisationCrossRegion() {
    m_combined_formula_cross_region.clear();
    for (std::size_t i = 0; i < m_cross_templates.size(); ++i) {
        this->ProcessCombinedFitCrossRegion(i);
    }
}

void FFFitter::ProcessCombinedFitSingleRegion(const std::size_t index) {

    TGraphErrors g{};

    const std::string region = m_single_region_names.at(index);
    const std::string param = m_single_templates.at(index).second;
    for (std::size_t ihist = 0; ihist < m_single_templates.at(index).first.size(); ++ihist) {
        const double value = m_is_jes_fit ? m_single_templates.at(index).first.at(ihist).GetMean() : m_single_templates.at(index).first.at(ihist).GetRMS();
        const double error = m_is_jes_fit ? m_single_templates.at(index).first.at(ihist).GetMeanError() : m_single_templates.at(index).first.at(ihist).GetRMSError();
        const double x = m_is_jes_fit ? (m_param_s->GetMin() + ihist * m_param_s->GetDelta()) : (m_param_r->GetMin() + ihist * m_param_r->GetDelta());
        g.SetPoint(ihist, x, value);
        g.SetPointError(ihist, 0., error);
    }

    const double min = m_is_jes_fit ? m_param_s->GetMin() : m_param_r->GetMin();
    const double max = m_is_jes_fit ? m_param_s->GetMax() : m_param_r->GetMax();

    //const std::string s = "[0]*x*x + [1]*x + [2]";
    const std::string s = "[0]*x + [1]";
    TF1 func("func", s.c_str(), min, max);
    func.SetLineColor(kRed);
    g.Fit(&func, "RQ");

    TCanvas c("","",800,600);
    TPad pad1("","",0.0, 0.3, 1.0, 1.00);
    TPad pad2("","", 0.0, 0.010, 1.0, 0.3);
    pad1.SetBottomMargin(0.001);
    pad1.SetBorderMode(0);
    pad2.SetBottomMargin(0.5);
    pad1.SetTicks(1,1);
    pad2.SetTicks(1,1);
    pad1.Draw();
    pad2.Draw();

    pad1.cd();
    g.GetXaxis()->SetTitle(m_is_jes_fit ? "S (JES) parameter" : "R (JER) parameter");
    g.GetXaxis()->SetTitleOffset(1.3*func.GetXaxis()->GetTitleOffset());
    g.GetYaxis()->SetTitle(m_is_jes_fit ? "mean" : "RMS");
    g.GetYaxis()->SetTitleOffset(1.4*func.GetYaxis()->GetTitleOffset());
    g.Draw("APE");
    func.Draw("same");

    ATLASLabel(0.2,0.9, "Internal");
    myText(0.2,0.85, 1, ("Region: " + region).c_str());

    TLegend leg(0.7,0.7, 0.9, 0.9);
    leg.SetFillStyle(0);
    leg.SetBorderSize(0);
    leg.AddEntry(&g, m_fit_is_okay ? "Template mean" : "Template RMS", "lf");
    leg.AddEntry(&func, "Fit function", "lf");
    leg.Draw("same");

    pad2.cd();
    TGraphErrors ratio;
    for (std::size_t ihist = 0; ihist < m_single_templates.at(index).first.size(); ++ihist) {
        double value = m_is_jes_fit ? m_single_templates.at(index).first.at(ihist).GetMean() : m_single_templates.at(index).first.at(ihist).GetRMS();
        double error = m_is_jes_fit ? m_single_templates.at(index).first.at(ihist).GetMeanError() : m_single_templates.at(index).first.at(ihist).GetRMSError();
        const double x = m_is_jes_fit ? (m_param_s->GetMin() + ihist * m_param_s->GetDelta()) : (m_param_r->GetMin() + ihist * m_param_r->GetDelta());

        //std::cout << "ihis: " << ihist << ", mean: " << m_single_templates.at(index).first.at(ihist).GetMean() << "\n";

        //m_single_templates.at(index).first.at(ihist).ResetStats();
        //std::cout << "new mean: " << m_single_templates.at(index).first.at(ihist).GetMean() << "\n";

        const double func_value = func.Eval(x);
        value /= func_value;
        error /= func_value;

        ratio.SetPoint(ihist, x, value);
        ratio.SetPointError(ihist, 0., error);
    }

    ratio.GetYaxis()->SetNdivisions(505);
    ratio.Draw("ape");

    TLine line(min, 1., max, 1.);
    line.SetLineColor(kGreen+2);
    line.SetLineWidth(3);
    line.SetLineStyle(2);
    line.Draw("same");

    m_single_region_functions.emplace_back(std::move(func));

    const std::string name = m_output_folder + "/ParametrisationCombined_"+(m_is_jes_fit ? "JES" : "JER")+"/Region_"+region+"_parametrisation.png";
    c.Print(name.c_str());

    const std::string formula = this->TranslateFormulaCombined1D(func, param);
    m_combined_formula_single_region.emplace_back(std::make_pair(region, formula));
}

std::string FFFitter::TranslateFormulaCombined1D(const TF1& func, const std::string& param) const {
    std::stringstream ss;
    ss << std::scientific;

    //ss << func.GetParameter(0) << "*" << param << "*" << param << " + " << func.GetParameter(1) << "*" << param << " + " << func.GetParameter(2);
    ss << func.GetParameter(0) << "*" << param << " + " << func.GetParameter(1);

    return ss.str();
}

void FFFitter::ProcessCombinedFitCrossRegion(const std::size_t index) {
    TGraph2DErrors g{};

    const std::string region = m_cross_region_names.at(index);
    const std::string param1 = m_cross_templates.at(index).second.first;
    const std::string param2 = m_cross_templates.at(index).second.second;
    for (std::size_t ihist = 0; ihist < m_cross_templates.at(index).first.size(); ++ihist) {
        for (std::size_t jhist = 0; jhist < m_cross_templates.at(index).first.at(ihist).size(); ++jhist) {
            const double value = m_is_jes_fit ? m_cross_templates.at(index).first.at(ihist).at(jhist).GetMean() : m_cross_templates.at(index).first.at(ihist).at(jhist).GetRMS();
            const double error = m_is_jes_fit ? m_cross_templates.at(index).first.at(ihist).at(jhist).GetMeanError() : m_cross_templates.at(index).first.at(ihist).at(jhist).GetRMSError();
            const double x = m_is_jes_fit ? (m_param_s->GetMin() + ihist * m_param_s->GetDelta()) : (m_param_r->GetMin() + ihist * m_param_r->GetDelta());
            const double y = m_is_jes_fit ? (m_param_s->GetMin() + jhist * m_param_s->GetDelta()) : (m_param_r->GetMin() + jhist * m_param_r->GetDelta());
            g.AddPointError(x, y, value, 0., 0., error);
        }
    }

    const double min = m_is_jes_fit ? m_param_s->GetMin() : m_param_r->GetMin();
    const double max = m_is_jes_fit ? m_param_s->GetMax() : m_param_r->GetMax();

    //const std::string s = "[0]*x*x*x*x + [1]*x*x*x*y + [2]*x*x*y*y + [3]*x*y*y*y + [4]*y*y*y*y + [5]*x*x*x + [6]*x*x*y + [7]*x*y*y + [8]*y*y*y + [9]*x*x + [10]*x*y + [11]*y*y + [12]*x + [13]*y + [14]";
    const std::string s = "[0]*x*x + [1]*x*y + [2]*y*y + [3]*x + [4]*y + [5]";
    //const std::string s = "[0]*x + [1]*y + [2]";
    TF2 func("func", s.c_str(), min, max, min, max);
    func.SetLineColor(kRed);
    g.Fit(&func, "RQ");

    TCanvas c("","",800,600);
    func.GetXaxis()->SetTitle(m_is_jes_fit ? "S (JES) parameter low" : "R (JER) parameter low");
    func.GetXaxis()->SetTitleOffset(1.3*func.GetXaxis()->GetTitleOffset());
    func.GetYaxis()->SetTitle(m_is_jes_fit ? "S (JES) parameter high" : "R (JER) parameter high");
    func.GetYaxis()->SetTitleOffset(1.4*func.GetYaxis()->GetTitleOffset());
    func.GetZaxis()->SetTitle(m_is_jes_fit ? "mean" : "RMS");
    func.GetZaxis()->SetTitleOffset(2.2*func.GetZaxis()->GetTitleOffset());
    func.Draw("surf1");
    g.Draw("surf2 same");

    ATLASLabel(0.1,0.9, "Internal");
    myText(0.70,0.9, 1, ("Region: " + region).c_str());

    TLegend leg(0.01,0.01, 0.3, 0.15);
    leg.SetFillStyle(0);
    leg.SetBorderSize(0);
    leg.AddEntry(&g, m_is_jes_fit ? "Template mean" : "Template RMS", "lf");
    leg.AddEntry(&func, "Fit function", "lf");
    leg.Draw("same");

    const std::string name = m_output_folder + "/ParametrisationCombined_"+(m_is_jes_fit ? "JES" : "JER")+"/Region_"+region+"_parametrisation.png";
    c.Print(name.c_str());

    m_cross_region_functions.emplace_back(std::move(func));
    const std::string formula = this->TranslateFormulaCombined2D(func, m_cross_templates.at(index).second);
    m_combined_formula_cross_region.emplace_back(std::make_pair(region, formula));
}

std::string FFFitter::TranslateFormulaCombined2D(const TF2& func, const std::pair<std::string, std::string>& param) const {
    std::stringstream ss;
    ss << std::scientific;

    //ss << func.GetParameter(0) << "*" << param.first << "*" << param.first << "*" << param.first << "*" << param.first << " + ";
    //ss << func.GetParameter(1) << "*" << param.first << "*" << param.first << "*" << param.first << "*" << param.second << " + ";
    //ss << func.GetParameter(2) << "*" << param.first << "*" << param.first << "*" << param.second << "*" << param.second << " + ";
    //ss << func.GetParameter(3) << "*" << param.first << "*" << param.second << "*" << param.second << "*" << param.second << " + ";
    //ss << func.GetParameter(4) << "*" << param.second << "*" << param.second << "*" << param.second << "*" << param.second << " + ";
    //ss << func.GetParameter(5) << "*" << param.first << "*" << param.first << "*" << param.first << " + ";
    //ss << func.GetParameter(6) << "*" << param.first << "*" << param.first << "*" << param.second << " + ";
    //ss << func.GetParameter(7) << "*" << param.first << "*" << param.second << "*" << param.second << " + ";
    //ss << func.GetParameter(8) << "*" << param.second << "*" << param.second << "*" << param.second << " + ";
    //ss << func.GetParameter(9) << "*" << param.first << "*" << param.first << " + ";
    //ss << func.GetParameter(10) << "*" << param.first << "*" << param.second << " + ";
    //ss << func.GetParameter(11) << "*" << param.second << "*" << param.second << " + ";
    //ss << func.GetParameter(12) << "*" << param.first << " + ";
    //ss << func.GetParameter(13) << "*" << param.second << " + ";
    //ss << func.GetParameter(14);

    ss << func.GetParameter(0) << "*" << param.first << "*" << param.first << " + ";
    ss << func.GetParameter(1) << "*" << param.first << "*" << param.second << " + ";
    ss << func.GetParameter(2) << "*" << param.second << "*" << param.second << " + ";
    ss << func.GetParameter(3) << "*" << param.first << " + ";
    ss << func.GetParameter(4) << "*" << param.second << " + ";
    ss << func.GetParameter(5);

    //ss << func.GetParameter(0) << "*" << param.first << " + ";
    //ss << func.GetParameter(1) << "*" << param.second << " + ";
    //ss << func.GetParameter(2);

    return ss.str();
}

void FFFitter::PrepareWSCombined() {

    m_pruned_params.clear();

    m_data_args.clear();

    const bool run_profile = m_profile && !m_impact_map.empty();

    m_ws_combined = std::make_unique<RooWorkspace>("workspace");
    // params
    RooArgList params;
    for (std::size_t i = m_start_index; i < 7; i++) {
        std::string name = m_is_jes_fit ? "JES" : "JER";
        name += std::to_string(i);
        const double min = m_is_jes_fit ? 0.8*m_param_s->GetMin() : 0.8*m_param_r->GetMin();
        const double max = m_is_jes_fit ? 1.2*m_param_s->GetMax() : 1.2*m_param_r->GetMax();
        RooRealVar par(name.c_str(), name.c_str(), 1, min, max);
        params.addClone(par);
    }

    if (run_profile) {
        for (const auto& isyst : m_impact_map) {
            const std::string name = "alpha_" + isyst.first;
            RooRealVar par(name.c_str(), name.c_str(), 0, -5, 5);
            params.addClone(par);
        }
    }

    m_ws_combined->import(params);

    std::map<std::string, std::size_t> prune_map;

    for (std::size_t i = 0; i < m_combined_formula_single_region.size(); ++i) {
        auto ireg = m_combined_formula_single_region.at(i);
        const std::string name_data = "data_mean_region_" + ireg.first;
        RooRealVar var(name_data.c_str(), name_data.c_str(), 0, 200);
        const std::string name_formula = "formula_region_" + ireg.first;
        RooArgList tmp;
        const std::string param = this->GetSingleParamFromRegion(ireg.first);
        auto* p = params.find(param.c_str());
        if (!p) {
            std::cerr << "FFFitter::PrepareWSCombined: Cannot find parameter: " << param << "\n";
            exit(1);
        }
        tmp.add(*p);
        std::string formula_string = ireg.second.c_str();
        if (m_profile) {
            for (const auto& isyst : m_impact_map) {
                const std::string name = "alpha_" + isyst.first;
                if (std::abs(isyst.second.first.at(i)) < m_nominal_single_region.at(i).GetMeanError()) {
                    auto itr = prune_map.find(isyst.first);
                    if (itr == prune_map.end()) {
                        prune_map.insert({isyst.first, 1});
                    } else {
                        ++(itr->second);
                    }
                    continue;
                }
                auto* alpha = params.find(name.c_str());
                if (!alpha) {
                    std::cerr << "FFFitter::PrepareWSCombined: Cannot find parameter: " << name << "\n";
                    exit(1);
                }
                tmp.add(*alpha);
                formula_string += "+(" + std::to_string(isyst.second.first.at(i)) + "*" + name + ")";
            }
        }
        RooFormulaVar formula(name_formula.c_str(), name_formula.c_str(), formula_string.c_str(), tmp);
        m_data_args.addClone(var);

        const std::string name_sigma = "data_sigma_region_" + ireg.first;
        RooRealVar sigma(name_sigma.c_str(), name_sigma.c_str(), 0.001, 100);
        const std::string name_gauss = "gauss_region_" + ireg.first;
        RooGaussian gauss(name_gauss.c_str(), name_gauss.c_str(), var, formula, sigma);
        m_data_args.addClone(sigma);

        m_ws_combined->import(gauss);
    }
    if (!m_is_1D_fit) {
        for (std::size_t i = 0; i < m_combined_formula_cross_region.size(); ++i) {
            auto ireg = m_combined_formula_cross_region.at(i);
            const std::string name_data = "data_mean_region_" + ireg.first;
            RooRealVar var(name_data.c_str(), name_data.c_str(), 0, 200);
            m_data_args.addClone(var);

            const std::string name_formula = "formula_region_" + ireg.first;
            RooArgList tmp;
            const std::pair<std::string, std::string> pars = this->GetCrossParamFromRegion(ireg.first);
            tmp.add(*params.find(pars.first.c_str()));
            tmp.add(*params.find(pars.second.c_str()));
            std::string formula_string = ireg.second.c_str();
            if (m_profile) {
                for (const auto& isyst : m_impact_map) {
                    const std::string name = "alpha_" + isyst.first;
                    if (std::abs(isyst.second.second.at(i)) < m_nominal_cross_region.at(i).GetMeanError()) {
                        auto itr = prune_map.find(isyst.first);
                        if (itr == prune_map.end()) {
                            prune_map.insert({isyst.first, 1});
                        } else {
                            ++(itr->second);
                        }
                        continue;
                    }
                    auto* alpha = params.find(name.c_str());
                    if (!alpha) {
                        std::cerr << "FFFitter::PrepareWSCombined: Cannot find parameter: " << name << "\n";
                        exit(1);
                    }
                    tmp.add(*alpha);
                    formula_string += "+(" + std::to_string(isyst.second.second.at(i)) + "*" + name + ")";
                }
            }
            RooFormulaVar formula(name_formula.c_str(), name_formula.c_str(), formula_string.c_str(), tmp);

            const std::string name_sigma = "data_sigma_region_" + ireg.first;
            RooRealVar sigma (name_sigma.c_str(), name_sigma.c_str(), 0.001, 100);
            const std::string name_gauss = "gauss_region_" + ireg.first;
            RooGaussian gauss(name_gauss.c_str(), name_gauss.c_str(), var, formula, sigma);
            m_data_args.addClone(sigma);

            m_ws_combined->import(gauss, RooFit::RecycleConflictNodes());
        }
    }

    //multiply the gaussians
    RooArgSet set;
    for (const auto& ireg : m_combined_formula_single_region) {
        const std::string name_gauss = "gauss_region_" + ireg.first;
        auto* gauss = m_ws_combined->pdf(name_gauss.c_str());
        if (!gauss) {
            std::cerr << "cannot read PDF: " << name_gauss << "\n";
            exit(1);
        }

        set.add(*gauss);
    }

    if (!m_is_1D_fit) {
        for (const auto& ireg : m_combined_formula_cross_region) {
            const std::string name_gauss = "gauss_region_" + ireg.first;
            auto* gauss = m_ws_combined->pdf(name_gauss.c_str());
            if (!gauss) {
                std::cerr << "cannot read PDF: " << name_gauss << "\n";
                exit(1);
            }

            set.add(*gauss);
        }
    }

    if (run_profile) {
        for (const auto& isyst : m_impact_map) {
            const std::string name_param = "alpha_" + isyst.first;
            auto itr = prune_map.find(isyst.first);
            if (itr != prune_map.end()) {
                if (itr->second == (m_nominal_single_region.size() + m_nominal_cross_region.size())) {
                    continue;
                }
            }
            const std::string name = "aux_" + isyst.first;
            const std::string data_name = "data_" + name;
            RooRealVar data(data_name.c_str(), data_name.c_str(), 0, -5, 5);
            m_data_args.addClone(data);
            const std::string formula_name = "formula_" + name;
            auto* tmp_par = params.find(name_param.c_str());
            RooFormulaVar formula(formula_name.c_str(), formula_name.c_str(), name_param.c_str(), *tmp_par);
            RooGaussian constraint(name.c_str(), name.c_str(), data, formula, 1);
            m_ws_combined->import(constraint);

            auto* tmp = m_ws_combined->pdf(name.c_str());
            set.add(*tmp);
        }
    }

    RooProdPdf prod("combined_model", "combined_model", set);
    m_ws_combined->import(prod);

    m_ws_combined->Print();

    for (const auto& iprune : prune_map) {
        if (iprune.second == (m_nominal_single_region.size() + m_nominal_cross_region.size())) {
            m_pruned_params.emplace_back(iprune.first);
        }
    }
}

std::map<std::string, double> FFFitter::FitCombinedModel(const std::vector<TH1D>& single_region_histograms,
                                                         const std::vector<TH1D>& cross_region_histograms,
                                                         const bool write,
                                                         const int print_level) {
    std::map<std::string, double> result;

    if (single_region_histograms.size() != m_single_region_names.size()) {
        std::cerr << "Incompatible sizes of histograms - single\n";
        return result;
    }
    if (cross_region_histograms.size() != m_cross_region_names.size()) {
        std::cerr << "Incompatible sizes of histograms - cross\n";
        return result;
    }

    if (!m_ws_combined) {
        std::cerr << "WS is nullptr\n";
        return result;
    }

    m_fit_is_okay = true;

    RooAbsPdf* model = m_ws_combined->pdf("combined_model");

    std::vector<double> data_values;

    RooDataSet data("data", "data", m_data_args);
    for (std::size_t ireg = 0; ireg < m_combined_formula_single_region.size(); ++ireg) {
        const std::string name_data = "data_mean_region_" + m_combined_formula_single_region.at(ireg).first;
        const std::string name_sigma = "data_sigma_region_" + m_combined_formula_single_region.at(ireg).first;

        auto value = m_data_args.find(name_data.c_str());
        auto sigma = m_data_args.find(name_sigma.c_str());
        if (!value || !sigma) {
            std::cerr << "Cannot find the right observables\n";
            std::cerr << "mean: " << name_data << ", sigma: " << name_sigma << "\n";
            return result;
        }

        const double value_float = m_is_jes_fit ? single_region_histograms.at(ireg).GetMean() : single_region_histograms.at(ireg).GetRMS();
        const double sigma_float = m_is_jes_fit ? Common::GetUncertaintyOnMean(single_region_histograms.at(ireg)) : Common::GetUncertaintyOnRMS(single_region_histograms.at(ireg));
        *static_cast<RooRealVar*>(value) = value_float;
        *static_cast<RooRealVar*>(sigma) = sigma_float;

        data_values.emplace_back(value_float);
        //std::cout << "single mean: " << value_float << ", sigma: " << sigma_float << "\n";
    }

    std::vector<double> data_values_cross;
    if (!m_is_1D_fit) {
        for (std::size_t ireg = 0; ireg < m_combined_formula_cross_region.size(); ++ireg) {
            const std::string name_data = "data_mean_region_" + m_combined_formula_cross_region.at(ireg).first;
            const std::string name_sigma = "data_sigma_region_" + m_combined_formula_cross_region.at(ireg).first;

            auto value = m_data_args.find(name_data.c_str());
            auto sigma = m_data_args.find(name_sigma.c_str());
            if (!value || !sigma) {
                std::cerr << "Cannot find the right observables\n";
                std::cerr << "mean: " << name_data << ", sigma: " << name_sigma << "\n";
                return result;
            }

            const double value_float = m_is_jes_fit ? cross_region_histograms.at(ireg).GetMean() : cross_region_histograms.at(ireg).GetRMS();
            const double sigma_float = m_is_jes_fit ? Common::GetUncertaintyOnMean(cross_region_histograms.at(ireg)) : Common::GetUncertaintyOnRMS(cross_region_histograms.at(ireg));
            *static_cast<RooRealVar*>(value) = value_float;
            *static_cast<RooRealVar*>(sigma) = sigma_float;
            data_values_cross.emplace_back(value_float);
            //std::cout << "cross mean: " << value_float << ", sigma: " << sigma_float << "\n";
        }
    }

    if (m_profile) {
        for (const auto& isyst : m_impact_map) {
            const std::string name = "data_aux_" + isyst.first;
            auto itr = std::find(m_pruned_params.begin(), m_pruned_params.end(), isyst.first);
            if (itr != m_pruned_params.end()) {
                continue;
            }
            auto* data_aux = m_data_args.find(name.c_str());
            if (!data_aux) {
                std::cerr << "Cannot find parameter: " << name << "\n";
                return result;
            }

            *static_cast<RooRealVar*>(data_aux) = 0;
        }
    }

    data.add(m_data_args);

    if (write) {
        m_ws_combined->import(data);
        m_ws_combined->writeToFile((m_output_folder+"/Workspace_combined_ " + (m_is_jes_fit ? "JES" : "JER") + ".root").c_str());
    }

    std::vector<std::pair<std::string, double> > params_starting_values = this->GetStartingValuesCombined(data_values, data_values_cross);

    this->SetStartingParameter(model, &data, params_starting_values);

    std::unique_ptr<RooAbsReal> nll = std::unique_ptr<RooAbsReal>(model->createNLL(data, RooFit::Offset(kTRUE), RooFit::Optimize(kTRUE)));
    ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit2");
    const TString minimType = ::ROOT::Math::MinimizerOptions::DefaultMinimizerType().c_str();
    const TString algorithm = ::ROOT::Math::MinimizerOptions::DefaultMinimizerAlgo().c_str();
    const double tol =        ::ROOT::Math::MinimizerOptions::DefaultTolerance();

    RooMinimizer minimizer(*nll);
    minimizer.optimizeConst(2);
    minimizer.setMinimizerType(minimType.Data());
    minimizer.setPrintLevel(print_level);
    minimizer.setEps(tol);
    minimizer.setStrategy(3);
    int status = minimizer.minimize(minimType.Data(),algorithm.Data());
    if (status != 0) {
        std::cerr << "FFFitter::FitCombinedModel: The fit failed!\n";
        m_fit_is_okay = false;
        return result;
    }

    minimizer.hesse();
    minimizer.minos();

    m_fit_result_combined.reset(minimizer.save());

    this->SetCombinedFitResults();

    for (const auto& i : m_combined_fit_results) {
        result.insert({i.first, i.second.central});
    }

    return result;
}

void FFFitter::SetCombinedFitResults() {
    if (!m_fit_result_combined) {
        std::cerr << "No fit results available\n";
    }

    m_combined_fit_results.clear();

    for (auto* ipar : m_fit_result_combined->floatParsFinal()) {
        RooRealVar* var = static_cast<RooRealVar*>(ipar);
        FFFitter::FitParams param;
        param.central = var->getVal();
        param.error = var->getError();
        param.positive_error = var->getErrorHi();
        param.negative_error = var->getErrorLo();

        m_combined_fit_results.insert({var->GetName(), param});
    }
}

FFFitter::FitParams FFFitter::GetCombinedFitResultSingleParam(const std::string& param) const {
    auto itr = m_combined_fit_results.find(param);
    if (itr == m_combined_fit_results.end()) {
        std::cerr << "Cannot find parameter: " << param << " in the fit results";

        exit(1);
    }

    return itr->second;
}

double FFFitter::GetCombinedFitCentralSingleParam(const std::string& param) const {
    auto itr = m_combined_fit_central.find(param);
    if (itr == m_combined_fit_central.end()) {
        std::cerr << "Cannot find parameter: " << param << " in the fit results";

        exit(1);
    }

    return itr->second;
}

std::string FFFitter::GetSingleParamFromRegion(const std::string& region) const {
    auto itr = std::find_if(m_single_regions.begin(), m_single_regions.end(), [&region](const auto& element){return region == element.first;});

    if (itr == m_single_regions.end()) {
        std::cerr << "FFFitter::GetSingleParamFromRegion: Cannot find region: " << region << "\n";
        exit(1);
    }

    return itr->second;
}

std::pair<std::string,std::string> FFFitter::GetCrossParamFromRegion(const std::string& region) const {
    auto itr = std::find_if(m_cross_regions.begin(), m_cross_regions.end(), [&region](const auto& element){return region == element.first;});

    if (itr == m_cross_regions.end()) {
        std::cerr << "FFFitter::GetCrossParamFromRegion: Cannot find region: " << region << "\n";
        exit(1);
    }

    return itr->second;
}

std::vector<std::pair<std::string, double> > FFFitter::GetStartingValuesCombined(const std::vector<double>& data_values,
                                                                                 const std::vector<double>& data_values_cross) const {

    std::vector<std::pair<std::string, double> > result;

    if (m_single_regions.size() != m_single_region_functions.size()) {
        std::cerr << "FFFitter::GetStartingValuesCombined: Size of the single regions and functions do not match\n";
        exit(1);
    }

    if (m_single_regions.size() != data_values.size()) {
        std::cerr << "FFFitter::GetStartingValuesCombined: Size of the single regions and data values do not match\n";
        exit(1);
    }

    for (std::size_t i = 0; i < m_single_regions.size(); ++i) {
        const std::string param = m_single_regions.at(i).second;
        const TF1& func = m_single_region_functions.at(i);
        //const double value = this->SolveStartingPoint(func, data_values.at(i));
        const double value = (data_values.at(i) - func.GetParameter(1)) / func.GetParameter(0);

        result.emplace_back(std::make_pair(param, value));
    }

    if (!m_is_1D_fit) {
        // need to work out starting points for JES/JER1
        const std::string param = m_is_jes_fit ? "JES1" : "JER1";
        const double start = this->SolveStartingPointFirst(param, result, data_values_cross);

        result.emplace_back(std::make_pair(param, start));
    }

    return result;
}

double FFFitter::SolveStartingPoint(const TF1& func, const double value) const {
    const double a = func.GetParameter(0);
    const double b = func.GetParameter(1);
    const double c = func.GetParameter(2) - value;

    const double d = b*b - 4.*a*c;

    double x1 = -b + std::sqrt(d);
    double x2 = -b - std::sqrt(d);

    x1 /= 2.*a;
    x2 /= 2.*a;

    const double min = m_is_jes_fit ? m_param_s->GetMin() : m_param_r->GetMin();
    const double max = m_is_jes_fit ? m_param_s->GetMax() : m_param_r->GetMax();

    if ((x1 > min) && (x1 < max)) {
        return x1;
    } else if ((x2 > min) && (x2 < max)) {
        return x2;
    }

    std::cerr << "FFFitter::SolveStartingPoint: Cannot solve equation!\n";
    std::cerr << "Value: " << value << "\n";
    return 1;
}

double FFFitter::SolveStartingPointFirst(const std::string& param,
                                         const std::vector<std::pair<std::string, double> >& starting_params,
                                         const std::vector<double>& data_values_cross) const {
    const int steps = 10;
    const double min = m_is_jes_fit ? m_param_s->GetMin() : m_param_r->GetMin();
    const double max = m_is_jes_fit ? m_param_s->GetMax() : m_param_r->GetMax();
    const double delta = (max - min)/steps;

    double best_diff(99999999);
    double best_value(0);
    for (int istep = 0; istep <= steps; ++istep) {
        double diff(0);
        for (std::size_t ireg = 0; ireg < m_cross_regions.size(); ++ireg) {
            if (m_cross_regions.at(ireg).second.first != param) continue;

            const std::string region2 = m_cross_regions.at(ireg).second.second;
            auto itr = std::find_if(starting_params.begin(), starting_params.end(), [&region2](const auto& element){return element.first == region2;});
            if (itr == starting_params.end()) {
                std::cout << "FFFitter::SolveStartingPointFirst: Cannot find parameter: " << region2 << "\n";
                return 1;
            }

            const double start2 = itr->second;
            const double x = min + istep*delta;
            const double value = m_cross_region_functions.at(ireg).Eval(x, start2) - data_values_cross.at(ireg);

            diff += value*value;
        }

        if (diff < best_diff) {
            best_diff = diff;
            best_value = min + istep*delta;
        }
    }

    return best_value;
}

std::map<std::string, std::vector<double> > FFFitter::RunCombinedPseudoExperiments(const std::vector<TH1D>& single_region_hists,
                                                                                   const std::vector<TH1D>& cross_region_hists,
                                                                                   const int n) {


    TRandom3 rand(m_seed);

    if (n < 0) {
        std::cerr << "FFFitter::RunCombinedPseudoExperiments: Number of pseudo-experiments < 0\n";
        return {};
    }

    std::map<std::string, std::vector<double> > result;

    for (int itoy = 0; itoy < n; ++itoy) {
        if (itoy % 100 == 0) {
            std::cout << "FFFitter::RunCombinedPseudoExperiments: Running pseudoexperiment " << itoy << " out of " << n << " toys\n";
        }
        std::vector<TH1D> single_hists_copy;
        std::vector<TH1D> cross_hists_copy;
        for (const auto& ihist : single_region_hists) {
            single_hists_copy.emplace_back(std::move(*static_cast<TH1D*>(ihist.Clone())));
            single_hists_copy.back().SetDirectory(nullptr);
        }
        for (const auto& ihist : cross_region_hists) {
            cross_hists_copy.emplace_back(std::move(*static_cast<TH1D*>(ihist.Clone())));
            cross_hists_copy.back().SetDirectory(nullptr);
        }

        for (auto& ihist : single_hists_copy) {

            this->Poissonise(&ihist, &rand);
        }
        for (auto& ihist : cross_hists_copy) {
            this->Poissonise(&ihist, &rand);
        }

        const auto map = this->FitCombinedModel(single_hists_copy, cross_hists_copy, false, -1);

        for (const auto& iresult : map) {
            const std::string param = iresult.first;
            const double value = iresult.second;

            auto itr = result.find(param);
            if (itr == result.end()) {
                result.insert({param, std::vector<double>(1, value)});
            } else {
                itr->second.emplace_back(value);
            }
        }
    }

    return result;
}

std::unique_ptr<TH2D> FFFitter::GetCorrelationMatrix() const {
    if (!m_fit_result_combined) {
        std::cerr << "FFFitter::GetCorrelationMatrix: Fit results not defined\n";
        return nullptr;
    }

    std::unique_ptr<TH2D> result = std::make_unique<TH2D>("","", 6, 0.5, 6.5, 6, 0.5, 6.5);

    for (int ibin = 1; ibin <= result->GetNbinsX(); ++ibin) {
        for (int jbin = 1; jbin <= result->GetNbinsX(); ++jbin) {
            const std::string param_i = (m_is_jes_fit ? "JES" : "JER") + std::to_string(ibin);
            const std::string param_j = (m_is_jes_fit ? "JES" : "JER") + std::to_string(jbin);
            const double corr = m_fit_result_combined->correlation(param_i.c_str(), param_j.c_str());
            result->SetBinContent(ibin, jbin, corr);
            result->SetBinContent(jbin, ibin, corr);
        }
    }
    for (int ibin = 1; ibin <= result->GetNbinsX(); ++ibin) {
        const std::string param = (m_is_jes_fit ? "JES" : "JER") + std::to_string(ibin);
        result->GetXaxis()->SetBinLabel(ibin, param.c_str());
        result->GetYaxis()->SetBinLabel(ibin, param.c_str());
    }

    return result;
}

void FFFitter::SetStartingParameter(const RooAbsPdf* model, const RooAbsData* data, const std::vector<std::pair<std::string, double> >& param_values) const {
    const auto* params = model->getParameters(data);
    for (auto iparam : *params) {
        const std::string name = iparam->GetName();
        auto it = std::find_if(param_values.begin(), param_values.end(), [&name](const auto& element){return element.first == name;});
        if (it != param_values.end()) {
            if (m_debug) {
                std::cout << "FFFitter::SetStartingParameter: Setting parameter " << name << " to: " << it->second << "\n";
            }
            static_cast<RooRealVar*>(iparam)->setVal(it->second);
        }
    }
}

void FFFitter::SetNominalSingleRegion(const std::vector<TH1D>& hists) {
    m_nominal_single_region.clear();
    for (const auto& ihist : hists) {
        std::unique_ptr<TH1D> h(static_cast<TH1D*>(ihist.Clone()));
        m_nominal_single_region.emplace_back(std::move(*h));
    }
}

void FFFitter::SetNominalCrossRegion(const std::vector<TH1D>& hists) {
    m_nominal_cross_region.clear();
    for (const auto& ihist : hists) {
        std::unique_ptr<TH1D> h(static_cast<TH1D*>(ihist.Clone()));
        m_nominal_cross_region.emplace_back(std::move(*h));
    }
}