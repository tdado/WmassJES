#include "WmassJES/Extractor.h"
#include "WmassJES/SetupSystematic.h"
#include "WmassJES/SystHistoManager.h"

#include "TError.h"

#include <iostream>
#include <string>

int main(int argc, char** argv) {

    if (argc < 3 || argc > 4) {
        std::cerr << "Expected two or three parameters but " << argc -1 << " parameters provided" << std::endl;
        exit(EXIT_FAILURE);
    }

    gErrorIgnoreLevel = kError;

    const std::string folder(argv[1]);
    bool is_jes(false);
    bool run_syst(false);
    const std::string jes_string = argv[2];
    if (jes_string == "jes") {
        is_jes = true;
    } else if (jes_string == "jer") {
        is_jes = false;
    } else {
        std::cerr << "The second argument has to be \"jes\" or \"jer\".\n";
        exit(1);
    }
    if (argc == 4) {
        const std::string s = argv[3];
        if (s == "syst") run_syst = true;
    }

    static const std::vector<std::string> suffixes = {"pt_35_50", "pt_50_70", "pt_70_100", "pt_100_150", "pt_150_200"};
    //static const std::vector<std::string> suffixes = {"pt_20_30", "pt_30_50", "pt_50_70", "pt_70_100", "pt_100_150"};
    //static const std::vector<std::string> suffixes = {"pt_100_150"};

    //std::vector<std::pair<std::string, std::string> > single_regions = {{"pt_20_30_20_30", "param1"},
    //                                                                    {"pt_30_50_30_50", "param2"},
    //                                                                    {"pt_50_70_50_70", "param3"},
    //                                                                    {"pt_70_100_70_100", "param4"},
    //                                                                    {"pt_100_150_100_150", "param5"}};

    std::vector<std::pair<std::string, std::string> > single_regions = {{"pt_35_50_35_50", "param2"},
                                                                        {"pt_50_70_50_70", "param3"},
                                                                        {"pt_70_100_70_100", "param4"},
                                                                        {"pt_100_150_100_150", "param5"},
                                                                        {"pt_150_200_150_200", "param6"}};

    std::vector<std::pair<std::string, std::pair<std::string, std::string> > > cross_regions = {{"pt_20_35_35_50", {"param1", "param2"}},
                                                                                                {"pt_20_35_50_70", {"param1", "param3"}},
                                                                                                {"pt_20_35_70_100", {"param1", "param4"}},
                                                                                                {"pt_20_35_100_150", {"param1", "param5"}},
                                                                                                {"pt_20_35_150_200", {"param1", "param6"}},
                                                                                                {"pt_35_50_50_70", {"param2", "param3"}},
                                                                                                {"pt_35_50_70_100", {"param2", "param4"}},
                                                                                                {"pt_35_50_100_150", {"param2", "param5"}},
                                                                                                {"pt_35_50_150_200", {"param2", "param6"}},
                                                                                                {"pt_50_70_70_100", {"param3", "param4"}},
                                                                                                {"pt_50_70_100_150", {"param3", "param5"}},
                                                                                                {"pt_50_70_150_200", {"param3", "param6"}},
                                                                                                {"pt_70_100_100_150", {"param4", "param5"}},
                                                                                                {"pt_70_100_150_200", {"param4", "param6"}},
                                                                                                {"pt_100_150_150_200", {"param5", "param6"}}};

    //std::vector<std::pair<std::string, std::pair<std::string, std::string> > > cross_regions = {{"pt_30_50_50_70", {"param2", "param3"}},
    //                                                                                            {"pt_30_50_70_100", {"param2", "param4"}},
    //                                                                                            {"pt_30_50_100_150", {"param2", "param5"}},
    //                                                                                            {"pt_50_70_70_100", {"param3", "param4"}},
    //                                                                                            {"pt_50_70_100_150", {"param3", "param5"}},
    //                                                                                            {"pt_70_100_100_150", {"param4", "param5"}}};

    std::vector<int> closure_indices_combined = {2,5,8};

    Extractor extractor(folder);

    extractor.SetProfile(false);
    extractor.SetIs1DFit(false);
    extractor.SetFitData(true);
    extractor.SetRunClosure(true);
    extractor.SetStartIndex(1);
    extractor.SetParameterS(11,0.95,1.05);
    extractor.SetParameterR(11,0.6,1.40);
    extractor.SetTtbarName(SetupSystematic::NominalTtbarSample());
    extractor.SetDebug(false);
    extractor.SetIsJesFit(is_jes);
    extractor.SetSingleRegions(single_regions);
    extractor.SetCrossRegions(cross_regions);
    extractor.SetRegion("Inclusive_no_met_cut");
    extractor.SetVariableSuffixes(suffixes);
    extractor.SetUseSyst(run_syst);
    extractor.SetNumberOfPE(-1);
    //extractor.SetNumberOfPE(1000);
    //extractor.SetNumberOfPE(1000);
    extractor.SetBackgroundNames(SetupSystematic::GetListOfBackgroundSamples());
    extractor.SetSpecialNames(SetupSystematic::GetListOfSpecialSamples());
    extractor.SetAllSysts(SetupSystematic::GetAllSystematics(false, is_jes));
    extractor.SetTemplateName("W_mass");
    extractor.SetObsName("W_mass_peak");
    extractor.SetClosureIndicesCombined(closure_indices_combined);

    extractor.ExtractJES();

    return 0;
}
