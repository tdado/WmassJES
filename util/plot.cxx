#include "WmassJES/Plotter.h"
#include "WmassJES/SetupSystematic.h"
#include "WmassJES/SystHistoManager.h"

#include "TError.h"

#include <iostream>
#include <string>
#include <vector>

int main (int argc, const char** argv) {
    if (argc < 2 || argc > 3) {
        std::cerr << "Expected one or two parameters but " << argc-1 << " parameters provided" << std::endl;
        exit(EXIT_FAILURE);
    }

    gErrorIgnoreLevel = kError;

    const std::string folder = (argv[1]);
    bool run_syst(false);

    if (argc == 3) {
        const std::string s = argv[2];
        if (s == "syst") {
            run_syst = true;
        } else {
            std::cerr << "Third argument can only be 'syst'" << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    Plotter plotter(folder);

    plotter.SetRegionNames({"Inclusive_no_met_cut","Inclusive","Jet_pt_20_35_20_35_eta_0p8","Jet_pt_35_50_35_50_eta_0p8","Jet_pt_50_70_50_70_eta_0p8","Jet_pt_70_100_70_100_eta_0p8","Jet_pt_100_150_100_150_eta_0p8","Jet_pt_150_200_150_200_eta_0p8",
                                                               "Jet_pt_20_35_35_50_eta_0p8","Jet_pt_20_35_50_70_eta_0p8","Jet_pt_20_35_70_100_eta_0p8","Jet_pt_20_35_100_150_eta_0p8","Jet_pt_20_35_150_200_eta_0p8",
                                                               "Jet_pt_35_50_50_70_eta_0p8","Jet_pt_35_50_70_100_eta_0p8","Jet_pt_35_50_100_150_eta_0p8","Jet_pt_35_50_150_200_eta_0p8",
                                                               "Jet_pt_50_70_70_100_eta_0p8","Jet_pt_50_70_100_150_eta_0p8","Jet_pt_50_70_150_200_eta_0p8",
                                                               "Jet_pt_70_100_100_150_eta_0p8","Jet_pt_70_100_150_200_eta_0p8",
                                                               "Jet_pt_100_150_150_200_eta_0p8"});
    plotter.SetOutputDirectory("Plots");
    plotter.SetTTbarNames(SetupSystematic::GetListOfTtbarSamples());
    plotter.SetBackgroundNames(SetupSystematic::GetListOfBackgroundSamples());
    plotter.SetSpecialNames(SetupSystematic::GetListOfSpecialSamples());
    plotter.SetDataName("Data");
    plotter.SetAtlasLabel("Internal");
    plotter.SetLumiLabel("140");
    plotter.SetCollection("pflow");
    plotter.SetNominalTTbarName(SetupSystematic::NominalTtbarSample());
    plotter.SetSystShapeOnly(false);
    plotter.SetUseSyst(run_syst);

    plotter.OpenRootFiles();

    /// Pass all systematics
    plotter.SetAllSysts(SetupSystematic::GetAllSystematics(true, true));

    /// 1D truth plots
    //plotter.PlotTruthPlots("truth_W_pt", "particle truth W p_{T} [GeV]", true);
    //plotter.PlotTruthPlots("truth_W_eta", "particle truth W |#eta| [-]", true);
    //plotter.PlotTruthPlots("truth_W_m", "particle truth W mass [GeV]", true);
    //plotter.PlotTruthPlots("reco_truth_ratio_W_m", "reco W mass /particle truth W mass [-]", true);
    //plotter.PlotTruthPlots("truth_W_jets_dR", "particle truth #Delta R jets from W [-]", true);
    //plotter.PlotTruthPlots("truth_W_jets_vec_sum_pt", "particle truth vector sum p_{T} jets from W [GeV]", true);
    //plotter.PlotTruthPlots("truth_W_jets_scalar_sum_pt", "particle truth scalar sum p_{T} jets from W [GeV]", true);
    //plotter.PlotTruthPlots("truth_W_quark_pt", "parton truth W p_{T} [GeV]", true);
    //plotter.PlotTruthPlots("truth_W_quark_eta", "parton truth W |#eta| [-]", true);
    //plotter.PlotTruthPlots("truth_W_quark_m", "parton truth W mass [GeV]", true);
    //plotter.PlotTruthPlots("truth_W_identified_pt", "reco matched particle truth W p_{T} [GeV]", true);
    //plotter.PlotTruthPlots("truth_W_identified_eta", "reco matched particle truth W |#eta| [-]", true);
    //plotter.PlotTruthPlots("truth_W_identified_m", "reco matched particle truth W mass [GeV]", true);
    //plotter.PlotTruthPlots("jet_parton_ratio_pt", "particle truth jet p_{T}/parton p_{T} [-]", true);
    //plotter.PlotTruthPlots("jet_parton_ratio_e", "particle truth jet E/parton E [-]", true);
    //plotter.PlotTruthPlots("jet_parton_difference_m", "particle truth jet mass - parton mass [GeV]", true);
    //plotter.PlotTruthPlots("jet_parton_difference_eta", "particle truth jet #eta - parton #eta [-]", true);
    //plotter.PlotTruthPlots("jet_parton_difference_phi", "particle truth jet #phi - parton #phi [-]", true);
    //plotter.PlotTruthPlots("jet_parton_W_difference_pt", "(j1 + j2 p_{T}) - (parton1 + parton2 p_{T}) [GeV]", true);
    //plotter.PlotTruthPlots("jet_parton_W_ratio_pt_50", "(j1 + j2 p_{T}) / (parton1 + parton2 p_{T}), (parton1 + parton2 p_{T} > 50) [-]", true);
    //plotter.PlotTruthPlots("jet_parton_W_ratio_m", "(j1 + j2 mass) / (parton1 + parton2 mass), [-]", true);
    //plotter.PlotTruthPlots("truth_parton_lepton_W_m", "parton leptonic W mass [GeV]", true);
    //plotter.PlotTruthPlots("truth_parton_lepton_W_pt", "parton leptonic W p_{T}  [GeV]", true);
    //plotter.PlotTruthPlots("truth_parton_lepton_W_eta", "parton leptonic W |#eta| [-]", true);

    // W mass template plots
    //1D:
    //normalised
    //plotter.PlotTemplatePlots("W_mass","Reconstructed W mass [GeV]", "Events", "GeV", true, true, true, false); //JES
    //plotter.PlotTemplatePlots("W_mass","Reconstructed W mass [GeV]", "Events", "GeV", true, false, true, false); //JER
    
    ////not normalised
    plotter.PlotTemplatePlots("W_mass","Reconstructed W mass [GeV]", "Events", "GeV", true, true, false, false); //JES
    plotter.PlotTemplatePlots("W_mass","Reconstructed W mass [GeV]", "Events", "GeV", true, false, false, false); //JER


    //2D:
    //normalised
    //plotter.PlotTemplatePlots("W_mass","Reconstructed W mass [GeV]", "Events", "GeV", false, true, true, false); //JES
    //plotter.PlotTemplatePlots("W_mass","Reconstructed W mass [GeV]", "Events", "GeV", false, false, true, false); //JER
    
    //not normalised
    plotter.PlotTemplatePlots("W_mass","Reconstructed W mass [GeV]", "Events", "GeV", false, true, false, false); //JES
    plotter.PlotTemplatePlots("W_mass","Reconstructed W mass [GeV]", "Events", "GeV", false, false, false, false); //JER


    // Response template plots for jets from W
    //plotter.PlotTemplatePlots("Jet1_response","Jet1 folded p_{T}/truth p_{T} [-]", "", "", true, true, true);
    //plotter.PlotTemplatePlots("Jet1_response","Jet1 folded p_{T}/truth p_{T} [-]", "", "", true, false, true);
    //plotter.PlotTemplatePlots("Jet1_response","Jet1 folded p_{T}/truth p_{T} [-]", "", "", false, true, true);
    //plotter.PlotTemplatePlots("Jet1_response","Jet1 folded p_{T}/truth p_{T} [-]", "", "", false, false, true);

    //plotter.PlotTemplatePlots("Jet2_response","Jet2 folded p_{T}/truth p_{T} [-]", "", "", true, true, true);
    //plotter.PlotTemplatePlots("Jet2_response","Jet2 folded p_{T}/truth p_{T} [-]", "", "", true, false, true);
    //plotter.PlotTemplatePlots("Jet2_response","Jet2 folded p_{T}/truth p_{T} [-]", "", "", false, true, true);
    //plotter.PlotTemplatePlots("Jet2_response","Jet2 folded p_{T}/truth p_{T} [-]", "", "", false, false, true);

    // 1D Data/MC plots
    plotter.PlotDataMCPlots("best_Chi2","Matching #chi^2", "Events", "", false);
    plotter.PlotDataMCPlots("best_Chi2","Matching #chi^2", "Events", "", true);
    plotter.PlotDataMCPlots("leading_jet_pt","Leading jet p_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("leading_jet_pt","Leading jet p_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("leading_jet_eta","Leading jet #eta [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("leading_jet_phi","Leading jet #phi [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("jet1_jvt","Leading jet JVT [-]", "Events", "", false);
    plotter.PlotDataMCPlots("second_leading_jet_pt","Second jet p_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("second_leading_jet_pt","Second jet p_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("second_leading_jet_eta","Second jet #eta [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("second_leading_jet_phi","Second jet #phi [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("jet2_jvt","Second jet JVT [-]", "Events", "", false);
    plotter.PlotDataMCPlots("third_leading_jet_pt","Third jet p_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("third_leading_jet_pt","Third jet p_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("third_leading_jet_eta","Third jet #eta [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("third_leading_jet_phi","Third jet #phi [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("jet3_jvt","Third jet JVT [-]", "Events", "", false);
    plotter.PlotDataMCPlots("fourth_leading_jet_pt","Fourth jet p_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("fourth_leading_jet_pt","Fourth jet p_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("fourth_leading_jet_eta","Fourth jet #eta [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("jet4_phi","Fourth jet #phi [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("jet4_jvt","Fourth jet JVT [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("jet5_pt","Fifth jet p_{T} [GeV]", "Events", "GeV", false);
    //plotter.PlotDataMCPlots("jet5_pt","Fifth jet p_{T} [GeV]", "Events", "GeV", true);
    //plotter.PlotDataMCPlots("jet5_eta","Fifth jet |#eta| [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("jet5_phi","Fifth jet #phi [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("jet5_jvt","Fifth jet JVT [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("jet6_pt","Sixth jet p_{T} [GeV]", "Events", "GeV", false);
    //plotter.PlotDataMCPlots("jet6_pt","Sixth jet p_{T} [GeV]", "Events", "GeV", true);
    //plotter.PlotDataMCPlots("jet6_eta","Sixth jet |#eta| [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("jet6_phi","Sixth jet #phi [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("jet6_jvt","Sixth jet JVT [-]", "Events", "", false);
    plotter.PlotDataMCPlots("leading_bjet_pt","Leading b-jet p_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("leading_bjet_pt","Leading b-jet p_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("leading_bjet_eta","Leading b-jet #eta [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("bjet1_phi","Leading b-jet #phi [-]", "Events", "", false);
    plotter.PlotDataMCPlots("second_leading_bjet_pt","Second b-jet p_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("second_leading_bjet_pt","Second b-jet p_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("second_leading_bjet_eta","Second b-jet #eta [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("bjet2_phi","Second b-jet #phi [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("jet_pt","All jet p_{T} [GeV]", "Jets", "GeV", false);
    //plotter.PlotDataMCPlots("jet_pt","All jet p_{T} [GeV]", "Jets", "GeV", true);
    //plotter.PlotDataMCPlots("jet_eta","All jet |#eta| [-]", "Jets", "", false);
    //plotter.PlotDataMCPlots("jet_phi","All jet #phi [-]", "Jets", "", false);
    //plotter.PlotDataMCPlots("jet_pt_20_50","Jet p_{T}, p_{T} < 50 GeV [GeV]", "Jets", "", false);
    //plotter.PlotDataMCPlots("jet_eta_20_50","Jet |#eta|, p_{T} < 50 GeV [-]", "Jets", "", false);
    //plotter.PlotDataMCPlots("jet_pt_20_30","Jet p_{T}, p_{T} < 30 GeV [GeV]", "Jets", "", false);
    //plotter.PlotDataMCPlots("jet_eta_20_30","Jet |#eta|, p_{T} < 30 GeV [-]", "Jets", "", false);
    //plotter.PlotDataMCPlots("jet_pt_50","Jet p_{T}, p_{T} > 50 GeV [GeV]", "Jets", "", false);
    //plotter.PlotDataMCPlots("jet_pt_50","Jet p_{T}, p_{T} > 50 GeV [GeV]", "Jets", "", true);
    //plotter.PlotDataMCPlots("jet_eta_50","Jet |#eta|, p_{T} > 50 GeV [-]", "Jets", "", false);
    //plotter.PlotDataMCPlots("jet_pt_crack","Crack jet p_{T} [GeV]", "Jets", "GeV", false);
    //plotter.PlotDataMCPlots("jet_pt_crack","Crack jet p_{T} [GeV]", "Jets", "GeV", true);
    //plotter.PlotDataMCPlots("jet_eta_crack","Crack jet |#eta| [-]", "Jets", "", false);
    //plotter.PlotDataMCPlots("jet_phi_crack","Crack jet #phi [-]", "Jets", "", false);
    //plotter.PlotDataMCPlots("jet_jvt_crack","Crack jet JVT [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("jet_jvt_crack","Crack jet JVT [-]", "Events", "", true);
    //plotter.PlotDataMCPlots("HT","H_{T} [GeV]", "Events", "GeV", false);
    //plotter.PlotDataMCPlots("HT","H_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("number_of_jets","Jet multiplicity [-]", "Events", "", false);
    plotter.PlotDataMCPlots("number_of_b_jets","b-jet multiplicity [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("jet_el_dR","#Delta R (jet, el) [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("jet_mu_dR","#Delta R (jet, mu) [-]", "Events", "", false);

    plotter.PlotDataMCPlots("electron_pt","Electron p_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("electron_pt","Electron p_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("electron_eta","Electron #eta [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("el_phi","Electron #phi [-]", "Events", "", false);
    plotter.PlotDataMCPlots("muon_pt","Muon p_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("muon_pt","Muon p_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("muon_eta","Muon #eta [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("mu_phi","Muon #phi [-]", "Events", "", false);
    plotter.PlotDataMCPlots("met","E_{T}^{miss} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("met","E_{T}^{miss} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("electron_met","E_{T}^{miss} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("electron_met","E_{T}^{miss} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("muon_met","E_{T}^{miss} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("muon_met","E_{T}^{miss} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("met_phi","E_{T}^{miss} #phi [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("average_mu","average #mu [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("actual_mu","actual #mu [-]", "Events", "", false);

    //plotter.PlotDataMCPlots("Wjet1_pt","Leading jet from W p_{T} [GeV]", "Events", "GeV", false);
    //plotter.PlotDataMCPlots("Wjet1_pt","Leading jet from W p_{T} [GeV]", "Events", "GeV", true);
    //plotter.PlotDataMCPlots("Wjet1_eta","Leading jet from W |#eta| [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("Wjet1_phi","Leading jet from W #phi [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("Wjet2_pt","Second jet from W p_{T} [GeV]", "Events", "GeV", false);
    //plotter.PlotDataMCPlots("Wjet2_pt","Second jet from W p_{T} [GeV]", "Events", "GeV", true);
    //plotter.PlotDataMCPlots("Wjet2_eta","Second jet from W |#eta| [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("Wjet2_phi","Second jet from W #phi [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("W_pt","W p_{T} [GeV]", "Events", "GeV", false);
    //plotter.PlotDataMCPlots("W_pt","W p_{T} [GeV]", "Events", "GeV", true);
    //plotter.PlotDataMCPlots("W_eta","W |#eta| [-]", "Events", "", false);
    //plotter.PlotDataMCPlots("W_phi","W #phi [-]", "Events", "", false);
    plotter.PlotDataMCPlots("W_mass","Reconstructed W mass [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("W_mass_peak","Reconstructed W mass [GeV]", "Events", "GeV", false);

    /// 1D response plots (binned in eta and pt)
    plotter.PlotResponsePlots("jet_response", "light_jet", "light jet reco p_{T}/truth p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response", "cjet", "c-jet reco p_{T}/truth p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response", "bjet", "b-jet reco p_{T}/truth p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response", "gluon_jet", "gluon jet reco p_{T}/truth p_{T} [-]", true);

    /// Make html tables with the reco efficiencies
    plotter.PlotRecoEfficiencies();

    plotter.PlotJetPairing();

    /// Make html table with yields
    plotter.CalculateYields();

    plotter.CloseRootFiles();

    /// Successful run
    return 0;
}
