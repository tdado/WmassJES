#include "TFile.h"
#include "TKey.h"
#include "TH1D.h"

#include <iostream>
#include <memory>
#include <string>
#include <vector>

std::unique_ptr<TH1D> CombineHistos(const std::vector<std::unique_ptr<TFile> >& mc, const std::unique_ptr<TFile>& data, const std::string& histo) {
    std::unique_ptr<TH1D> result(data->Get<TH1D>(("NOSYS/"+histo).c_str()));
    if (!result) {
        std::cerr << "Cannot find histo: " << "NOSYS/"+histo << " in data\n";
        exit(EXIT_FAILURE);
    }
    result->SetName(histo.c_str());

    for (const auto& imc : mc) {
        std::unique_ptr<TH1D> h(imc->Get<TH1D>(("NOSYS/"+histo).c_str()));
        if (!h) {
            std::cerr << "Cannot find histo: " << "NOSYS/"+histo << "\n";
            exit(EXIT_FAILURE);
        }

        result->Add(h.get(), -1);
    }

    result->SetDirectory(nullptr);
    return result;
}

std::vector<std::string> GetHistoList(std::unique_ptr<TFile>& data) {

    std::vector<std::string> result;

    data->cd("NOSYS");

    TIter next(gDirectory->GetListOfKeys());
    TKey *key;
    while ((key = static_cast<TKey*>(next()))) {
        if (key->IsFolder()) continue;
        const std::string str = key->GetName();
        result.emplace_back(str);
    }

    return result;
}


int main(int argc, char** argv) {
    if (argc != 3) {
        std::cerr << "Expected 2 arguments, but " << argc - 1 << " arguments provided\n";
        return 1;
    }

    const std::string input_folder = argv[1];
    const std::string output_folder = argv[2];

    static const std::vector<std::string> mc_files = {"ttbar_PP8_FS_loose.root", "Wjets_Sherpa_FS_loose.root",
                                                      "Zjets_Sherpa_FS_loose.root", "Diboson_Sherpa_FS_loose.root", "SingleTop_s_PP8_FS_loose.root",
                                                      "SingleTop_t_PP8_FS_loose.root", "SingleTop_tW_DR_PP8_FS_loose.root"};
    static const std::string data_file = "Data_loose.root";

    std::vector<std::unique_ptr<TFile> > mc;
    for (const auto& imc : mc_files) {
        std::unique_ptr<TFile> file(TFile::Open((input_folder + "/" + imc).c_str(), "READ"));
        if (!file) {
            std::cerr << "Cannot open MC file: " << imc << "\n";
            return 1;
        }

        mc.emplace_back(std::move(file));
    }

    std::unique_ptr<TFile> data(TFile::Open((input_folder + "/" + data_file).c_str(), "READ"));
    if (!data) {
        std::cerr << "Cannot open data file: " << data_file << "\n";
        return 1;
    }

    std::unique_ptr<TFile> out(TFile::Open((output_folder + "/Fakes.root").c_str(), "RECREATE"));
    if (!out) {
        std::cerr << "Cannot open output file: " << output_folder + "/Fakes.root\n";
        return 1;
    }

    out->cd();
    out->mkdir("NOSYS");

    std::vector<std::string> histos = GetHistoList(data);
    for (const auto& ihist : histos) {
        std::unique_ptr<TH1D> hist = CombineHistos(mc, data, ihist);
        out->cd("NOSYS");
        hist->Write(hist->GetName());
    }

    out->Close();
    data->Close();
    for (auto& f : mc) {
        f->Close();
    }

    return 0;
}
